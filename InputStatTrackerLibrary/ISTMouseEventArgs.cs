﻿using System.Windows.Forms;

namespace InputStatTrackerLibrary
{
    /**
     * Provides data for the MouseClickExt and MouseMoveExt events. It also provides a property Handled.
     * Set this property to <b>true</b> to prevent further processing of the event in other applications.
     *
     * @author  Kovox
     * @date    07/03/2018
     */
    public class ISTMouseEventArgs : MouseEventArgs
    {
        private bool handled;

        /**
         * Initializes a new instance of the MouseEventArgs class.
         *
         * @author  Kovox
         * @date    07/03/2018
         *
         * @param   buttons Value indicating which mouse button was pressed.
         * @param   clicks  The number of times a mouse button was pressed.
         * @param   x       The x coordinate of a mouse click in pixels.
         * @param   y       The y coordinate of a mouse click, in pixels.
         * @param   delta   A signed count of the number of detents the wheel has rotated.
         */
        public ISTMouseEventArgs(MouseButtons buttons, int clicks, int x, int y, int delta)
            : base(buttons, clicks, x, y, delta)
        {

        }

        /**
         * Set this property to <b>true</b> inside your event handler to prevent 
         * further processing of the event in other applications.
         *
         * @return  True if not handled, otherwise false.
         */
        public bool Handled
        {
            get { return handled; }
            set { handled = value; }
        }
    }
}