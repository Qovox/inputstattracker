using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace InputStatTrackerLibrary
{
    public static partial class HookManager
    {
        /**
         * The CallWndProc hook procedure is an application-defined or library-defined callback function
         * used with the SetWindowsHookEx function. The HOOKPROC type defines a pointer to this callback
         * function. CallWndProc is a placeholder for the application-defined or library-defined
         * function name.
         *
         * @author Kovox
         * @date 07/03/2018
         *
         * @param [in] nCode  Specifies whether the hook procedure must process the message. If nCode is
         *                    HC_ACTION, the hook procedure must process the message. If nCode is less
         *                    than zero, the hook procedure must pass the message to the CallNextHookEx
         *                    function without further processing and must return the value returned by
         *                    CallNextHookEx.
         * @param [in] wParam Specifies whether the message was sent by the current thread. If the message
         *                    was sent by the current thread, it is nonzero; otherwise, it is zero.
         * @param [in] lParam Pointer to a CWPSTRUCT structure that contains details about the message.
         *
         * @return If nCode is less than zero, the hook procedure must return the value returned by
         *         CallNextHookEx. If nCode is greater than or equal to zero, it is highly recommended
         *         that you call CallNextHookEx and return the value it returns; otherwise, other
         *         applications that have installed WH_CALLWNDPROC hooks will not receive hook
         *         notifications and may behave incorrectly as a result. If the hook procedure does not
         *         call CallNextHookEx, the return value should be zero.
         */
        private delegate int HookProc(int nCode, int wParam, IntPtr lParam);

        #region Mouse hook processing

        /**
         * This field is not objectively needed but we need to keep a reference on a delegate which will
         * be passed to unmanaged code. To avoid GC to clean it up. When passing delegates to unmanaged
         * code, they must be kept alive by the managed application until it is guaranteed that they
         * will never be called.
         */
        private static HookProc MouseHookProcDelegate;

        /**
         * Stores the handle to the mouse hook procedure.
         */
        private static int MouseHookHandle;

        /**
         * The old X coordinate value of the mouse pointer.
         */
        private static int OldX;

        /**
         * The old Y coordinate value of the mouse pointer.
         */
        private static int OldY;

        /**
         * A callback function which will be called every Time a mouse activity is detected.
         *
         * @author Kovox
         * @date 07/03/2018
         *
         * @param [in] nCode  Specifies whether the hook procedure must process the message. If nCode is
         *                    HC_ACTION, the hook procedure must process the message. If nCode is less
         *                    than zero, the hook procedure must pass the message to the CallNextHookEx
         *                    function without further processing and must return the value returned by
         *                    CallNextHookEx.
         * @param [in] wParam Specifies whether the message was sent by the current thread. If the message
         *                    was sent by the current thread, it is nonzero; otherwise, it is zero.
         * @param [in] lParam Pointer to a CWPSTRUCT structure that contains details about the message.
         *
         * @return If nCode is less than zero, the hook procedure must return the value returned by
         *         CallNextHookEx. If nCode is greater than or equal to zero, it is highly recommended
         *         that you call CallNextHookEx and return the value it returns; otherwise, other
         *         applications that have installed WH_CALLWNDPROC hooks will not receive hook
         *         notifications and may behave incorrectly as a result. If the hook procedure does not
         *         call CallNextHookEx, the return value should be zero.
         */
        private static int MouseHookProc(int nCode, int wParam, IntPtr lParam)
        {
            if (nCode >= 0)
            {
                // Marshall the data from callback.
                MouseHookStruct mouseHookStruct =
                    (MouseHookStruct)Marshal.PtrToStructure(lParam, typeof(MouseHookStruct));

                // Detect button clicked
                MouseButtons button = MouseButtons.None;
                short mouseDelta = 0;
                int clickCount = 0;
                bool mouseDown = false;
                bool mouseUp = false;

                switch (wParam)
                {
                    case WM_LBUTTONDOWN:
                        mouseDown = true;
                        button = MouseButtons.Left;
                        clickCount = 1;
                        break;
                    case WM_LBUTTONUP:
                        mouseUp = true;
                        button = MouseButtons.Left;
                        clickCount = 1;
                        break;
                    case WM_LBUTTONDBLCLK:
                        button = MouseButtons.Left;
                        clickCount = 2;
                        break;
                    case WM_RBUTTONDOWN:
                        mouseDown = true;
                        button = MouseButtons.Right;
                        clickCount = 1;
                        break;
                    case WM_RBUTTONUP:
                        mouseUp = true;
                        button = MouseButtons.Right;
                        clickCount = 1;
                        break;
                    case WM_RBUTTONDBLCLK:
                        button = MouseButtons.Right;
                        clickCount = 2;
                        break;
                    case WM_MBUTTONDOWN:
                        mouseDown = true;
                        button = MouseButtons.Middle;
                        clickCount = 1;
                        break;
                    case WM_MBUTTONUP:
                        mouseUp = true;
                        button = MouseButtons.Middle;
                        clickCount = 1;
                        break;
                    case WM_MBUTTONDBLCLK:
                        button = MouseButtons.Middle;
                        clickCount = 2;
                        break;
                    case WM_MOUSEWHEEL:
                        // If the message is WM_MOUSEWHEEL, the high-order word of MouseData member is 
                        // the wheel delta. One wheel roll is defined as WHEEL_DELTA, which is 120. 
                        // (value >> 16) & 0xffff; retrieves the high-order word from the given 32-bit value.
                        // Remember: int = 32bits 0xffff = 1111 1111 1111 1111 (16bits)
                        // If we right shift by 16 the value mouseHookStruct.MouseData which is an int,
                        // then we'll have 0000 0000 0000 0000 ???? ???? ???? ???? ???? where ? are the
                        // unknown bit values of the mouseHookStruct.MouseData attribute.
                        // The & operator with 0xffff will then ouput the correct high-order value.
                        // Remember : 11010 & 10101 = 10000...
                        mouseDelta = (short)((mouseHookStruct.MouseData >> 16) & 0xffff);
                        break;
                    case WM_XBUTTONDOWN:
                        mouseDown = true;
                        clickCount = 1;
                        break;
                    case WM_XBUTTONUP:
                        mouseUp = true;
                        clickCount = 1;
                        break;
                    case WM_XBUTTONDBLCLK:
                        clickCount = 2;
                        break;
                }

                // Check the x button. It is done after the switch to avoid code duplication.
                if (button == MouseButtons.None && mouseDelta == 0)
                {
                    short xbutton = (short)((mouseHookStruct.MouseData >> 16) & 0xffff);
                    if (xbutton == XBUTTON1)
                    {
                        button = MouseButtons.XButton1;
                    }
                    else if (xbutton == XBUTTON2)
                    {
                        button = MouseButtons.XButton2;
                    }
                }

                // Generate event 
                ISTMouseEventArgs e = new ISTMouseEventArgs(button, clickCount, mouseHookStruct.Point.X,
                    mouseHookStruct.Point.Y, mouseDelta);

                if (mouseUp) { MouseUpEvent?.Invoke(null, e); }
                if (mouseDown) { MouseDownEvent?.Invoke(null, e); }
                if (clickCount > 0) { MouseClickEvent?.Invoke(null, e); }
                // if (clickCount > 0) { MouseClickExtEvent?.Invoke(null, e); }
                if (clickCount == 2)
                {
                    MouseDoubleClickEvent?.Invoke(null, e);
                }
                if (mouseDelta != 0) { MouseWheelEvent?.Invoke(null, e); }

                // If someone listens to move and there was a change in coordinates raise move event
                if (OldX != mouseHookStruct.Point.X || OldY != mouseHookStruct.Point.Y)
                {
                    OldX = mouseHookStruct.Point.X;
                    OldY = mouseHookStruct.Point.Y;

                    MouseMoveEvent?.Invoke(null, e);
                    // MouseMoveExtEvent?.Invoke(null, e);
                }

                if (e.Handled)
                {
                    return -1;
                }
            }

            // Call next hook
            return CallNextHookEx(MouseHookHandle, nCode, wParam, lParam);
        }

        /**
         * Subscribe to mouse events. It first checks if the subscribe hasn't been done before. If not,
         * then it installs the hook and throws an error if the hook operation failed.
         */
        private static void SubscribeToMouseEvents()
        {
            // Install Mouse hook only if it is not installed and must be installed
            if (MouseHookHandle == 0)
            {
                // See comment of this field. To avoid GC (Garbage Collector) to clean it up.
                // This field is like a kind of pointer to function. Delegate in C# is a bit 
                // weird to understand.
                MouseHookProcDelegate = MouseHookProc;

                // Installs hook.
                // Previous line of code to retrieve module handle (works for .NET Framework with
                // version less than 4.0)
                // Marshal.GetHINSTANCE(Assembly.GetExecutingAssembly().GetModules()[0])
                MouseHookHandle = SetWindowsHookEx(WH_MOUSE_LL, MouseHookProcDelegate,
                    GetModuleHandle(Process.GetCurrentProcess().MainModule.ModuleName), 0);

                // If SetWindowsHookEx fails.
                if (MouseHookHandle == 0)
                {
                    // Returns the error code returned by the last unmanaged function called using 
                    // platform invoke that has the DllImportAttribute.SetLastError flag set. 
                    int errorCode = Marshal.GetLastWin32Error();

                    // Initializes and throws a new instance of the Win32Exception class with the specified error. 
                    throw new Win32Exception(errorCode);
                }
            }
        }

        /**
         * Unsubscribe from mouse events. Reset the hook. Throws an error if the unhook operation failed.
         */
        private static void UnsubscribeFromMouseEvents()
        {
            // If no subsribers are registered unsubscribe from hook
            if (MouseClickEvent == null && MouseDownEvent == null && MouseMoveEvent == null &&
                MouseWheelEvent == null && MouseHookHandle != 0)
            {
                // Uninstall hook
                int result = UnhookWindowsHookEx(MouseHookHandle);

                MouseHookHandle = 0;
                MouseHookProcDelegate = null;

                // If failed and exception must be thrown
                if (result == 0)
                {
                    // Returns the error code returned by the last unmanaged function called using platform 
                    // invoke that has the DllImportAttribute.SetLastError flag set. 
                    int errorCode = Marshal.GetLastWin32Error();

                    // Initializes and throws a new instance of the Win32Exception class with 
                    // the specified error. 
                    throw new Win32Exception(errorCode);
                }
            }
        }

        #endregion

        #region Keyboard hook processing

        /**
         * This field is not objectively needed but we need to keep a reference on a delegate which will
         * be passed to unmanaged code. To avoid GC to clean it up. When passing delegates to unmanaged
         * code, they must be kept alive by the managed application until it is guaranteed that they
         * will never be called.
         */
        private static HookProc KeyboardHookProcDelegate;

        /**
         * Stores the handle to the Keyboard hook procedure.
         */
        private static int KeyboardHookHandle;

        /**
         * A callback function which will be called every Time a keyboard activity detected.
         *
         * @author Kovox
         * @date 08/03/2018
         *
         * @param [in] nCode  Specifies whether the hook procedure must process the message. If nCode is
         *                    HC_ACTION, the hook procedure must process the message. If nCode is less
         *                    than zero, the hook procedure must pass the message to the CallNextHookEx
         *                    function without further processing and must return the value returned by
         *                    CallNextHookEx.
         * @param [in] wParam Specifies whether the message was sent by the current thread. If the message
         *                    was sent by the current thread, it is nonzero; otherwise, it is zero.
         * @param [in] lParam Pointer to a CWPSTRUCT structure that contains details about the message.
         *
         * @return If nCode is less than zero, the hook procedure must return the value returned by
         *         CallNextHookEx. If nCode is greater than or equal to zero, it is highly recommended
         *         that you call CallNextHookEx and return the value it returns; otherwise, other
         *         applications that have installed WH_CALLWNDPROC hooks will not receive hook
         *         notifications and may behave incorrectly as a result. If the hook procedure does not
         *         call CallNextHookEx, the return value should be zero.
         */
        private static int KeyboardHookProc(int nCode, Int32 wParam, IntPtr lParam)
        {
            // Indicates if any of underlying events set e.Handled flag
            bool handled = false;

            if (nCode >= 0)
            {
                KeyboardHookStruct keyboardHookStruct =
                    (KeyboardHookStruct)Marshal.PtrToStructure(lParam, typeof(KeyboardHookStruct));

                // Raise KeyDown
                if (wParam == WM_KEYDOWN || wParam == WM_SYSKEYDOWN)
                {
                    Keys key = (Keys)keyboardHookStruct.VirtualKeyCode;
                    KeyEventArgs e = new KeyEventArgs(key);
                    KeyDownEvent?.Invoke(null, e);
                    handled = e.Handled;
                }

                // Raise KeyPress
                if (wParam == WM_KEYDOWN)
                {
                    bool isDownShift = ((GetKeyState(VK_SHIFT) & 0x80) == 0x80 ? true : false);
                    bool isDownCapslock = (GetKeyState(VK_CAPITAL) != 0 ? true : false);

                    byte[] keyState = new byte[256];
                    GetKeyboardState(keyState);
                    byte[] transKey = new byte[2];

                    if (ToAscii(keyboardHookStruct.VirtualKeyCode, keyboardHookStruct.ScanCode,
                        keyState, transKey, keyboardHookStruct.Flags) == 1)
                    {
                        char key = (char)transKey[0];
                        if ((isDownShift ^ isDownCapslock) && Char.IsLetter(key)) key = Char.ToUpper(key);
                        KeyPressEventArgs e = new KeyPressEventArgs(key);
                        KeyPressEvent?.Invoke(null, e);
                        handled = handled || e.Handled;
                    }
                }

                // Raise KeyUp
                if (wParam == WM_KEYUP || wParam == WM_SYSKEYUP)
                {
                    Keys keyData = (Keys)keyboardHookStruct.VirtualKeyCode;
                    KeyEventArgs e = new KeyEventArgs(keyData);
                    KeyUpEvent?.Invoke(null, e);
                    handled = handled || e.Handled;
                }

            }

            // If event handled in application do not handoff to other listeners
            if (handled)
            {
                return -1;
            }

            // Forward to other application
            return CallNextHookEx(KeyboardHookHandle, nCode, wParam, lParam);
        }

        /**
         * Subscribe to keyboard events. It first checks if the subscribe hasn't been done before. If not,
         * then it installs the hook and throws an error if the hook operation failed.
         */
        private static void SubscribeToKeyboardEvents()
        {
            // Install Keyboard hook only if it is not installed and must be installed
            if (KeyboardHookHandle == 0)
            {
                KeyboardHookProcDelegate = KeyboardHookProc;

                // Install hook
                KeyboardHookHandle = SetWindowsHookEx(WH_KEYBOARD_LL, KeyboardHookProcDelegate,
                    GetModuleHandle(Process.GetCurrentProcess().MainModule.ModuleName), 0);

                // If SetWindowsHookEx fails.
                if (KeyboardHookHandle == 0)
                {
                    // Returns the error code returned by the last unmanaged function called using 
                    // platform invoke that has the DllImportAttribute.SetLastError flag set. 
                    int errorCode = Marshal.GetLastWin32Error();

                    // Initializes and throws a new instance of the Win32Exception class 
                    // with the specified error. 
                    throw new Win32Exception(errorCode);
                }
            }
        }

        /**
         * Unsubscribe from keyboard events. Reset the hook. Throws an error if the unhook operation failed.
         */
        private static void UnsubscribeFromKeyboardEvents()
        {
            // If no subsribers are registered unsubsribe from hook
            if (KeyDownEvent == null && KeyUpEvent == null && KeyPressEvent == null && KeyboardHookHandle != 0)
            {
                // Uninstall hook
                int result = UnhookWindowsHookEx(KeyboardHookHandle);

                KeyboardHookHandle = 0;
                KeyboardHookProcDelegate = null;

                // If failed and exception must be thrown
                if (result == 0)
                {
                    // Returns the error code returned by the last unmanaged function called using 
                    // platform invoke that has the DllImportAttribute.SetLastError flag set. 
                    int errorCode = Marshal.GetLastWin32Error();

                    // Initializes and throws a new instance of the Win32Exception 
                    // class with the specified error. 
                    throw new Win32Exception(errorCode);
                }
            }
        }

        #endregion
    }
}