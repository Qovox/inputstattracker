using System;
using System.Windows.Forms;

namespace InputStatTrackerLibrary
{
    /**
     * This class monitors all mouse and keyboard activities and provides appropriate events.
     *
     * @author Kovox
     * @date 07/03/2018
     */
    public static partial class HookManager
    {
        #region Mouse events

        private static event MouseEventHandler MouseMoveEvent;
        private static event MouseEventHandler MouseClickEvent;
        private static event MouseEventHandler MouseDownEvent;
        private static event MouseEventHandler MouseUpEvent;
        private static event MouseEventHandler MouseWheelEvent;
        private static event MouseEventHandler MouseDoubleClickEvent;

        /**
         * Occurs when the mouse pointer is moved.
         */
        public static event MouseEventHandler MouseMove
        {
            add
            {
                SubscribeToMouseEvents();
                MouseMoveEvent += value;
            }

            remove
            {
                MouseMoveEvent -= value;
                UnsubscribeFromMouseEvents();
            }
        }

        /**
         * Occurs when a click was performed by the mouse.
         */
        public static event MouseEventHandler MouseClick
        {
            add
            {
                SubscribeToMouseEvents();
                MouseClickEvent += value;
            }
            remove
            {
                MouseClickEvent -= value;
                UnsubscribeFromMouseEvents();
            }
        }

        /**
         * Occurs when the mouse a mouse button is pressed.
         */
        public static event MouseEventHandler MouseDown
        {
            add
            {
                SubscribeToMouseEvents();
                MouseDownEvent += value;
            }
            remove
            {
                MouseDownEvent -= value;
                UnsubscribeFromMouseEvents();
            }
        }

        /**
         * Occurs when a mouse button is released.
         */
        public static event MouseEventHandler MouseUp
        {
            add
            {
                SubscribeToMouseEvents();
                MouseUpEvent += value;
            }
            remove
            {
                MouseUpEvent -= value;
                UnsubscribeFromMouseEvents();
            }
        }

        /**
         * Occurs when the mouse wheel moves.
         */
        public static event MouseEventHandler MouseWheel
        {
            add
            {
                SubscribeToMouseEvents();
                MouseWheelEvent += value;
            }
            remove
            {
                MouseWheelEvent -= value;
                UnsubscribeFromMouseEvents();
            }
        }

        /**
         * Occurs when a double clicked was performed by the mouse.
         * 
         * The double click event will not be provided directly from hook.
         * To fire the double click event we need to monitor mouse up event and when it occures
         * two times during the time interval which is defined in Windows as a double click time 
         * we fire this event.
         */
        public static event MouseEventHandler MouseDoubleClick
        {
            add
            {
                SubscribeToMouseEvents();

                if (MouseDoubleClickEvent == null)
                {
                    // We create a timer to monitor interval between two clicks
                    DoubleClickTimer = new Timer
                    {
                        // This interval will be set to the value we retrive from windows.
                        // This is a windows setting from contro planel.
                        Interval = GetDoubleClickTime(),

                        // We do not start timer yet. It will be start when the click occures.
                        Enabled = false
                    };

                    // We define the callback function for the timer
                    DoubleClickTimer.Tick += DoubleClickTimeElapsed;

                    // We start to monitor mouse up event.
                    MouseUp += OnMouseUp;
                }

                MouseDoubleClickEvent += value;
            }
            remove
            {
                if (MouseDoubleClickEvent != null)
                {
                    MouseDoubleClickEvent -= value;

                    if (MouseDoubleClickEvent == null)
                    {
                        // Stop monitoring mouse up
                        MouseUp -= OnMouseUp;

                        // Dispose the timer
                        DoubleClickTimer.Tick -= DoubleClickTimeElapsed;
                        DoubleClickTimer = null;
                    }
                }

                UnsubscribeFromMouseEvents();
            }
        }

        /**
         * This field remembers mouse button pressed because in addition to the short 
         * interval it must be also the same button.
         */
        private static MouseButtons PrevClickedButton;

        /**
         * The timer to monitor time interval between two clicks.
         */
        private static Timer DoubleClickTimer;

        /**
         * Triggered when the double click time is elapsed. Reset double click event settings.
         * 
         * @author Kovox
         * @date 07/03/2018
         *
         * @param sender Useless here.
         * @param e      Useless here.
         */
        private static void DoubleClickTimeElapsed(object sender, EventArgs e)
        {
            // Timer is elapsed and no second click occured
            DoubleClickTimer.Enabled = false;
            PrevClickedButton = MouseButtons.None;
        }

        /**
         * This method is designed to monitor mouse clicks in order to fire a double click event if
         * interval between clicks was short enough.
         *
         * @author Kovox
         * @date 07/03/2018
         *
         * @param sender Is always null.
         * @param e      Some information about the click happened.
         */
        private static void OnMouseUp(object sender, MouseEventArgs e)
        {
            // This should not happen
            if (e.Clicks < 1)
            {
                return;
            }

            // If the second click happened on the same button
            if (e.Button.Equals(PrevClickedButton))
            {
                // Fire double click
                MouseDoubleClickEvent?.Invoke(null, e);

                // Stop timer
                DoubleClickTimer.Enabled = false;
                PrevClickedButton = MouseButtons.None;
            }
            else
            {
                // If it was the firt click start the timer
                DoubleClickTimer.Enabled = true;
                PrevClickedButton = e.Button;
            }
        }

        #endregion

        #region Keyboard events

        private static event KeyEventHandler KeyDownEvent;
        private static event KeyPressEventHandler KeyPressEvent;
        private static event KeyEventHandler KeyUpEvent;

        /**
         * Occurs when a key is pressed down.
         */
        public static event KeyEventHandler KeyDown
        {
            add
            {
                SubscribeToKeyboardEvents();
                KeyDownEvent += value;
            }
            remove
            {
                KeyDownEvent -= value;
                UnsubscribeFromKeyboardEvents();
            }
        }

        /**
         * Occurs when a key is pressed.
         *
         * Key events occur in the following order:
         * <list type="number">
         * <item>KeyDown</item>
         * <item>KeyPress</item>
         * <item>KeyUp</item>
         * </list>
         * 
         * The KeyPress event is not raised by noncharacter keys; however, the noncharacter keys do 
         * raise the KeyDown and KeyUp events. Use the KeyChar property to sample keystrokes at run 
         * time and to consume or modify a subset of common keystrokes.
         * 
         * To handle keyboard events only in your application and not enable other applications to 
         * receive keyboard events, set the KeyPressEventArgs. Handled property in your form's 
         * KeyPress event-handling method to <b>true</b>.
         */
        public static event KeyPressEventHandler KeyPress
        {
            add
            {
                SubscribeToKeyboardEvents();
                KeyPressEvent += value;
            }
            remove
            {
                KeyPressEvent -= value;
                UnsubscribeFromKeyboardEvents();
            }
        }

        /**
         * Occurs when a key is released.
         */
        public static event KeyEventHandler KeyUp
        {
            add
            {
                SubscribeToKeyboardEvents();
                KeyUpEvent += value;
            }
            remove
            {
                KeyUpEvent -= value;
                UnsubscribeFromKeyboardEvents();
            }
        }

        #endregion
    }
}