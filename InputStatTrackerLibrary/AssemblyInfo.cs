using System.Resources;
using System.Runtime.InteropServices;
using System.Reflection;

[assembly: AssemblyTitle("InputStatTrackerLibrary")]
[assembly: AssemblyDescription("This is the library containing components which manage all mouse and keyboard activities and provides appropriate events.")]
[assembly: AssemblyCompany("Kovox")]
[assembly: AssemblyProduct("InputStatTracker")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: ComVisible(false)]
[assembly: NeutralResourcesLanguage("en")]