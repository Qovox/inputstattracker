using System.Runtime.InteropServices;

namespace InputStatTrackerLibrary
{
    /**
     * In this part of the HookManager class, we'll find only structures which will contain
     * information from the events about the mouse and the keyboard.
     * 
     * There are 3 structures defined in this part of the class. First structure is a Point.
     * It will be used to represent the coordinates of the mouse on the screen.
     * The second structure is the MouseHookStruct which will contain everything that can be 
     * catch from an event, namely: a Point Struct to know where the mouse was when the event
     * was sent, mouse data to know what was the event about, a flag (unknown utility?), the
     * time when the event raised and finally some extra info.
     * 
     * Finally there's the KeyboardHookStruct which instead of having a Point Struct and mouse
     * data, had a VirtualKeyCode which is nothing else than the key code of the key pressed
     * represented on the virtual keyboard layout (position), and the scan code which is this 
     * time the real (hardware) code of the key.
     *
     * @author Kovox
     * @date 08/03/2018
     */
    public static partial class HookManager
    {
        /**
         * The Point structure defines the x and  coordinates of a point.
         * Note: The [StructLayout(Layout.Sequential)] is a little bit useless/overkill since
         * C# struct implictly have sequential struct.
         *
         * @author Kovox
         * @date 07/03/2018
         */
        [StructLayout(LayoutKind.Sequential)]
        private struct Point
        {
            /**
             * Specifies the x coordinate of the point.
             */
            public int X;

            /**
             * Specifies the y coordinate of the point.
             */
            public int Y;
        }

        /**
         * The MouseHookStruct structure contains information about a low-level mouse input event.
         *
         * @author Kovox
         * @date 07/03/2018
         */
        [StructLayout(LayoutKind.Sequential)]
        private struct MouseHookStruct
        {
            /**
             * Specifies a Point structure that contains the X- and Y-coordinates of the cursor, in screen
             * coordinates.
             */
            public Point Point;

            /**
             * If the message is WM_MOUSEWHEEL, the high-order word of this member is the wheel delta. The
             * low-order word is reserved. A positive value indicates that the wheel was rotated forward,
             * away from the user; a negative value indicates that the wheel was rotated backward, toward
             * the user. One wheel click is defined as WHEEL_DELTA, which is 120. If the message is
             * WM_XBUTTONDOWN, WM_XBUTTONUP, WM_XBUTTONDBLCLK, WM_NCXBUTTONDOWN, WM_NCXBUTTONUP, or
             * WM_NCXBUTTONDBLCLK, the high-order word specifies which X button was pressed or released, and
             * the low-order word is reserved. This value can be one or more of the following values.
             * Otherwise, MouseData is not used. XBUTTON1 The first X button was pressed or released.
             * XBUTTON2 The second X button was pressed or released.
             */
            public int MouseData;

            /**
             * Specifies the event-injected flag. An application can use the following value to test the
             * mouse Flags. Value Purpose LLMHF_INJECTED Test the event-injected flag.
             * 0 Specifies whether the event was injected. The value is 1 if the event was injected;
             * otherwise, it is 0. 1-15 Reserved.
             */
            public int Flags;

            /**
             * Specifies the Time stamp for this message.
             */
            public int Time;

            /**
             * Specifies extra information associated with the message.
             */
            public int ExtraInfo;
        }

        /**
         * The KeyboardHookStruct contains information about a low-level keyboard input event.
         *
         * @author Kovox
         * @date 07/03/2018
         */
        [StructLayout(LayoutKind.Sequential)]
        private struct KeyboardHookStruct
        {
            /**
             * Specifies a virtual-key code. The code must be a value in the range 1 to 254.
             */
            public int VirtualKeyCode;

            /**
             * Specifies a hardware scan code for the key.
             */
            public int ScanCode;

            /** 
             * Specifies the extended-key flag, event-injected flag, context code, and transition-state flag.
             */
            public int Flags;

            /**
             * Specifies the Time stamp for this message.
             */
            public int Time;

            /**
             * Specifies extra information associated with the message.
             */
            public int ExtraInfo;
        }
    }
}