using System;
using System.Runtime.InteropServices;

namespace InputStatTrackerLibrary
{
    /**
     * This part of the HookManager class ensures to get everything needed to retrieve all
     * the important values and methods to correctly hook to mouse and keyboard devices.
     * 
     * There will be windows constants which contain mouse constants as well as keyboard
     * constants. There will also be windows native functions usually found in user32.dll.
     * Those functions will be used to retrieve the correct information about the keyboard
     * state for example, or more importantly to hook and unhook to hook procedures.
     *
     * @author Kovox
     * @date 08/03/2018
     */
    public static partial class HookManager
    {
        #region Windows constants

        // Values from Winuser.h in Microsoft SDK.

        #region Mouse constants

        /**
         * The WH_MOUSE_LL hook enables to monitor mouse input events about 
         * to be posted in a thread input queue.
         * Installs a hook procedure that monitors low-level mouse input events.
         */
        private const int WH_MOUSE_LL = 14;

        /**
         * The WH_MOUSE hook enables you to monitor mouse messages about to be returned by 
         * the GetMessage or PeekMessage function. The WH_MOUSE hook can be used to 
         * monitor mouse input posted to a message queue.
         * For more information, see the MouseProc callback function.
         * 
         * Installs a hook procedure that monitors mouse messages. For more information, see the
         * MouseProc hook procedure.
         */
        private const int WH_MOUSE = 7;

        /**
         * The WM_MOUSEMOVE message is posted to a window when the cursor moves.
         */
        private const int WM_MOUSEMOVE = 0x0200;

        /**
         * The WM_LBUTTONDOWN message is posted when the user presses the left mouse button.
         */
        private const int WM_LBUTTONDOWN = 0x0201;

        /**
         * The WM_LBUTTONUP message is posted when the user releases the left mouse button.
         */
        private const int WM_LBUTTONUP = 0x0202;

        /**
         * The WM_LBUTTONDBLCLK message is posted when the user double-clicks the left mouse button.
         */
        private const int WM_LBUTTONDBLCLK = 0x0203;

        /**
         * The WM_RBUTTONDOWN message is posted when the user presses the right mouse button.
         */
        private const int WM_RBUTTONDOWN = 0x0204;

        /**
         * The WM_RBUTTONUP message is posted when the user releases the right mouse button.
         */
        private const int WM_RBUTTONUP = 0x0205;

        /**
         * The WM_RBUTTONDBLCLK message is posted when the user double-clicks the right mouse button.
         */
        private const int WM_RBUTTONDBLCLK = 0x0206;

        /**
         * The WM_MBUTTONDOWN message is posted when the user presses the middle mouse button.
         */
        private const int WM_MBUTTONDOWN = 0x0207;

        /**
         * The WM_MBUTTONUP message is posted when the user releases the middle mouse button.
         */
        private const int WM_MBUTTONUP = 0x0208;

        /**
         * The WM_MBUTTONDBLCLK message is posted when the user double-clicks the middle mouse button.
         */
        private const int WM_MBUTTONDBLCLK = 0x0209;

        /**
         * The WM_MOUSEWHEEL message is posted when the user rolls (forward or backward) the mouse wheel.
         */
        private const int WM_MOUSEWHEEL = 0x020A;

        /**
         * The WM_XBUTTONDOWN message is posted when the user presses the x mouse button.
         */
        private const int WM_XBUTTONDOWN = 0x020B;

        /**
         * The WM_XBUTTONUP message is posted when the user releases the x mouse button.
         */
        private const int WM_XBUTTONUP = 0x020C;

        /**
         * The WM_XBUTTONDBLCLK message is posted when the user double-clicks the x mouse button.
         */
        private const int WM_XBUTTONDBLCLK = 0x020D;

        /**
         * The XBUTTON1 represents the first x mouse button.
         */
        private const int XBUTTON1 = 0x0001;

        /**
         * The XBUTTON2 represents the second x mouse button.
         */
        private const int XBUTTON2 = 0x0002;

        #endregion

        #region Keyboard constants

        /**
         * The WH_KEYBOARD_LL hook enables to monitor keyboard input events about 
         * to be posted in a thread input queue. 
         * Installs a hook procedure that monitors low-level keyboard input events.
         */
        private const int WH_KEYBOARD_LL = 13;

        /**
         * The WH_KEYBOARD hook enables an application to monitor message traffic for 
         * WM_KEYDOWN and WM_KEYUP messages about to be returned by the GetMessage or 
         * PeekMessage function. The WH_KEYBOARD hook can be used to monitor keyboard input 
         * posted to a message queue.
         * For more information, see the KeyboardProc callback function. 
         * 
         * Installs a hook procedure that monitors keystroke messages. For more information, see the
         * KeyboardProc hook procedure.
         */
        private const int WH_KEYBOARD = 2;

        /**
         * The WM_KEYDOWN message is posted to the window with the keyboard focus when a nonsystem key
         * is pressed. A nonsystem key is a key that is pressed when the ALT key is not pressed.
         */
        private const int WM_KEYDOWN = 0x100;

        /**
         * The WM_KEYUP message is posted to the window with the keyboard focus when a nonsystem key is
         * released. A nonsystem key is a key that is pressed when the ALT key is not pressed, or a
         * keyboard key that is pressed when a window has the keyboard focus.
         */
        private const int WM_KEYUP = 0x101;

        /**
         * The WM_SYSKEYDOWN message is posted to the window with the keyboard focus when the user
         * presses the F10 key (which activates the menu bar) or holds down the ALT key and then presses
         * another key. It also occurs when no window currently has the keyboard focus;
         * in this case, the WM_SYSKEYDOWN message is sent to the active window. The window that
         * receives the message can distinguish between these two contexts by checking the context code
         * in the lParam parameter.
         */
        private const int WM_SYSKEYDOWN = 0x104;

        /**
         * The WM_SYSKEYUP message is posted to the window with the keyboard focus when the user
         * releases a key that was pressed while the ALT key was held down. It also occurs when no
         * window currently has the keyboard focus; in this case, the WM_SYSKEYUP message is sent to the
         * active window. The window that receives the message can distinguish between these two
         * contexts by checking the context code in the lParam parameter.
         */
        private const int WM_SYSKEYUP = 0x105;

        /**
         * Any Shift Key
         */
        private const byte VK_SHIFT = 0x10;

        /**
         * Capital (Caps Lock) Key
         */
        private const byte VK_CAPITAL = 0x14;

        /**
         * Numlock Key
         */
        private const byte VK_NUMLOCK = 0x90;

        #endregion

        #endregion

        #region Windows function imports

        /**
         * The CallNextHookEx function passes the hook information to the next hook procedure in the
         * current hook chain. A hook procedure can call this function either before or after processing
         * the hook information.
         *
         * @author Kovox
         * @date 07/03/2018
         *
         * @param      idHook Ignored.
         * @param [in] nCode  Specifies the hook code passed to the current hook procedure. The next hook
         *                    procedure uses this code to determine how to process the hook information.
         * @param [in] wParam Specifies the wParam value passed to the current hook procedure. The meaning
         *                    of this parameter depends on the type of hook associated with the current
         *                    hook chain.
         * @param [in] lParam Specifies the lParam value passed to the current hook procedure. The meaning
         *                    of this parameter depends on the type of hook associated with the current
         *                    hook chain.
         *
         * @return The value is returned by the next hook procedure in the chain. The current hook
         *         procedure must also return this value. The meaning of the return value depends on the
         *         hook type. For more information, see the descriptions of the individual hook
         *         procedures.
         */
        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        private static extern int CallNextHookEx(int idHook, int nCode, int wParam, IntPtr lParam);

        /**
         * The SetWindowsHookEx function installs an application-defined hook procedure into a hook
         * chain. You would install a hook procedure to monitor the system for certain types of events.
         * These events are associated either with a specific thread or with all threads in the same
         * desktop as the calling thread.
         *
         * @author Kovox
         * @date 07/03/2018
         *
         * @param [in] idHook     Specifies the type of hook procedure to be installed. This parameter can
         *                        be one of the following values.
         * @param [in] lpfn       Pointer to the hook procedure. If the dwThreadId parameter is zero or
         *                        specifies the identifier of a thread created by a different process,
         *                        the lpfn parameter must point to a hook procedure in a dynamic-link
         *                        library (DLL). Otherwise, lpfn can point to a hook procedure in the
         *                        code associated with the current process.
         * @param [in] hMod       Handle to the DLL containing the hook procedure pointed to by the lpfn
         *                        parameter. The hMod parameter must be set to NULL if the dwThreadId
         *                        parameter specifies a thread created by the current process and if
         *                        the hook procedure is within the code associated with the current
         *                        process.
         * @param [in] dwThreadId Specifies the identifier of the thread with which the hook procedure is
         *                        to be associated. If this parameter is zero, the hook procedure is
         *                        associated with all existing threads running in the same desktop as
         *                        the calling thread.
         *
         * @return If the function succeeds, the return value is the handle to the hook procedure. If
         *         the function fails, the return value is NULL. To get extended error information, call
         *         GetLastError.
         */
        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall,
            SetLastError = true)]
        private static extern int SetWindowsHookEx(int idHook, HookProc lpfn, IntPtr hMod, int dwThreadId);

        /**
         * The UnhookWindowsHookEx function removes a hook procedure installed in a hook chain by the
         * SetWindowsHookEx function.
         *
         * @author Kovox
         * @date 07/03/2018
         *
         * @param [in] idHook Handle to the hook to be removed. This parameter is a hook handle obtained by
         *                    a previous call to SetWindowsHookEx.
         *
         * @return If the function succeeds, the return value is nonzero. If the function fails, the
         *         return value is zero. To get extended error information, call GetLastError.
         */
        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall,
            SetLastError = true)]
        private static extern int UnhookWindowsHookEx(int idHook);

        /**
         * The GetDoubleClickTime function retrieves the current double-click time for the mouse. A
         * double-click is a series of two clicks of the mouse button, the second occurring within a
         * specified time after the first. The double-click time is the maximum number of milliseconds
         * that may occur between the first and second click of a double-click.
         *
         * @author Kovox
         * @date 07/03/2018
         *
         * @return The return value specifies the current double-click time, in milliseconds.
         */
        [DllImport("user32")]
        public static extern int GetDoubleClickTime();

        /**
         * The ToAscii function translates the specified virtual-key code and keyboard state to the
         * corresponding character or characters. The function translates the code using the input
         * language and physical keyboard layout identified by the keyboard layout handle.
         *
         * @author Kovox
         * @date 07/03/2018
         *
         * @param [in]  uVirtKey    Specifies the virtual-key code to be translated.
         * @param [in]  uScanCode   Specifies the hardware scan code of the key to be translated. The
         *                          high-order bit of this value is set if the key is up (not pressed).
         * @param [in]  lpbKeyState Pointer to a 256-byte array that contains the current keyboard state.
         *                          Each element (byte) in the array contains the state of one key. If
         *                          the high-order bit of a byte is set, the key is down (pressed). The
         *                          low bit, if set, indicates that the key is toggled on. In this
         *                          function, only the toggle bit of the CAPS LOCK key is relevant. The
         *                          toggle state of the NUM LOCK and SCROLL LOCK keys is ignored.
         * @param [out] lpwTransKey Pointer to the buffer that receives the translated character or
         *                          characters.
         * @param [in]  fuState     Specifies whether a menu is active. This parameter must be 1 if a
         *                          menu is active, or 0 otherwise.
         *
         * @return If the specified key is a dead key, the return value is negative. Otherwise, it is
         *         one of the following values. Value Meaning 0 The specified virtual key has no
         *         translation for the current state of the keyboard. 1 One character was copied to the
         *         buffer. 2 Two characters were copied to the buffer. This usually happens when a dead-
         *         key character (accent or diacritic) stored in the keyboard layout cannot be composed
         *         with the specified virtual key to form a single character.
         */
        [DllImport("user32")]
        private static extern int ToAscii(int uVirtKey, int uScanCode, byte[] lpbKeyState,
            byte[] lpwTransKey, int fuState);

        /**
         * The GetKeyboardState function copies the status of the 256 virtual keys to the specified
         * buffer.
         *
         * @author Kovox
         * @date 07/03/2018
         *
         * @param [in] pbKeyState Pointer to a 256-byte array that contains keyboard key states.
         *
         * @return If the function succeeds, the return value is nonzero. If the function fails, the
         *         return value is zero. To get extended error information, call GetLastError.
         */
        [DllImport("user32")]
        private static extern int GetKeyboardState(byte[] pbKeyState);

        /**
         * The GetKeyState function retrieves the status of the specified virtual key. The status
         * specifies whether the key is up, down, or toggled (on, off�alternating each time the key is
         * pressed).
         *
         * @author Kovox
         * @date 07/03/2018
         *
         * @param [in] vKey Specifies a virtual key. If the desired virtual key is a letter or digit (A
         *                  through Z, a through z, or 0 through 9), nVirtKey must be set to the ASCII
         *                  value of that character. For other keys, it must be a virtual-key code.
         *
         * @return The return value specifies the status of the specified virtual key, as follows: If
         *         the high-order bit is 1, the key is down; otherwise, it is up. If the low-order bit
         *         is 1, the key is toggled. A key, such as the CAPS LOCK key, is toggled if it is
         *         turned on. The key is off and untoggled if the low-order bit is 0. A toggle key's
         *         indicator light (if any) on the keyboard will be on when the key is toggled, and off
         *         when the key is untoggled.
         */
        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        private static extern short GetKeyState(int vKey);

        /**
         * Gets module handle to avoid having Unfound module in .NET Framework 4.0.
         *
         * @author Kovox
         * @date 08/03/2018
         *
         * @param name The name.
         *
         * @return The module handle.
         */
        [DllImport("kernel32.dll")]
        public static extern IntPtr GetModuleHandle(string name);

        #endregion
    }
}