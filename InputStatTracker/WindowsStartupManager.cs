﻿using Microsoft.Win32;
using System;
using System.Security.Principal;

namespace InputStatTracker
{
    public class WindowsStartupManager
    {
        /**
         * Add application on Windows Startup to the registry only for the current user.
         */
        public static void AddApplicationToCurrentUserStartup()
        {
            using (RegistryKey key = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true))
            {
                key.SetValue("InputStatTrackerApplication", "\"" + System.Reflection.Assembly.GetExecutingAssembly().Location + "\"");
            }
        }

        /**
         * Add application on Windows Startup to the registry for all users.
         */
        public static void AddApplicationToAllUserStartup()
        {
            using (RegistryKey key = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true))
            {
                key.SetValue("InputStatTrackerApplication", "\"" + System.Reflection.Assembly.GetExecutingAssembly().Location + "\"");
            }
        }

        /**
         * Remove application on Windows Startup from the registry only for the current user.
         */
        public static void RemoveApplicationFromCurrentUserStartup()
        {
            using (RegistryKey key = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true))
            {
                key.DeleteValue("InputStatTrackerApplication", false);
            }
        }

        /**
         * Remove application on Windows Startup from the registry for all users.
         */
        public static void RemoveApplicationFromAllUserStartup()
        {
            using (RegistryKey key = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true))
            {
                key.DeleteValue("InputStatTrackerApplication", false);
            }
        }

        /**
         * Check wheter the user is an administrator or not.
         */
        public static bool IsUserAdministrator()
        {
            bool isAdmin;

            try
            {
                // Get the currently logged in user
                WindowsIdentity user = WindowsIdentity.GetCurrent();
                WindowsPrincipal principal = new WindowsPrincipal(user);
                isAdmin = principal.IsInRole(WindowsBuiltInRole.Administrator);
            }
            catch (UnauthorizedAccessException e)
            {
                isAdmin = false;
            }
            catch (Exception e)
            {
                isAdmin = false;
            }
            return isAdmin;
        }
    }
}