﻿using InputStatTrackerApplication;
using InputStatTrackerLibrary;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace InputStatTracker
{
    #region Blur Activation Content

    internal enum AccentState
    {
        ACCENT_DISABLED = 1,
        ACCENT_ENABLE_GRADIENT = 0,
        ACCENT_ENABLE_TRANSPARENTGRADIENT = 2,
        ACCENT_ENABLE_BLURBEHIND = 3,
        ACCENT_INVALID_STATE = 4
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct AccentPolicy
    {
        public AccentState AccentState;
        public int AccentFlags;
        public int GradientColor;
        public int AnimationId;
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct WindowCompositionAttributeData
    {
        public WindowCompositionAttribute Attribute;
        public IntPtr Data;
        public int SizeOfData;
    }

    internal enum WindowCompositionAttribute
    {
        // ...
        WCA_ACCENT_POLICY = 19
        // ...
    }

    #endregion

    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const string MOUSE_STATS = "mouse_stats";
        private const string KEYBOARD_STATS = "keyboard_stats";
        private const string USER_SETTINGS = "user_settings";

        private static MouseStats mouseStats;
        private static KeyBoardStats keyBoardStats;
        private static UserSettings userSettings;

        private static NotifyIcon trayIcon;
        private static DispatcherTimer MainTimer;

        private ObservableCollection<SpeedListViewItem> SpeedClickListViewItems;
        private ObservableCollection<StatsListViewItem> MouseStatsListViewItems;
        private ObservableCollection<StatsListViewItem> KeyboardStatsListViewItems;

        private Grid LastGrid;
        private string LastImageBrush;

        // private KeyboardHook keyboardHook = new KeyboardHook();

        private int speedIndex = 0;
        private State speedClickState = State.RESTART;
        private enum State { RUNNING, STOPPED, RESTART }

        private double timeCounter = 0;
        private DispatcherTimer speedTimer;
        private Stopwatch speedDisplayTimer;

        #region Static Image Resource

        private static readonly ImageBrush HomeButton_ImageBrush = new ImageBrush(new BitmapImage(new Uri("pack://application:,,,/InputStatTracker;component/Resources/HomeButton.png", UriKind.Absolute)));
        private static readonly ImageBrush HomeButtonClicked_ImageBrush = new ImageBrush(new BitmapImage(new Uri("pack://application:,,,/InputStatTracker;component/Resources/HomeButton_Clicked.png", UriKind.Absolute)));

        private static readonly ImageBrush SpeedButton_ImageBrush = new ImageBrush(new BitmapImage(new Uri("pack://application:,,,/InputStatTracker;component/Resources/SpeedButton.png", UriKind.Absolute)));
        private static readonly ImageBrush SpeedButtonClicked_ImageBrush = new ImageBrush(new BitmapImage(new Uri("pack://application:,,,/InputStatTracker;component/Resources/SpeedButton_Clicked.png", UriKind.Absolute)));

        private static readonly ImageBrush SettingsButton_ImageBrush = new ImageBrush(new BitmapImage(new Uri("pack://application:,,,/InputStatTracker;component/Resources/SettingsButton.png", UriKind.Absolute)));
        private static readonly ImageBrush SettingsButtonClicked_ImageBrush = new ImageBrush(new BitmapImage(new Uri("pack://application:,,,/InputStatTracker;component/Resources/SettingsButton_Clicked.png", UriKind.Absolute)));

        private static readonly ImageBrush MouseStatsButton_ImageBrush = new ImageBrush(new BitmapImage(new Uri("pack://application:,,,/InputStatTracker;component/Resources/MouseStatsButton.png", UriKind.Absolute)));
        private static readonly ImageBrush MouseStatsButtonClicked_ImageBrush = new ImageBrush(new BitmapImage(new Uri("pack://application:,,,/InputStatTracker;component/Resources/MouseStatsButton_Clicked.png", UriKind.Absolute)));

        private static readonly ImageBrush KeyboardStatsButton_ImageBrush = new ImageBrush(new BitmapImage(new Uri("pack://application:,,,/InputStatTracker;component/Resources/KeyboardStatsButton.png", UriKind.Absolute)));
        private static readonly ImageBrush KeyboardStatsButtonClicked_ImageBrush = new ImageBrush(new BitmapImage(new Uri("pack://application:,,,/InputStatTracker;component/Resources/KeyboardStatsButton_Clicked.png", UriKind.Absolute)));

        #endregion

        [DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);

        [DllImport("user32.dll")]
        internal static extern int SetWindowCompositionAttribute(IntPtr hwnd, ref WindowCompositionAttributeData data);

        public MainWindow()
        {
            InitializeComponent();

            // Setup System Tray Icon.
            var iconHandle = Properties.Resources.IST_Icon_48x48_png.GetHicon();

            trayIcon = new NotifyIcon
            {
                Icon = System.Drawing.Icon.FromHandle(iconHandle),
                Text = "InputStatTracker Application",
                ContextMenu = new System.Windows.Forms.ContextMenu()
            };

            DeleteObject(iconHandle);

            trayIcon.DoubleClick +=
                delegate (object sender, EventArgs args)
                {
                    Show();
                    WindowState = WindowState.Normal;
                    trayIcon.Visible = false;
                };

            // Setup Tray Icon Context Menu.
            System.Windows.Forms.MenuItem exitItem = new System.Windows.Forms.MenuItem();

            trayIcon.ContextMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] { exitItem });

            exitItem.Index = 0;
            exitItem.Text = "E&xit";
            exitItem.Click += new EventHandler(ExitItem_Click);

            // Setup elements properties
            HomeButton_ImageBrush.Stretch = Stretch.Fill;
            HomeButtonClicked_ImageBrush.Stretch = Stretch.Fill;
            SpeedButton_ImageBrush.Stretch = Stretch.Fill;
            SpeedButtonClicked_ImageBrush.Stretch = Stretch.Fill;
            SettingsButton_ImageBrush.Stretch = Stretch.Fill;
            SettingsButtonClicked_ImageBrush.Stretch = Stretch.Fill;
            MouseStatsButton_ImageBrush.Stretch = Stretch.None;
            MouseStatsButtonClicked_ImageBrush.Stretch = Stretch.None;
            KeyboardStatsButton_ImageBrush.Stretch = Stretch.None;
            KeyboardStatsButtonClicked_ImageBrush.Stretch = Stretch.None;

            // Setup elements visibility
            MouseStatsGrid.Visibility = Visibility.Hidden;
            KeyboardStatsGrid.Visibility = Visibility.Hidden;
            SpeedClickGrid.Visibility = Visibility.Hidden;
            SettingsGrid.Visibility = Visibility.Hidden;
            DataSavingLabel.Visibility = Visibility.Hidden;
            LastGrid = OverwiewStatsGrid;
            LastImageBrush = "None";

            // Initializes and / or load data.
            Startup();

            // Timer setup for periodical data saving.
            MainTimer = new DispatcherTimer();
            MainTimer.Tick += new EventHandler(OnTimer);
            MainTimer.Interval = new TimeSpan(0, 3, 0);
            MainTimer.Start();

            SetupSpeedTimer();

            // Binding the hooks.
            HookManager.MouseMove += HookManager_MouseMove;
            HookManager.MouseDown += HookManager_MouseDown;
            HookManager.MouseDoubleClick += HookManager_MouseDoubleClick;
            HookManager.MouseWheel += HookManager_MouseWheel;
            // HookManager.KeyDown += HookManager_KeyDown;
            // HookManager.KeyPress += HookManager_KeyPress;
            // HookManager.KeyUp += HookManager_KeyUp;
            // keyboardHook.KeyDown += new RawKeyEventHandler(HookManager_KeyDown);
            // keyboardHook.KeyDown += new RawKeyEventHandler(HookManager_KeyUp);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            EnableBlur();
        }

        internal void EnableBlur()
        {
            var windowHelper = new WindowInteropHelper(this);

            var accent = new AccentPolicy
            {
                AccentState = AccentState.ACCENT_ENABLE_BLURBEHIND
            };

            var accentStructSize = Marshal.SizeOf(accent);

            var accentPtr = Marshal.AllocHGlobal(accentStructSize);
            Marshal.StructureToPtr(accent, accentPtr, false);

            var data = new WindowCompositionAttributeData
            {
                Attribute = WindowCompositionAttribute.WCA_ACCENT_POLICY,
                SizeOfData = accentStructSize,
                Data = accentPtr
            };

            SetWindowCompositionAttribute(windowHelper.Handle, ref data);

            Marshal.FreeHGlobal(accentPtr);
        }

        #region Data Management

        /**
         * Initialize the data when the application is starting.
         */
        private void Startup()
        {
            // Fetch user settings
            if (File.Exists(USER_SETTINGS) && new FileInfo(USER_SETTINGS).Length != 0)
            {
                userSettings = new UserSettings(JObject.Parse(ReadFile(USER_SETTINGS)));
            }
            else
            {
                userSettings = new UserSettings();
            }

            // Fetch mouse data
            if (File.Exists(MOUSE_STATS) && new FileInfo(MOUSE_STATS).Length != 0)
            {
                mouseStats = new MouseStats(JObject.Parse(ReadFile(MOUSE_STATS)));
            }
            else
            {
                mouseStats = new MouseStats();
            }

            // Fetch keyboard data
            if (File.Exists(KEYBOARD_STATS) && new FileInfo(KEYBOARD_STATS).Length != 0)
            {
                keyBoardStats = new KeyBoardStats(JObject.Parse(ReadFile(KEYBOARD_STATS)));
            }
            else
            {
                keyBoardStats = new KeyBoardStats();
            }

            InitSpeedClickListView();
            InitMouseStatsListView();
            InitKeyboardStatsListView();
            InitSettings();
        }

        #region Initializing ListViews

        public void InitSpeedClickListView()
        {
            SpeedClickListViewItems = new ObservableCollection<SpeedListViewItem>
            {
                /*new SpeedListViewItem() { Date = DateTime.Now.ToString("dd/MM/yyyy HH:mm"),
                    Clicks = Clicks.ToString(), Timer = SetTimerSlider.Value.ToString(),
                    Cps = (float.Parse(Clicks.ToString()) / SetTimerSlider.Value).ToString(),
                    Index = speedIndex++ }*/
            };

            SpeedClickListView.ItemsSource = SpeedClickListViewItems;
        }

        public void InitMouseStatsListView()
        {
            int index = 0;

            MouseStatsListViewItems = new ObservableCollection<StatsListViewItem>
            {
                new StatsListViewItem() { Type = "Mouse Left Clicks", Count = mouseStats.Mapping["Left"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Mouse Right Clicks", Count = mouseStats.Mapping["Right"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Mouse Middle Clicks", Count = mouseStats.Mapping["Middle"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Mouse X Button 1 Clicks", Count = mouseStats.Mapping["XButton1"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Mouse X Button 2 Clicks", Count = mouseStats.Mapping["XButton2"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Mouse Left Double Clicks", Count = mouseStats.Mapping["LeftDblClk"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Mouse Right Double Clicks", Count = mouseStats.Mapping["RightDblClk"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Mouse Middle Double Clicks", Count = mouseStats.Mapping["MiddleDblClk"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Mouse X Button 1 Double Clicks", Count = mouseStats.Mapping["XButton1DblClk"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Mouse X Button 2 Double Clicks", Count = mouseStats.Mapping["XButton2DblClk"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Mouse Wheel Back Scroll", Count = mouseStats.Mapping["WheelBackScroll"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Mouse Wheel Forward Scroll", Count = mouseStats.Mapping["WheelForwardScroll"].ToString(), Index = index }
            };

            MouseStatsListView.ItemsSource = MouseStatsListViewItems;
        }

        public void InitKeyboardStatsListView()
        {
            int index = 0;

            KeyboardStatsListViewItems = new ObservableCollection<StatsListViewItem>
            {
                new StatsListViewItem() { Type = "Cancel", Count = keyBoardStats.Mapping["Cancel"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Back", Count = keyBoardStats.Mapping["Back"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Tab", Count = keyBoardStats.Mapping["Tab"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Clear", Count = keyBoardStats.Mapping["Clear"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Return", Count = keyBoardStats.Mapping["Return"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Shift", Count = keyBoardStats.Mapping["Shift"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Control", Count = keyBoardStats.Mapping["Control"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Menu", Count = keyBoardStats.Mapping["Menu"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Pause", Count = keyBoardStats.Mapping["Pause"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Capital", Count = keyBoardStats.Mapping["Capital"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Kana", Count = keyBoardStats.Mapping["Kana"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Junja", Count = keyBoardStats.Mapping["Junja"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Final", Count = keyBoardStats.Mapping["Final"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Hanja", Count = keyBoardStats.Mapping["Hanja"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Kanji", Count = keyBoardStats.Mapping["Kanji"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Escape", Count = keyBoardStats.Mapping["Escape"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Convert", Count = keyBoardStats.Mapping["Convert"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "NonConvert", DisplayedName = "Non Convert", Count = keyBoardStats.Mapping["NonConvert"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Accept", Count = keyBoardStats.Mapping["Accept"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "ModeChange", DisplayedName = "Mode Change", Count = keyBoardStats.Mapping["ModeChange"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Space", Count = keyBoardStats.Mapping["Space"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "PageUp", DisplayedName = "Page Up", Count = keyBoardStats.Mapping["PageUp"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Next", Count = keyBoardStats.Mapping["Next"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "End", Count = keyBoardStats.Mapping["End"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Home", Count = keyBoardStats.Mapping["Home"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Left", Count = keyBoardStats.Mapping["Left"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Up", Count = keyBoardStats.Mapping["Up"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Right", Count = keyBoardStats.Mapping["Right"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Down", Count = keyBoardStats.Mapping["Down"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Select", Count = keyBoardStats.Mapping["Select"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "PrintScreen", DisplayedName = "Print Screen", Count = keyBoardStats.Mapping["PrintScreen"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Execute", Count = keyBoardStats.Mapping["Execute"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Snapshot", Count = keyBoardStats.Mapping["Snapshot"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Insert", Count = keyBoardStats.Mapping["Insert"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Delete", Count = keyBoardStats.Mapping["Delete"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Help", Count = keyBoardStats.Mapping["Help"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "D0",  DisplayedName = "0", Count = keyBoardStats.Mapping["D0"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "D1", DisplayedName = "1", Count = keyBoardStats.Mapping["D1"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "D2", DisplayedName = "2", Count = keyBoardStats.Mapping["D2"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "D3", DisplayedName = "3", Count = keyBoardStats.Mapping["D3"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "D4", DisplayedName = "4", Count = keyBoardStats.Mapping["D4"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "D5", DisplayedName = "5", Count = keyBoardStats.Mapping["D5"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "D6", DisplayedName = "6", Count = keyBoardStats.Mapping["D6"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "D7", DisplayedName = "7", Count = keyBoardStats.Mapping["D7"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "D8", DisplayedName = "8", Count = keyBoardStats.Mapping["D8"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "D9", DisplayedName = "9", Count = keyBoardStats.Mapping["D9"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "A", Count = keyBoardStats.Mapping["A"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "B", Count = keyBoardStats.Mapping["B"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "C", Count = keyBoardStats.Mapping["C"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "D", Count = keyBoardStats.Mapping["D"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "E", Count = keyBoardStats.Mapping["E"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "F", Count = keyBoardStats.Mapping["F"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "G", Count = keyBoardStats.Mapping["G"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "H", Count = keyBoardStats.Mapping["H"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "I", Count = keyBoardStats.Mapping["I"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "J", Count = keyBoardStats.Mapping["J"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "K", Count = keyBoardStats.Mapping["K"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "L", Count = keyBoardStats.Mapping["L"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "M", Count = keyBoardStats.Mapping["M"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "N", Count = keyBoardStats.Mapping["N"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "O", Count = keyBoardStats.Mapping["O"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "P", Count = keyBoardStats.Mapping["P"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Q", Count = keyBoardStats.Mapping["Q"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "R", Count = keyBoardStats.Mapping["R"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "S", Count = keyBoardStats.Mapping["S"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "T", Count = keyBoardStats.Mapping["T"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "U", Count = keyBoardStats.Mapping["U"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "V", Count = keyBoardStats.Mapping["V"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "W", Count = keyBoardStats.Mapping["W"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "X", Count = keyBoardStats.Mapping["X"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Y", Count = keyBoardStats.Mapping["Y"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Z", Count = keyBoardStats.Mapping["Z"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "LWin", DisplayedName = "Left Win", Count = keyBoardStats.Mapping["LWin"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "RWin", DisplayedName = "Right Win", Count = keyBoardStats.Mapping["RWin"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Apps", Count = keyBoardStats.Mapping["Apps"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Sleep", Count = keyBoardStats.Mapping["Sleep"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "NumPad0", DisplayedName = "NumPad 0", Count = keyBoardStats.Mapping["NumPad0"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "NumPad1", DisplayedName = "NumPad 1", Count = keyBoardStats.Mapping["NumPad1"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "NumPad2", DisplayedName = "NumPad 2", Count = keyBoardStats.Mapping["NumPad2"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "NumPad3", DisplayedName = "NumPad 3", Count = keyBoardStats.Mapping["NumPad3"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "NumPad4", DisplayedName = "NumPad 4", Count = keyBoardStats.Mapping["NumPad4"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "NumPad5", DisplayedName = "NumPad 5", Count = keyBoardStats.Mapping["NumPad5"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "NumPad6", DisplayedName = "NumPad 6", Count = keyBoardStats.Mapping["NumPad6"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "NumPad7", DisplayedName = "NumPad 7", Count = keyBoardStats.Mapping["NumPad7"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "NumPad8", DisplayedName = "NumPad 8", Count = keyBoardStats.Mapping["NumPad8"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "NumPad9", DisplayedName = "NumPad 9", Count = keyBoardStats.Mapping["NumPad9"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Multiply", Count = keyBoardStats.Mapping["Multiply"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Add", Count = keyBoardStats.Mapping["Add"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Separator", Count = keyBoardStats.Mapping["Separator"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Subtract", Count = keyBoardStats.Mapping["Subtract"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Decimal", Count = keyBoardStats.Mapping["Decimal"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Divide", Count = keyBoardStats.Mapping["Divide"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "F1", Count = keyBoardStats.Mapping["F1"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "F2", Count = keyBoardStats.Mapping["F2"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "F3", Count = keyBoardStats.Mapping["F3"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "F4", Count = keyBoardStats.Mapping["F4"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "F5", Count = keyBoardStats.Mapping["F5"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "F6", Count = keyBoardStats.Mapping["F6"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "F7", Count = keyBoardStats.Mapping["F7"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "F8", Count = keyBoardStats.Mapping["F8"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "F9", Count = keyBoardStats.Mapping["F9"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "F10", Count = keyBoardStats.Mapping["F10"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "F11", Count = keyBoardStats.Mapping["F11"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "F12", Count = keyBoardStats.Mapping["F12"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "F13", Count = keyBoardStats.Mapping["F13"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "F14", Count = keyBoardStats.Mapping["F14"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "F15", Count = keyBoardStats.Mapping["F15"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "F16", Count = keyBoardStats.Mapping["F16"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "F17", Count = keyBoardStats.Mapping["F17"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "F18", Count = keyBoardStats.Mapping["F18"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "F19", Count = keyBoardStats.Mapping["F19"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "F20", Count = keyBoardStats.Mapping["F20"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "F21", Count = keyBoardStats.Mapping["F21"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "F22", Count = keyBoardStats.Mapping["F22"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "F23", Count = keyBoardStats.Mapping["F23"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "F24", Count = keyBoardStats.Mapping["F24"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "NumLock", Count = keyBoardStats.Mapping["NumLock"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Scroll", Count = keyBoardStats.Mapping["Scroll"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "LShiftKey", DisplayedName = "Left Shift Key", Count = keyBoardStats.Mapping["LShiftKey"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "RShiftKey", DisplayedName = "Right Shift Key", Count = keyBoardStats.Mapping["RShiftKey"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "LControlKey", DisplayedName = "Left Control Key", Count = keyBoardStats.Mapping["LControlKey"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "RControlKey", DisplayedName = "Right Control Key", Count = keyBoardStats.Mapping["RControlKey"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "LMenu", DisplayedName = "Left Menu", Count = keyBoardStats.Mapping["LMenu"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "RMenu", DisplayedName = "Right Menu", Count = keyBoardStats.Mapping["RMenu"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "BrowserBack", DisplayedName = "Browser Back", Count = keyBoardStats.Mapping["BrowserBack"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "BrowserForward", DisplayedName = "Browser Forward", Count = keyBoardStats.Mapping["BrowserForward"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "BrowserRefresh", DisplayedName = "Browser Refresh", Count = keyBoardStats.Mapping["BrowserRefresh"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "BrowserStop", DisplayedName = "Browser Stop", Count = keyBoardStats.Mapping["BrowserStop"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "BrowserSearch", DisplayedName = "Browser Search", Count = keyBoardStats.Mapping["BrowserSearch"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "BrowserFavorites", DisplayedName = "Browser Favorites", Count = keyBoardStats.Mapping["BrowserFavorites"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "BrowserHome", DisplayedName = "Browser Home", Count = keyBoardStats.Mapping["BrowserHome"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "VolumeMute", DisplayedName = "Volume Mute", Count = keyBoardStats.Mapping["VolumeMute"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "VolumeDown", DisplayedName = "Volume Down", Count = keyBoardStats.Mapping["VolumeDown"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "VolumeUp", DisplayedName = "Volume Up", Count = keyBoardStats.Mapping["VolumeUp"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "MediaNextTrack", DisplayedName = "Media Next Track", Count = keyBoardStats.Mapping["MediaNextTrack"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "MediaPrevTrack", DisplayedName = "Media Prev Track", Count = keyBoardStats.Mapping["MediaPrevTrack"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "MediaStop", DisplayedName = "Media Stop", Count = keyBoardStats.Mapping["MediaStop"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "MediaPlayPause", DisplayedName = "Media Play Pause", Count = keyBoardStats.Mapping["MediaPlayPause"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "LaunchMail", DisplayedName = "Launch Mail", Count = keyBoardStats.Mapping["LaunchMail"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "LaunchMediaSelect", DisplayedName = "Launch Media Select", Count = keyBoardStats.Mapping["LaunchMediaSelect"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "LaunchApp1", DisplayedName = "Launch App 1", Count = keyBoardStats.Mapping["LaunchApp1"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "LaunchApp2", DisplayedName = "Launch App 2", Count = keyBoardStats.Mapping["LaunchApp2"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "ProcessKey", DisplayedName = "Process Key", Count = keyBoardStats.Mapping["ProcessKey"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Packet", Count = keyBoardStats.Mapping["Packet"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Attn", Count = keyBoardStats.Mapping["Attn"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Crsel", Count = keyBoardStats.Mapping["Crsel"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Exsel", Count = keyBoardStats.Mapping["Exsel"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Ereof", Count = keyBoardStats.Mapping["Ereof"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Play", Count = keyBoardStats.Mapping["Play"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Zoom", Count = keyBoardStats.Mapping["Zoom"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "NoName", DisplayedName = "No Name", Count = keyBoardStats.Mapping["NoName"].ToString(), Index = index++ },
                new StatsListViewItem() { Type = "Pa1", Count = keyBoardStats.Mapping["Pa1"].ToString(), Index = index }
            };

            KeyboardStatsListView.ItemsSource = KeyboardStatsListViewItems;
        }

        #endregion

        private void InitSettings()
        {
            if (userSettings.CloseApplicationOption)
            {
                CloseApplicationButton_CloseApplicationOption.IsChecked = true;
                CloseApplicationButton_ReduceApplicationOption.IsChecked = false;
            }
            else
            {
                CloseApplicationButton_CloseApplicationOption.IsChecked = false;
                CloseApplicationButton_ReduceApplicationOption.IsChecked = true;
            }

            if (userSettings.OnStartUpWindows)
            {
                StartupApplicationButton_YesOption.IsChecked = true;
                StartupApplicationButton_NoOption.IsChecked = false;
            }
            else
            {
                StartupApplicationButton_YesOption.IsChecked = false;
                StartupApplicationButton_NoOption.IsChecked = true;
            }

            if (userSettings.KeyboardTracking)
            {
                KeyboardTrackingButton_YesOption.IsChecked = true;
                KeyboardTrackingButton_NoOption.IsChecked = false;
            }
            else
            {
                KeyboardTrackingButton_YesOption.IsChecked = false;
                KeyboardTrackingButton_NoOption.IsChecked = true;
            }
        }

        /**
         * Saves the data periodically.
         */
        private void OnTimer(object sender, EventArgs e)
        {
            DataSavingLabel.Visibility = Visibility.Visible;
            WriteFile(MOUSE_STATS, mouseStats.ToString());
            WriteFile(KEYBOARD_STATS, keyBoardStats.ToString());
            DelayOnTimer();
        }

        private async void DelayOnTimer()
        {
            await Task.Delay(1000);
            DataSavingLabel.Content = "Data Saved!";
            await Task.Delay(1000);
            DataSavingLabel.Visibility = Visibility.Hidden;
            DataSavingLabel.Content = "Saving Data...";
        }

        /**
         * Saves the data when and close the application.
         */
        public void CloseApplication()
        {
            HookManager.MouseMove -= HookManager_MouseMove;
            HookManager.MouseDown -= HookManager_MouseDown;
            HookManager.MouseDoubleClick -= HookManager_MouseDoubleClick;
            HookManager.MouseWheel -= HookManager_MouseWheel;
            // HookManager.KeyPress -= HookManager_KeyPress;
            HookManager.KeyUp -= HookManager_KeyUp;
            // HookManager.KeyDown -= HookManager_KeyDown;
            // keyboardHook.Dispose();
            WriteFile(USER_SETTINGS, userSettings.ToString());
            WriteFile(MOUSE_STATS, mouseStats.ToString());
            WriteFile(KEYBOARD_STATS, keyBoardStats.ToString());
            System.Windows.Application.Current.Shutdown();
        }

        private static void CreateFile(string path)
        {
            File.Create(path).Dispose();
        }

        private static string ReadFile(string path)
        {
            string data = File.ReadAllText(path);
            // Avoid deleting the file when the application is running because if the system crashes,
            // then the data are lost forever. Moreover, the data will be saved periodically when
            // the service is running so that in case of system crash, the least data are lost.
            // File.Delete(path);
            return data;
        }

        private static void WriteFile(string path, string data)
        {
            File.WriteAllText(path, data);
        }

        #endregion

        #region Hooks

        #region Mouse events

        private void HookManager_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            MouseStats.X = e.X;
            MouseStats.Y = e.Y;
            XMouseCoordinateLabel.Text = string.Format("X = {0:0000}", MouseStats.X);
            YMouseCoordinateLabel.Text = string.Format("Y = {0:0000}", MouseStats.Y);
        }

        private void HookManager_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            MouseStats.IncreaseValue(e.Button.ToString());

            switch (e.Button)
            {
                case MouseButtons.Left:
                    ((StatsListViewItem)MouseStatsListView.Items[0]).Count = mouseStats.Mapping["Left"].ToString();
                    break;
                case MouseButtons.Right:
                    ((StatsListViewItem)MouseStatsListView.Items[1]).Count = mouseStats.Mapping["Right"].ToString();
                    break;
                case MouseButtons.Middle:
                    ((StatsListViewItem)MouseStatsListView.Items[2]).Count = mouseStats.Mapping["Middle"].ToString();
                    break;
                case MouseButtons.XButton1:
                    ((StatsListViewItem)MouseStatsListView.Items[3]).Count = mouseStats.Mapping["XButton1"].ToString();
                    break;
                case MouseButtons.XButton2:
                    ((StatsListViewItem)MouseStatsListView.Items[4]).Count = mouseStats.Mapping["XButton2"].ToString();
                    break;
            }

            MouseStatsListView.Items.Refresh();
        }

        private void HookManager_MouseWheel(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            MouseStats.IncreaseWheelMovementValue((short)((e.Delta >> 16) & 0xffff));

            if (e.Delta == 120)
            {
                ((StatsListViewItem)MouseStatsListView.Items[11]).Count = mouseStats.Mapping["WheelForwardScroll"].ToString();
            }
            else
            {
                ((StatsListViewItem)MouseStatsListView.Items[10]).Count = mouseStats.Mapping["WheelBackScroll"].ToString();
            }

            MouseStatsListView.Items.Refresh();
        }

        private void HookManager_MouseDoubleClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            MouseStats.IncreaseValue(e.Button.ToString() + "DblClk");

            switch (e.Button)
            {
                case MouseButtons.Left:
                    ((StatsListViewItem)MouseStatsListView.Items[5]).Count = mouseStats.Mapping["LeftDblClk"].ToString();
                    break;
                case MouseButtons.Right:
                    ((StatsListViewItem)MouseStatsListView.Items[6]).Count = mouseStats.Mapping["RightDblClk"].ToString();
                    break;
                case MouseButtons.Middle:
                    ((StatsListViewItem)MouseStatsListView.Items[7]).Count = mouseStats.Mapping["MiddleDblClk"].ToString();
                    break;
                case MouseButtons.XButton1:
                    ((StatsListViewItem)MouseStatsListView.Items[8]).Count = mouseStats.Mapping["XButton1DblClk"].ToString();
                    break;
                case MouseButtons.XButton2:
                    ((StatsListViewItem)MouseStatsListView.Items[9]).Count = mouseStats.Mapping["XButton2DblClk"].ToString();
                    break;
            }

            MouseStatsListView.Items.Refresh();
        }

        #endregion

        #region Keyboard events

        private void HookManager_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {

        }

        private void HookManager_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void HookManager_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            string keyValue = e.KeyCode.ToString();

            KeyBoardStats.IncreaseValue(keyValue);

            foreach (StatsListViewItem lvi in KeyboardStatsListView.Items)
            {
                if (lvi.Type.Equals(keyValue))
                {
                    if (lvi.DisplayedName == null)
                    {
                        LastKeystrokeLabel.Text = "Last Keystroke: " + lvi.Type;
                    }
                    else
                    {
                        LastKeystrokeLabel.Text = "Last Keystroke: " + lvi.DisplayedName;
                    }

                    ((StatsListViewItem)KeyboardStatsListView.Items[lvi.Index]).Count = keyBoardStats.Mapping[lvi.Type].ToString();
                    break;
                }
            }

            KeyboardStatsListView.Items.Refresh();
        }

        #endregion

        #endregion

        #region Application Mode Management

        private void ExitItem_Click(Object sender, EventArgs args)
        {
            CloseApplication();
        }

        /**
         * Minimizes the application into the task bar.
         */
        private void MinimizeApplicationButton_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        /**
         * Allows to either close the application or hide it into the system tray.
         */
        private void CloseApplicationButton_Click(object sender, RoutedEventArgs e)
        {
            if (userSettings.CloseApplicationOption)
            {
                CloseApplication();
            }
            else
            {
                trayIcon.Visible = true;
                Hide();
            }
        }

        #endregion

        #region Menu Interface Buttons

        #region Home Button

        private void HomeButton_Click(object sender, RoutedEventArgs e)
        {
            ResetLastImage(LastImageBrush);
            LastImageBrush = "HomeButton_Image";
            HomeButton.Background = HomeButtonClicked_ImageBrush;
            LastGrid.Visibility = Visibility.Hidden;
            LastGrid = OverwiewStatsGrid;
            OverwiewStatsGrid.Visibility = Visibility.Visible;
        }

        #endregion

        #region Speed Button

        private void SpeedButton_Click(object sender, RoutedEventArgs e)
        {
            ResetLastImage(LastImageBrush);
            LastImageBrush = "SpeedButton_Image";
            SpeedButton.Background = SpeedButtonClicked_ImageBrush;
            LastGrid.Visibility = Visibility.Hidden;
            LastGrid = SpeedClickGrid;
            SpeedClickGrid.Visibility = Visibility.Visible;
        }

        private void SpeedPlayButton_Click(object sender, RoutedEventArgs e)
        {
            if (speedClickState == State.RUNNING)
            {
                Clicks.Content = (int.Parse(Clicks.Content.ToString()) + 1).ToString();
            }
            else if (speedClickState == State.STOPPED)
            {
                if (speedDisplayTimer.ElapsedMilliseconds > 3000)
                {
                    PlayStateLabel.Content = "Click Here";
                    Clicks.Content = "0";
                    Timer.Content = Math.Round(SetTimerSlider.Value).ToString();
                    ClicksPerSeconds.Content = "-";
                    SetTimerSlider.IsEnabled = true;
                    speedDisplayTimer.Stop();
                    speedDisplayTimer.Reset();
                    speedClickState = State.RESTART;
                }
            }
            else if (speedClickState == State.RESTART)
            {
                StartSpeedTimer();
            }
        }

        private void SetTimerSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Timer.Content = Math.Round(SetTimerSlider.Value).ToString();
        }

        private void SetupSpeedTimer()
        {
            speedTimer = new DispatcherTimer();
            speedTimer.Tick += new EventHandler(OnSpeedTimerEvent);
            speedTimer.Interval = new TimeSpan(100);
            speedTimer.IsEnabled = false;

            speedDisplayTimer = new Stopwatch();
        }

        private void StartSpeedTimer()
        {
            timeCounter = speedTimer.Interval.Seconds;
            speedTimer.Start();
            speedDisplayTimer.Start();
            SetTimerSlider.IsEnabled = false;
            speedClickState = State.RUNNING;
        }

        private void OnSpeedTimerEvent(object sender, EventArgs e)
        {
            if (speedDisplayTimer.ElapsedMilliseconds > (int)Math.Round(SetTimerSlider.Value) * 1000)
            {
                SpeedListViewItem item = new SpeedListViewItem
                {
                    Date = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                    Clicks = Clicks.Content.ToString(),
                    Timer = ((int)Math.Round(SetTimerSlider.Value)).ToString(),
                    Cps = Math.Round(float.Parse(Clicks.Content.ToString()) / (int)Math.Round(SetTimerSlider.Value), 1).ToString(),
                    Index = speedIndex++
                };

                SpeedClickListViewItems.Insert(0, item);
                ClicksPerSeconds.Content = item.Cps;

                PlayStateLabel.Content = "Click To Restart";
                speedTimer.Stop();
                speedDisplayTimer.Reset();
                speedDisplayTimer.Start();
                speedClickState = State.STOPPED;
            }
            else
            {
                Timer.Content = Math.Round((int)Math.Round(SetTimerSlider.Value) - speedDisplayTimer.ElapsedMilliseconds / 1000.0, 3);
            }
        }

        #endregion

        #region Settings Button

        private void SettingsButton_Click(object sender, RoutedEventArgs e)
        {
            ResetLastImage(LastImageBrush);
            LastImageBrush = "SettingsButton_Image";
            SettingsButton.Background = SettingsButtonClicked_ImageBrush;
            LastGrid.Visibility = Visibility.Hidden;
            LastGrid = SettingsGrid;
            SettingsGrid.Visibility = Visibility.Visible;
        }

        private void CloseApplicationButton_ReduceApplicationOption_Checked(object sender, RoutedEventArgs e)
        {
            userSettings.CloseApplicationOption = false;
        }

        private void CloseApplicationButton_CloseApplicationOption_Checked(object sender, RoutedEventArgs e)
        {
            userSettings.CloseApplicationOption = true;
        }

        private void StartupApplicationButton_YesOption_Checked(object sender, RoutedEventArgs e)
        {
            userSettings.OnStartUpWindows = true;
            WindowsStartupManager.AddApplicationToCurrentUserStartup();
        }

        private void StartupApplicationButton_NoOption_Checked(object sender, RoutedEventArgs e)
        {
            userSettings.OnStartUpWindows = false;
            WindowsStartupManager.RemoveApplicationFromCurrentUserStartup();
        }

        private void KeyboardTrackingButton_YesOption_Checked(object sender, RoutedEventArgs e)
        {
            userSettings.KeyboardTracking = true;
            HookManager.KeyUp += HookManager_KeyUp;
        }

        private void KeyboardTrackingButton_NoOption_Checked(object sender, RoutedEventArgs e)
        {
            userSettings.KeyboardTracking = false;
            HookManager.KeyUp -= HookManager_KeyUp;
        }

        #endregion

        #endregion

        private void HomeInterfaceRoundBorder_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ResetLastImage(LastImageBrush);
            LastImageBrush = "HomeButton_Image";
            HomeButton.Background = HomeButtonClicked_ImageBrush;
            LastGrid.Visibility = Visibility.Hidden;
            LastGrid = OverwiewStatsGrid;
            OverwiewStatsGrid.Visibility = Visibility.Visible;
        }

        private void MouseStatsButton_Click(object sender, RoutedEventArgs e)
        {
            ResetLastImage(LastImageBrush);
            LastImageBrush = "MouseStatsButton_Image";
            MouseStatsButton.Background = MouseStatsButtonClicked_ImageBrush;
            LastGrid.Visibility = Visibility.Hidden;
            LastGrid = MouseStatsGrid;
            MouseStatsGrid.Visibility = Visibility.Visible;
        }

        private void KeyboardStatsButton_Click(object sender, RoutedEventArgs e)
        {
            ResetLastImage(LastImageBrush);
            LastImageBrush = "KeyboardStatsButton_Image";
            KeyboardStatsButton.Background = KeyboardStatsButtonClicked_ImageBrush;
            LastGrid.Visibility = Visibility.Hidden;
            LastGrid = KeyboardStatsGrid;
            KeyboardStatsGrid.Visibility = Visibility.Visible;
        }

        private void DragZoneLeftAngle_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void DragZoneBorder_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void DragZoneRightAngle_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void ResetLastImage(string image)
        {
            switch (image)
            {
                case "HomeButton_Image":
                    HomeButton.Background = HomeButton_ImageBrush;
                    break;
                case "SpeedButton_Image":
                    SpeedButton.Background = SpeedButton_ImageBrush;
                    break;
                case "SettingsButton_Image":
                    SettingsButton.Background = SettingsButton_ImageBrush;
                    break;
                case "MouseStatsButton_Image":
                    MouseStatsButton.Background = MouseStatsButton_ImageBrush;
                    break;
                case "KeyboardStatsButton_Image":
                    KeyboardStatsButton.Background = KeyboardStatsButton_ImageBrush;
                    break;
                default:
                    HomeButton.Background = HomeButton_ImageBrush;
                    SettingsButton.Background = SettingsButton_ImageBrush;
                    MouseStatsButton.Background = MouseStatsButton_ImageBrush;
                    KeyboardStatsButton.Background = KeyboardStatsButton_ImageBrush;
                    break;
            }
        }
    }

    #region AdHoc Class

    public class SpeedListViewItem
    {
        public string Date { get; set; }

        public string Clicks { get; set; }

        public string Timer { get; set; }

        public string Cps { get; set; }

        public int Index { get; set; }
    }

    public class StatsListViewItem
    {
        public string Type { get; set; }

        public string DisplayedName { get; set; }

        public string Count { get; set; }

        public int Index { get; set; }
    }

    public class UserSettings
    {
        public bool CloseApplicationOption { get; set; }

        public bool OnStartUpWindows { get; set; }

        public bool KeyboardTracking { get; set; }

        /**
         * This default constructor is used to instanciate a brand new UserSettings object
         * either because the service is launched for the first time or because user settings
         * file has been deleted or corrupted.
         */
        public UserSettings()
        {
            CloseApplicationOption = false;
            OnStartUpWindows = false;
            KeyboardTracking = true;
        }

        /**
         * Instanciate a MouseStats object from existing data source.
         */
        public UserSettings(JObject data)
        {
            CloseApplicationOption = (bool)data["CloseApplicationOption"];
            OnStartUpWindows = (bool)data["OnStartUpWindows"];
            KeyboardTracking = (bool)data["KeyboardTracking"];
        }

        /**
         * Serialize the object into JSON object format. Used when saving data.
         */
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    #endregion
}
