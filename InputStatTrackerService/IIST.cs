﻿using System.ServiceModel;
using System.ServiceModel.Web;

namespace InputStatTrackerService
{
    [ServiceContract]
    public partial interface IIST
    {
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "LeftMouseClicks", ResponseFormat = WebMessageFormat.Json)]
        ulong GetLeftMouseClicks();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "RightMouseClicks", ResponseFormat = WebMessageFormat.Json)]
        ulong GetRightMouseClicks();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "MiddleMouseClicks", ResponseFormat = WebMessageFormat.Json)]
        ulong GetMiddleMouseClicks();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "XButton1MouseClicks", ResponseFormat = WebMessageFormat.Json)]
        ulong GetXButton1MouseClicks();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "XButton2MouseClicks", ResponseFormat = WebMessageFormat.Json)]
        ulong GetXButton2MouseClicks();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "LeftDblMouseClicks", ResponseFormat = WebMessageFormat.Json)]
        ulong GetLeftDblMouseClicks();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "RightDblMouseClicks", ResponseFormat = WebMessageFormat.Json)]
        ulong GetRightDblMouseClicks();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "MiddleDblMouseClicks", ResponseFormat = WebMessageFormat.Json)]
        ulong GetMiddleDblMouseClicks();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "XButton1DblMouseClicks", ResponseFormat = WebMessageFormat.Json)]
        ulong GetXButton1DblMouseClicks();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "XButton2DblMouseClicks", ResponseFormat = WebMessageFormat.Json)]
        ulong GetXButton2DblMouseClicks();
    }

    public partial interface IIST
    {
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Cancel", ResponseFormat = WebMessageFormat.Json)]
        ulong GetCancel();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Back", ResponseFormat = WebMessageFormat.Json)]
        ulong GetBack();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Tab", ResponseFormat = WebMessageFormat.Json)]
        ulong GetTab();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Clear", ResponseFormat = WebMessageFormat.Json)]
        ulong GetClear();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Return", ResponseFormat = WebMessageFormat.Json)]
        ulong GetReturn();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Shift", ResponseFormat = WebMessageFormat.Json)]
        ulong GetShift();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Control", ResponseFormat = WebMessageFormat.Json)]
        ulong GetControl();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Menu", ResponseFormat = WebMessageFormat.Json)]
        ulong GetMenu();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Pause", ResponseFormat = WebMessageFormat.Json)]
        ulong GetPause();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Capital", ResponseFormat = WebMessageFormat.Json)]
        ulong GetCapital();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Kana", ResponseFormat = WebMessageFormat.Json)]
        ulong GetKana();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Hangul", ResponseFormat = WebMessageFormat.Json)]
        ulong GetHangul();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Junja", ResponseFormat = WebMessageFormat.Json)]
        ulong GetJunja();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Final", ResponseFormat = WebMessageFormat.Json)]
        ulong GetFinal();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Hanja", ResponseFormat = WebMessageFormat.Json)]
        ulong GetHanja();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Kanji", ResponseFormat = WebMessageFormat.Json)]
        ulong GetKanji();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Escape", ResponseFormat = WebMessageFormat.Json)]
        ulong GetEscape();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Convert", ResponseFormat = WebMessageFormat.Json)]
        ulong GetConvert();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "NonConvert", ResponseFormat = WebMessageFormat.Json)]
        ulong GetNonConvert();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Accept", ResponseFormat = WebMessageFormat.Json)]
        ulong GetAccept();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ModeChange", ResponseFormat = WebMessageFormat.Json)]
        ulong GetModeChange();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Space", ResponseFormat = WebMessageFormat.Json)]
        ulong GetSpace();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Prior", ResponseFormat = WebMessageFormat.Json)]
        ulong GetPrior();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Next", ResponseFormat = WebMessageFormat.Json)]
        ulong GetNext();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "End", ResponseFormat = WebMessageFormat.Json)]
        ulong GetEnd();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Home", ResponseFormat = WebMessageFormat.Json)]
        ulong GetHome();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Left", ResponseFormat = WebMessageFormat.Json)]
        ulong GetLeft();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Up", ResponseFormat = WebMessageFormat.Json)]
        ulong GetUp();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Right", ResponseFormat = WebMessageFormat.Json)]
        ulong GetRight();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Down", ResponseFormat = WebMessageFormat.Json)]
        ulong GetDown();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Select", ResponseFormat = WebMessageFormat.Json)]
        ulong GetSelect();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "PrintScreen", ResponseFormat = WebMessageFormat.Json)]
        ulong GetPrintScreen();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Execute", ResponseFormat = WebMessageFormat.Json)]
        ulong GetExecute();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Snapshot", ResponseFormat = WebMessageFormat.Json)]
        ulong GetSnapshot();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Insert", ResponseFormat = WebMessageFormat.Json)]
        ulong GetInsert();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Delete", ResponseFormat = WebMessageFormat.Json)]
        ulong GetDelete();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Help", ResponseFormat = WebMessageFormat.Json)]
        ulong GetHelp();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "D0", ResponseFormat = WebMessageFormat.Json)]
        ulong GetD0();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "D1", ResponseFormat = WebMessageFormat.Json)]
        ulong GetD1();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "D2", ResponseFormat = WebMessageFormat.Json)]
        ulong GetD2();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "D3", ResponseFormat = WebMessageFormat.Json)]
        ulong GetD3();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "D4", ResponseFormat = WebMessageFormat.Json)]
        ulong GetD4();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "D5", ResponseFormat = WebMessageFormat.Json)]
        ulong GetD5();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "D6", ResponseFormat = WebMessageFormat.Json)]
        ulong GetD6();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "D7", ResponseFormat = WebMessageFormat.Json)]
        ulong GetD7();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "D8", ResponseFormat = WebMessageFormat.Json)]
        ulong GetD8();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "D9", ResponseFormat = WebMessageFormat.Json)]
        ulong GetD9();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "A", ResponseFormat = WebMessageFormat.Json)]
        ulong GetA();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "B", ResponseFormat = WebMessageFormat.Json)]
        ulong GetB();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "C", ResponseFormat = WebMessageFormat.Json)]
        ulong GetC();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "D", ResponseFormat = WebMessageFormat.Json)]
        ulong GetD();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "E", ResponseFormat = WebMessageFormat.Json)]
        ulong GetE();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "F", ResponseFormat = WebMessageFormat.Json)]
        ulong GetF();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "G", ResponseFormat = WebMessageFormat.Json)]
        ulong GetG();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "H", ResponseFormat = WebMessageFormat.Json)]
        ulong GetH();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "I", ResponseFormat = WebMessageFormat.Json)]
        ulong GetI();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "J", ResponseFormat = WebMessageFormat.Json)]
        ulong GetJ();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "K", ResponseFormat = WebMessageFormat.Json)]
        ulong GetK();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "L", ResponseFormat = WebMessageFormat.Json)]
        ulong GetL();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "M", ResponseFormat = WebMessageFormat.Json)]
        ulong GetM();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "N", ResponseFormat = WebMessageFormat.Json)]
        ulong GetN();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "O", ResponseFormat = WebMessageFormat.Json)]
        ulong GetO();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "P", ResponseFormat = WebMessageFormat.Json)]
        ulong GetP();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Q", ResponseFormat = WebMessageFormat.Json)]
        ulong GetQ();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "R", ResponseFormat = WebMessageFormat.Json)]
        ulong GetR();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "S", ResponseFormat = WebMessageFormat.Json)]
        ulong GetS();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "T", ResponseFormat = WebMessageFormat.Json)]
        ulong GetT();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "U", ResponseFormat = WebMessageFormat.Json)]
        ulong GetU();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "V", ResponseFormat = WebMessageFormat.Json)]
        ulong GetV();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "W", ResponseFormat = WebMessageFormat.Json)]
        ulong GetW();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "X", ResponseFormat = WebMessageFormat.Json)]
        ulong GetX();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Y", ResponseFormat = WebMessageFormat.Json)]
        ulong GetY();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Z", ResponseFormat = WebMessageFormat.Json)]
        ulong GetZ();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "LWin", ResponseFormat = WebMessageFormat.Json)]
        ulong GetLWin();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "RWin", ResponseFormat = WebMessageFormat.Json)]
        ulong GetRWin();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Apps", ResponseFormat = WebMessageFormat.Json)]
        ulong GetApps();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Sleep", ResponseFormat = WebMessageFormat.Json)]
        ulong GetSleep();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "NumPad0", ResponseFormat = WebMessageFormat.Json)]
        ulong GetNumPad0();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "NumPad1", ResponseFormat = WebMessageFormat.Json)]
        ulong GetNumPad1();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "NumPad2", ResponseFormat = WebMessageFormat.Json)]
        ulong GetNumPad2();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "NumPad3", ResponseFormat = WebMessageFormat.Json)]
        ulong GetNumPad3();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "NumPad4", ResponseFormat = WebMessageFormat.Json)]
        ulong GetNumPad4();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "NumPad5", ResponseFormat = WebMessageFormat.Json)]
        ulong GetNumPad5();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "NumPad6", ResponseFormat = WebMessageFormat.Json)]
        ulong GetNumPad6();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "NumPad7", ResponseFormat = WebMessageFormat.Json)]
        ulong GetNumPad7();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "NumPad8", ResponseFormat = WebMessageFormat.Json)]
        ulong GetNumPad8();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "NumPad9", ResponseFormat = WebMessageFormat.Json)]
        ulong GetNumPad9();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Multiply", ResponseFormat = WebMessageFormat.Json)]
        ulong GetMultiply();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Add", ResponseFormat = WebMessageFormat.Json)]
        ulong GetAdd();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Separator", ResponseFormat = WebMessageFormat.Json)]
        ulong GetSeparator();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Substract", ResponseFormat = WebMessageFormat.Json)]
        ulong GetSubstract();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Decimal", ResponseFormat = WebMessageFormat.Json)]
        ulong GetDecimal();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Divide", ResponseFormat = WebMessageFormat.Json)]
        ulong GetDivide();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "F1", ResponseFormat = WebMessageFormat.Json)]
        ulong GetF1();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "F2", ResponseFormat = WebMessageFormat.Json)]
        ulong GetF2();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "F3", ResponseFormat = WebMessageFormat.Json)]
        ulong GetF3();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "F4", ResponseFormat = WebMessageFormat.Json)]
        ulong GetF4();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "F5", ResponseFormat = WebMessageFormat.Json)]
        ulong GetF5();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "F6", ResponseFormat = WebMessageFormat.Json)]
        ulong GetF6();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "F7", ResponseFormat = WebMessageFormat.Json)]
        ulong GetF7();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "F8", ResponseFormat = WebMessageFormat.Json)]
        ulong GetF8();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "F9", ResponseFormat = WebMessageFormat.Json)]
        ulong GetF9();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "F10", ResponseFormat = WebMessageFormat.Json)]
        ulong GetF10();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "F11", ResponseFormat = WebMessageFormat.Json)]
        ulong GetF11();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "F12", ResponseFormat = WebMessageFormat.Json)]
        ulong GetF12();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "F13", ResponseFormat = WebMessageFormat.Json)]
        ulong GetF13();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "F14", ResponseFormat = WebMessageFormat.Json)]
        ulong GetF14();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "F15", ResponseFormat = WebMessageFormat.Json)]
        ulong GetF15();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "F16", ResponseFormat = WebMessageFormat.Json)]
        ulong GetF16();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "F17", ResponseFormat = WebMessageFormat.Json)]
        ulong GetF17();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "F18", ResponseFormat = WebMessageFormat.Json)]
        ulong GetF18();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "F19", ResponseFormat = WebMessageFormat.Json)]
        ulong GetF19();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "F20", ResponseFormat = WebMessageFormat.Json)]
        ulong Get20();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "F21", ResponseFormat = WebMessageFormat.Json)]
        ulong GetF21();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "F22", ResponseFormat = WebMessageFormat.Json)]
        ulong GetF22();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "F23", ResponseFormat = WebMessageFormat.Json)]
        ulong GetF23();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "F24", ResponseFormat = WebMessageFormat.Json)]
        ulong GetF24();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "NumLock", ResponseFormat = WebMessageFormat.Json)]
        ulong GetNumLock();
        [OperationContract]

        [WebInvoke(Method = "GET", UriTemplate = "Scroll", ResponseFormat = WebMessageFormat.Json)]
        ulong GetScroll();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "LShiftKey", ResponseFormat = WebMessageFormat.Json)]
        ulong GetLShiftKey();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "RShiftKey", ResponseFormat = WebMessageFormat.Json)]
        ulong GetRShiftKey();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "LControlKey", ResponseFormat = WebMessageFormat.Json)]
        ulong GetLControlKey();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "RControlKey", ResponseFormat = WebMessageFormat.Json)]
        ulong GetRControlKey();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "LMenu", ResponseFormat = WebMessageFormat.Json)]
        ulong GetLMenu();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "RMenu", ResponseFormat = WebMessageFormat.Json)]
        ulong GetRMenu();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "BrowserBack", ResponseFormat = WebMessageFormat.Json)]
        ulong GetBrowserBack();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "BrowserForward", ResponseFormat = WebMessageFormat.Json)]
        ulong GetBrowserForward();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "BrowserRefresh", ResponseFormat = WebMessageFormat.Json)]
        ulong GetBrowserRefresh();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "BrowserStop", ResponseFormat = WebMessageFormat.Json)]
        ulong GetBrowserStop();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "BrowserSearch", ResponseFormat = WebMessageFormat.Json)]
        ulong GetBrowserSearch();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "BrowserFavorites", ResponseFormat = WebMessageFormat.Json)]
        ulong GetBrowserFavorites();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "BrowserHome", ResponseFormat = WebMessageFormat.Json)]
        ulong GetBrowserHome();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "VolumeMute", ResponseFormat = WebMessageFormat.Json)]
        ulong GetVolumeMute();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "VolumeDown", ResponseFormat = WebMessageFormat.Json)]
        ulong GetVolumeDown();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "VolumeUp", ResponseFormat = WebMessageFormat.Json)]
        ulong GetVolumeUp();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "MediaNextTrack", ResponseFormat = WebMessageFormat.Json)]
        ulong GetMediaNextTrack();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "MediaPrevTrack", ResponseFormat = WebMessageFormat.Json)]
        ulong GetMediaPrevTrack();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "MediaStop", ResponseFormat = WebMessageFormat.Json)]
        ulong GetMediaStop();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "MediaPlayPause", ResponseFormat = WebMessageFormat.Json)]
        ulong GetMediaPlayPause();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "LaunchMail", ResponseFormat = WebMessageFormat.Json)]
        ulong GetLaunchMail();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "LaunchMediaSelect", ResponseFormat = WebMessageFormat.Json)]
        ulong GetLaunchMediaSelect();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "LaunchApp1", ResponseFormat = WebMessageFormat.Json)]
        ulong GetLaunchApp1();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "LaunchApp2", ResponseFormat = WebMessageFormat.Json)]
        ulong GetLaunchApp2();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ProcessKey", ResponseFormat = WebMessageFormat.Json)]
        ulong GetProcessKey();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Packet", ResponseFormat = WebMessageFormat.Json)]
        ulong GetPacket();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Attn", ResponseFormat = WebMessageFormat.Json)]
        ulong GetAttn();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Crsel", ResponseFormat = WebMessageFormat.Json)]
        ulong GetCrsel();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Exsel", ResponseFormat = WebMessageFormat.Json)]
        ulong GetExsel();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Ereof", ResponseFormat = WebMessageFormat.Json)]
        ulong GetEreof();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Play", ResponseFormat = WebMessageFormat.Json)]
        ulong GetPlay();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Zoom", ResponseFormat = WebMessageFormat.Json)]
        ulong GetZoom();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "NoName", ResponseFormat = WebMessageFormat.Json)]
        ulong GetNoName();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Pa1", ResponseFormat = WebMessageFormat.Json)]
        ulong GetPa1();
    }
}