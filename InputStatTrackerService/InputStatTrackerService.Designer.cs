﻿namespace InputStatTrackerService
{
    partial class InputStatTrackerService
    {
        /**
         * Variable nécessaire au concepteur.
         */
        private System.ComponentModel.IContainer components = null;

        /**
         * Nettoyage des ressources utilisées.
         *
         * @author Kovox
         * @date 08/03/2018
         *
         * @param disposing true si les ressources managées doivent être supprimées ; sinon, false.
         */
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.ServiceName = "InputStatTrackerService";
        }

        #endregion
    }
}