﻿using System;
using System.ServiceProcess;

namespace InputStatTrackerService
{
    static class Program
    {
        /**
         * Service application main entry point.
         *
         * @author Kovox
         * @date 08/03/2018
         */
        static void Main(string[] args)
        {
            if (Environment.UserInteractive)
            {
                InputStatTrackerService service = new InputStatTrackerService(args);
                service.TestStartAndStop(args);
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                new InputStatTrackerService(args)
                };
                ServiceBase.Run(ServicesToRun);
            }
        }
    }
}