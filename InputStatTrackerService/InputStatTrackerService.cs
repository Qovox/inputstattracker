﻿using InputStatTrackerLibrary;
using Newtonsoft.Json.Linq;
using System;
using System.Drawing;
using System.IO;
using System.ServiceProcess;
using System.Threading;
using System.Windows.Forms;

namespace InputStatTrackerService
{
    public partial class InputStatTrackerService : ServiceBase
    {
        private const string MOUSE_STATS = "mouse_stats";
        private const string KEYBOARD_STATS = "keyboard_stats";

        private static string DynamicLocation = ""; // System.Reflection.Assembly.GetEntryAssembly().Location;

        public static MouseStats mouseStats;
        public static KeyBoardStats keyBoardStats;

        private Server server;

        public InputStatTrackerService()
        {

        }

        public InputStatTrackerService(string[] args)
        {
            InitializeComponent();
        }

        public void TestStartAndStop(string[] args)
        {
            OnStart(args);
            Console.ReadLine();
            OnStop();
        }

        protected override void OnStart(string[] args)
        {
            // Fetch mouse data
            if (File.Exists(DynamicLocation + MOUSE_STATS) && new FileInfo(DynamicLocation + MOUSE_STATS).Length != 0)
            {
                mouseStats = new MouseStats(JObject.Parse(ReadFile(DynamicLocation + MOUSE_STATS)));
            }
            else
            {
                mouseStats = new MouseStats();
            }

            // Fetch keyboard data
            if (File.Exists(DynamicLocation + KEYBOARD_STATS) && new FileInfo(DynamicLocation + KEYBOARD_STATS).Length != 0)
            {
                keyBoardStats = new KeyBoardStats(JObject.Parse(ReadFile(DynamicLocation + KEYBOARD_STATS)));
            }
            else
            {
                keyBoardStats = new KeyBoardStats();
            }

            server = new Server();
            server.Start();

            new Thread(RunEventPump).Start();

            // Set up a timer to trigger every minute.  
            System.Timers.Timer timer = new System.Timers.Timer
            {
                Interval = 60000 * 5 // 5 minutes
            };

            timer.Elapsed += new System.Timers.ElapsedEventHandler(OnTimer);
            timer.Start();
        }

        /**
         * Run an application (Form) in a new thread.
         */
        private void RunEventPump()
        {
            Application.Run(new HiddenForm());
        }

        /**
         * Saves the data a last time, stops the server and exits the application.
         */
        protected override void OnStop()
        {
            WriteFile(DynamicLocation + MOUSE_STATS, mouseStats.ToString());
            WriteFile(DynamicLocation + KEYBOARD_STATS, keyBoardStats.ToString());
            server.Stop();
            Application.Exit();
        }

        /**
         * Saves the data.
         */
        public void OnTimer(object sender, System.Timers.ElapsedEventArgs args)
        {
            WriteFile(DynamicLocation + MOUSE_STATS, mouseStats.ToString());
            WriteFile(DynamicLocation + KEYBOARD_STATS, keyBoardStats.ToString());
        }

        private static void CreateFile(string path)
        {
            File.Create(path).Dispose();
        }

        private static string ReadFile(string path)
        {
            string data = File.ReadAllText(DynamicLocation + path);
            // Avoid deleting the file when the service is running because if the system crashes,
            // then the data are lost forever. Moreover, the data will be saved periodically when
            // the service is running so that in case of system crash, the least data are lost.
            // File.Delete(path);
            return data;
        }

        private static void WriteFile(string path, string data)
        {
            File.WriteAllText(DynamicLocation + path, data);
        }
    }

    public partial class HiddenForm : Form
    {
        public HiddenForm()
        {
            FormBorderStyle = FormBorderStyle.SizableToolWindow;
            WindowState = FormWindowState.Minimized;
            Visible = false;
            ShowInTaskbar = false;
            Load += new EventHandler(HiddenForm_Load);

            HookManager.MouseMove += HookManager_MouseMove;
            HookManager.MouseDown += HookManager_MouseDown;
            HookManager.MouseDoubleClick += HookManager_MouseDoubleClick;
            HookManager.MouseWheel += HookManager_MouseWheel;
            HookManager.KeyPress += HookManager_KeyPress;
            HookManager.KeyUp += HookManager_KeyUp;
            HookManager.KeyDown += HookManager_KeyDown;
        }

        private void HiddenForm_Load(object sender, EventArgs e)
        {
            Size = new Size(0, 0);
            Opacity = 0.0;
        }

        #region Mouse events

        private void HookManager_MouseMove(object sender, MouseEventArgs e)
        {

        }

        private void HookManager_MouseDown(object sender, MouseEventArgs e)
        {
            MouseStats.IncreaseValue(e.Button.ToString());
        }

        private void HookManager_MouseWheel(object sender, MouseEventArgs e)
        {
            MouseStats.IncreaseWheelMovementValue((short)((e.Delta >> 16) & 0xffff));
        }

        private void HookManager_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            MouseStats.IncreaseValue(e.Button.ToString() + "DblClk");
        }

        #endregion

        #region Keyboard events

        private void HookManager_KeyDown(object sender, KeyEventArgs e)
        {
            KeyBoardStats.IncreaseValue(e.KeyCode.ToString());
        }

        private void HookManager_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void HookManager_KeyUp(object sender, KeyEventArgs e)
        {

        }

        #endregion
    }
}