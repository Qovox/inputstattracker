﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace InputStatTrackerService
{
    /**
     * Visit https://msdn.microsoft.com/en-us/library/windows/desktop/dd375731(v=vs.85).aspx
     * to find the different virtual key code of the keyboard keys. OEM Keys are not supported
     * because by definition, they depend of the manufacturer.
     *
     * @author Kovox
     * @date 09/03/2018
     */
    public class KeyBoardStats
    {
        private static ulong CANCEL;              // 0x03
        private static ulong BACK;                // 0x08 // BACKSPACE Key
        private static ulong TAB;                 // 0x09
        private static ulong CLEAR;               // 0x0C
        private static ulong RETURN;              // 0x0D
        private static ulong SHIFT;               // 0x10
        private static ulong CONTROL;             // 0x11
        private static ulong MENU;                // 0x12 // ALT Key
        private static ulong PAUSE;               // 0x13
        private static ulong CAPITAL;             // 0x14 // CAPS LOCK Key
        private static ulong KANA;                // 0x15 // IME Kana mode
        private static ulong HANGUL;              // 0x15 // IME Hangul mode
        private static ulong JUNJA;               // 0x17 // IME Junja mode
        private static ulong FINAL;               // 0x18 // IME Final mode
        private static ulong HANJA;               // 0x19 // IME Hanja mode
        private static ulong KANJI;               // 0x19 // IME Kanji mode
        private static ulong ESCAPE;              // 0x1B
        private static ulong CONVERT;             // 0x1C // IME convert
        private static ulong NONCONVERT;          // 0x1D // IME nonconvert
        private static ulong ACCEPT;              // 0x1E // IME accept
        private static ulong MODECHANGE;          // 0x1F // IME mode change request
        private static ulong SPACE;               // 0x20
        private static ulong PRIOR;               // 0x21 // PAGE UP Key
        private static ulong NEXT;                // 0x22 // PAGE DOWN Key
        private static ulong END;                 // 0x23
        private static ulong HOME;                // 0x24
        private static ulong LEFT;                // 0x25 // LEFT ARROW Key
        private static ulong UP;                  // 0x26 // UP ARROW Key
        private static ulong RIGHT;               // 0x27 // RIGHT ARROW Key
        private static ulong DOWN;                // 0x28 // DOWN ARROW Key
        private static ulong SELECT;              // 0x29
        private static ulong PRINT;               // 0x2A
        private static ulong EXECUTE;             // 0x2B
        private static ulong SNAPSHOT;            // 0x2C // PRINT SCREEN Key
        private static ulong INSERT;              // 0x2D
        private static ulong DELETE;              // 0x2E
        private static ulong HELP;                // 0x2F
        private static ulong ZERO;                // 0x30
        private static ulong ONE;                 // 0x31
        private static ulong TWO;                 // 0x32
        private static ulong THREE;               // 0x33
        private static ulong FOUR;                // 0x34
        private static ulong FIVE;                // 0x35
        private static ulong SIX;                 // 0x36
        private static ulong SEVEN;               // 0x37
        private static ulong EIGHT;               // 0x38
        private static ulong NINE;                // 0x39
        private static ulong A;                   // 0x41
        private static ulong B;                   // 0x42
        private static ulong C;                   // 0x43
        private static ulong D;                   // 0x44
        private static ulong E;                   // 0x45
        private static ulong F;                   // 0x46
        private static ulong G;                   // 0x47
        private static ulong H;                   // 0x48
        private static ulong I;                   // 0x49
        private static ulong J;                   // 0x4A
        private static ulong K;                   // 0x4B
        private static ulong L;                   // 0x4C
        private static ulong M;                   // 0x4D
        private static ulong N;                   // 0x4E
        private static ulong O;                   // 0x4F
        private static ulong P;                   // 0x50
        private static ulong Q;                   // 0x51
        private static ulong R;                   // 0x52
        private static ulong S;                   // 0x53
        private static ulong T;                   // 0x54
        private static ulong U;                   // 0x55
        private static ulong V;                   // 0x56
        private static ulong W;                   // 0x57
        private static ulong X;                   // 0x58
        private static ulong Y;                   // 0x59
        private static ulong Z;                   // 0x5A
        private static ulong LWIN;                // 0x5B // Left Windows key (Natural keyboard)
        private static ulong RWIN;                // 0x5C // Right Windows key (Natural keyboard)
        private static ulong APPS;                // 0x5D // Applications key (Natural keyboard)
        private static ulong SLEEP;               // 0x5F // Computer Sleep Key
        private static ulong NUMPAD0;             // 0x60
        private static ulong NUMPAD1;             // 0x61
        private static ulong NUMPAD2;             // 0x62
        private static ulong NUMPAD3;             // 0x63
        private static ulong NUMPAD4;             // 0x64
        private static ulong NUMPAD5;             // 0x65
        private static ulong NUMPAD6;             // 0x66
        private static ulong NUMPAD7;             // 0x67
        private static ulong NUMPAD8;             // 0x68
        private static ulong NUMPAD9;             // 0x69
        private static ulong MULTIPLY;            // 0x6A
        private static ulong ADD;                 // 0x6B
        private static ulong SEPARATOR;           // 0x6C
        private static ulong SUBSTRACT;           // 0x6D
        private static ulong DECIMAL;             // 0x6E
        private static ulong DIVIDE;              // 0x6F
        private static ulong F1;                  // 0x70
        private static ulong F2;                  // 0x71
        private static ulong F3;                  // 0x72
        private static ulong F4;                  // 0x73
        private static ulong F5;                  // 0x74
        private static ulong F6;                  // 0x75
        private static ulong F7;                  // 0x76
        private static ulong F8;                  // 0x77
        private static ulong F9;                  // 0x78
        private static ulong F10;                 // 0x79
        private static ulong F11;                 // 0x7A
        private static ulong F12;                 // 0x7B
        private static ulong F13;                 // 0x7C
        private static ulong F14;                 // 0x7D
        private static ulong F15;                 // 0x7E
        private static ulong F16;                 // 0x7F
        private static ulong F17;                 // 0x80
        private static ulong F18;                 // 0x81
        private static ulong F19;                 // 0x82
        private static ulong F20;                 // 0x83
        private static ulong F21;                 // 0x84
        private static ulong F22;                 // 0x85
        private static ulong F23;                 // 0x86
        private static ulong F24;                 // 0x87
        private static ulong NUMLOCK;             // 0x90
        private static ulong SCROLL;              // 0x91 // SCROLL LOCK Key
        private static ulong LSHIFT;              // 0xA0 // Left SHIFT Key
        private static ulong RSHIFT;              // 0xA1 // Right SHIFT Key
        private static ulong LCONTROL;            // 0xA2 // Left CONTROL Key
        private static ulong RCONTROL;            // 0xA3 // Right CONTROL Key
        private static ulong LMENU;               // 0xA4 // Left MENU Key
        private static ulong RMENU;               // 0xA5 // Right MENU Key
        private static ulong BROWSER_BACK;        // 0xA6
        private static ulong BROWSER_FORWARD;     // 0xA7
        private static ulong BROWSER_REFRESH;     // 0xA8
        private static ulong BROWSER_STOP;        // 0xA9
        private static ulong BROWSER_SEARCH;      // 0xAA
        private static ulong BROWSER_FAVORITES;   // 0xAB
        private static ulong BROWSER_HOME;        // 0xAC
        private static ulong VOLUME_MUTE;         // 0xAD
        private static ulong VOLUME_DOWN;         // 0xAE
        private static ulong VOLUME_UP;           // 0xAF
        private static ulong MEDIA_NEXT_TRACK;    // 0xB0
        private static ulong MEDIA_PREV_TRACK;    // 0xB1
        private static ulong MEDIA_STOP;          // 0xB2
        private static ulong MEDIA_PLAY_PAUSE;    // 0xB3
        private static ulong LAUNCH_MAIL;         // 0xB4
        private static ulong LAUNCH_MEDIA_SELECT; // 0xB5
        private static ulong LAUNCH_APP1;         // 0xB6
        private static ulong LAUNCH_APP2;         // 0xB7
        // private static ulong OEM_1;               // 0xBA // Used for miscellaneous characters; it can vary by keyboard. US Keyboard: ;: key.
        // private static ulong OEM_PLUS;            // 0xBB // For any country/region, the '+' key
        // private static ulong OEM_COMMA;           // 0xBC // For any country/region, the ',' key
        // private static ulong OEM_MINUS;           // 0xBD // For any country/region, the '-' key
        // private static ulong OEM_PERIOD;          // 0xBE // For any country/region, the '.' key
        // private static ulong OEM_2;               // 0xBF // Used for miscellaneous characters; it can vary by keyboard. US Keyboard: /? key.
        // private static ulong OEM_3;               // 0xC0 // Used for miscellaneous characters; it can vary by keyboard. US Keyboard: `~ key.
        // private static ulong OEM_4;               // 0xDB // Used for miscellaneous characters; it can vary by keyboard. US Keyboard: [{ key.
        // private static ulong OEM_5;               // 0xDC // Used for miscellaneous characters; it can vary by keyboard. US Keyboard: \| key.
        // private static ulong OEM_6;               // 0xDD // Used for miscellaneous characters; it can vary by keyboard. US Keyboard: ]} key.
        // private static ulong OEM_7;               // 0xDE // Used for miscellaneous characters; it can vary by keyboard. US Keyboard: '" key.
        // private static ulong OEM_8;               // 0xDF // Used for miscellaneous characters; it can vary by keyboard.
        // private static ulong OEM_102;             // 0xE2 // Either the angle bracket key or the backslash key on the RT 102-key keyboard
        private static ulong PROCESSKEY;          // 0xE5 // IME PROCESS Key
        private static ulong PACKET;              // 0xE7 // See description at website address in class description.
        private static ulong ATTN;                // 0xF6
        private static ulong CRSEL;               // 0xF7 // CrSel Key
        private static ulong EXSEL;               // 0xF8 // ExSel Key
        private static ulong EREOF;               // 0xF9 // Erase EOF Key
        private static ulong PLAY;                // 0xFA
        private static ulong ZOOM;                // 0xFB
        private static ulong NONAME;              // 0xFC // Reserved
        private static ulong PA1;                 // 0xFD
        // private static ulong OEM_CLEAR;           // 0xFE

        private static Dictionary<string, ulong> mapping = new Dictionary<string, ulong>()
        {
            { "Cancel", CANCEL },
            { "Back", BACK },
            { "Tab", TAB },
            { "Clear", CLEAR },
            { "Return", RETURN },
            { "Shift", SHIFT },
            { "Control", CONTROL },
            { "Menu", MENU },
            { "Pause", PAUSE },
            { "Capital", CAPITAL },
            { "Kana", KANA },
            { "Hangul", HANGUL },
            { "Junja", JUNJA },
            { "Final", FINAL },
            { "Hanja", HANJA },
            { "Kanji", KANJI },
            { "Escape", ESCAPE },
            { "Convert", CONVERT },
            { "NonConvert", NONCONVERT },
            { "Accept", ACCEPT },
            { "ModeChange", MODECHANGE },
            { "Space", SPACE },
            { "Prior", PRIOR },
            { "Next", NEXT },
            { "End", END },
            { "Home", HOME },
            { "Left", LEFT },
            { "Up", UP },
            { "Right", RIGHT },
            { "Down", DOWN },
            { "Select", SELECT },
            { "PrintScreen", PRINT },
            { "Execute", EXECUTE },
            { "Snapshot", SNAPSHOT },
            { "Insert", INSERT },
            { "Delete", DELETE },
            { "Help", HELP },
            { "D0", ZERO },
            { "D1", ONE },
            { "D2", TWO },
            { "D3", THREE },
            { "D4", FOUR },
            { "D5", FIVE },
            { "D6", SIX },
            { "D7", SEVEN },
            { "D8", EIGHT },
            { "D9", NINE },
            { "A", A },
            { "B", B },
            { "C", C },
            { "D", D },
            { "E", E },
            { "F", F },
            { "G", G },
            { "H", H },
            { "I", I },
            { "J", J },
            { "K", K },
            { "L", L },
            { "M", M },
            { "N", N },
            { "O", O },
            { "P", P },
            { "Q", Q },
            { "R", R },
            { "S", S },
            { "T", T },
            { "U", U },
            { "V", V },
            { "W", W },
            { "X", X },
            { "Y", Y },
            { "Z", Z },
            { "LWin", LWIN },
            { "RWin", RWIN },
            { "Apps", APPS },
            { "Sleep", SLEEP },
            { "NumPad0", NUMPAD0 },
            { "NumPad1", NUMPAD1 },
            { "NumPad2", NUMPAD2 },
            { "NumPad3", NUMPAD3 },
            { "NumPad4", NUMPAD4 },
            { "NumPad5", NUMPAD5 },
            { "NumPad6", NUMPAD6 },
            { "NumPad7", NUMPAD7 },
            { "NumPad8", NUMPAD8 },
            { "NumPad9", NUMPAD9 },
            { "Multiply", MULTIPLY },
            { "Add", ADD },
            { "Separator", SEPARATOR },
            { "Substract", SUBSTRACT },
            { "Decimal", DECIMAL },
            { "Divide", DIVIDE },
            { "F1", F1 },
            { "F2", F2 },
            { "F3", F3 },
            { "F4", F4 },
            { "F5", F5 },
            { "F6", F6 },
            { "F7", F7 },
            { "F8", F8 },
            { "F9", F9 },
            { "F10", F10 },
            { "F11", F11 },
            { "F12", F12 },
            { "F13", F13 },
            { "F14", F14 },
            { "F15", F15 },
            { "F16", F16 },
            { "F17", F17 },
            { "F18", F18 },
            { "F19", F19 },
            { "F20", F20 },
            { "F21", F21 },
            { "F22", F22 },
            { "F23", F23 },
            { "F24", F24 },
            { "NumLock", NUMLOCK },
            { "Scroll", SCROLL },
            { "LShiftKey", LSHIFT },
            { "RShiftKey", RSHIFT },
            { "LControlKey", LCONTROL },
            { "RControlKey", RCONTROL },
            { "LMenu", LMENU },
            { "RMenu", RMENU },
            { "BrowserBack", BROWSER_BACK },
            { "BrowserForward", BROWSER_FORWARD },
            { "BrowserRefresh", BROWSER_REFRESH },
            { "BrowserStop", BROWSER_STOP },
            { "BrowserSearch", BROWSER_SEARCH },
            { "BrowserFavorites", BROWSER_FAVORITES },
            { "BrowserHome", BROWSER_HOME },
            { "VolumeMute", VOLUME_MUTE },
            { "VolumeDown", VOLUME_DOWN },
            { "VolumeUp", VOLUME_UP },
            { "MediaNextTrack", MEDIA_NEXT_TRACK },
            { "MediaPrevTrack", MEDIA_PREV_TRACK },
            { "MediaStop", MEDIA_STOP },
            { "MediaPlayPause", MEDIA_PLAY_PAUSE },
            { "LaunchMail", LAUNCH_MAIL },
            { "LaunchMediaSelect", LAUNCH_MEDIA_SELECT },
            { "LaunchApp1", LAUNCH_APP1 },
            { "LaunchApp2", LAUNCH_APP2 },
            // { "Oem1", OEM_1 },
            // { "Oemplus", OEM_PLUS },
            // { "Oemcomma", OEM_COMMA },
            // { "Oemminus", OEM_MINUS },
            // { "Oemperiod", OEM_PERIOD },
            // { "Oem2", OEM_2 },
            // { "Oem3", OEM_3 },
            // { "Oem4", OEM_4 },
            // { "Oem5", OEM_5 },
            // { "Oem6", OEM_6 },
            // { "Oem7", OEM_7 },
            // { "Oem8", OEM_8 },
            // { "Oem102", OEM_102 },
            { "ProcessKey", PROCESSKEY },
            { "Packet", PACKET },
            { "Attn", ATTN },
            { "Crsel", CRSEL },
            { "Exsel", EXSEL },
            { "Ereof", EREOF },
            { "Play", PLAY },
            { "Zoom", ZOOM },
            { "NoName", NONAME },
            { "Pa1", PA1 },
            // { "Oemclear", OEM_CLEAR }
        };

        /**
         * This default constructor is used to instanciate a brand new KeyBoardStats object
         * either because the service is launched for the first time or because keyboard statistics
         * file has been deleted or corrupted.
         */
        public KeyBoardStats()
        {

        }

        /**
         * Instanciate a KeyBoardStats object from existing data source.
         */
        public KeyBoardStats(JObject data)
        {
            foreach (var obj in data)
            {
                mapping[obj.Key] = (ulong)obj.Value;
            }
        }

        public static void IncreaseValue(string keyCode)
        {
            if (mapping.ContainsKey(keyCode))
            {
                mapping[keyCode]++;
            }
        }

        public Dictionary<string, ulong> Mapping
        {
            get
            {
                return mapping;
            }
        }

        /**
         * Serialize the object into JSON object format. Used when saving data.
         */
        public override string ToString()
        {
            return JsonConvert.SerializeObject(mapping);
        }
    }
}