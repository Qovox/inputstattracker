﻿using System;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace InputStatTrackerService
{
    public class Server
    {
        private string Address = "localhost";
        private string Port = "50000";

        // Web Server used to host services
        private WebServiceHost InputStatTrackerHost;

        /**
         * Start the web server on the given port, and host the expected service
         */
        public void Start()
        {
            string url = "http://" + Address + ":" + Port;

            WebHttpBinding b = new WebHttpBinding();
            InputStatTrackerHost = new WebServiceHost(typeof(InputStatTrackerDataService), new Uri(url + "/IST"));

            // Adding the service to the host
            InputStatTrackerHost.AddServiceEndpoint(typeof(IIST), b, "");

            // Starting the InputStatTracker server
            InputStatTrackerHost.Open();
        }

        /**
         * Stop the already started web server
         */
        public void Stop()
        {
            InputStatTrackerHost.Close();
        }
    }
}