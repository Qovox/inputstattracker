﻿namespace InputStatTrackerService
{
    public partial class InputStatTrackerDataService : IIST
    {
        public ulong GetLeftMouseClicks()
        {
            return InputStatTrackerService.mouseStats.Mapping["Left"];
        }

        public ulong GetMiddleMouseClicks()
        {
            return InputStatTrackerService.mouseStats.Mapping["Middle"];
        }

        public ulong GetRightMouseClicks()
        {
            return InputStatTrackerService.mouseStats.Mapping["Right"];
        }

        public ulong GetXButton1MouseClicks()
        {
            return InputStatTrackerService.mouseStats.Mapping["XButton1"];
        }

        public ulong GetXButton2MouseClicks()
        {
            return InputStatTrackerService.mouseStats.Mapping["XButton2"];
        }

        public ulong GetLeftDblMouseClicks()
        {
            return InputStatTrackerService.mouseStats.Mapping["LeftDblClk"];
        }

        public ulong GetMiddleDblMouseClicks()
        {
            return InputStatTrackerService.mouseStats.Mapping["MiddleDblClk"];
        }

        public ulong GetRightDblMouseClicks()
        {
            return InputStatTrackerService.mouseStats.Mapping["RightDblClk"];
        }

        public ulong GetXButton1DblMouseClicks()
        {
            return InputStatTrackerService.mouseStats.Mapping["XButton1DblClk"];
        }

        public ulong GetXButton2DblMouseClicks()
        {
            return InputStatTrackerService.mouseStats.Mapping["XButton2DblClk"];
        }
    }

    public partial class InputStatTrackerDataService : IIST
    {
        public ulong GetCancel()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Cancel"];
        }

        public ulong GetBack()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Back"];
        }

        public ulong GetTab()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Tab"];
        }

        public ulong GetClear()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Clear"];
        }

        public ulong GetReturn()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Return"];
        }

        public ulong GetShift()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Shift"];
        }

        public ulong GetControl()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Control"];
        }

        public ulong GetMenu()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Menu"];
        }

        public ulong GetPause()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Pause"];
        }

        public ulong GetCapital()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Capital"];
        }

        public ulong GetKana()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Kana"];
        }

        public ulong GetHangul()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Hangul"];
        }

        public ulong GetJunja()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Junja"];
        }

        public ulong GetFinal()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Final"];
        }

        public ulong GetHanja()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Hanja"];
        }

        public ulong GetKanji()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Kanji"];
        }

        public ulong GetEscape()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Escape"];
        }

        public ulong GetConvert()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Convert"];
        }

        public ulong GetNonConvert()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["NonConvert"];
        }

        public ulong GetAccept()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Accept"];
        }

        public ulong GetModeChange()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["ModeChange"];
        }

        public ulong GetSpace()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Space"];
        }

        public ulong GetPrior()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Prior"];
        }

        public ulong GetNext()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Next"];
        }

        public ulong GetEnd()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["End"];
        }

        public ulong GetHome()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Home"];
        }

        public ulong GetLeft()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Left"];
        }

        public ulong GetUp()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Up"];
        }

        public ulong GetRight()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Right"];
        }

        public ulong GetDown()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Down"];
        }

        public ulong GetSelect()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Select"];
        }

        public ulong GetPrintScreen()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["PrintScreen"];
        }

        public ulong GetExecute()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Execute"];
        }

        public ulong GetSnapshot()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Snapshot"];
        }

        public ulong GetInsert()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Insert"];
        }

        public ulong GetDelete()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Delete"];
        }

        public ulong GetHelp()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Help"];
        }

        public ulong GetD0()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["D0"];
        }

        public ulong GetD1()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["D1"];
        }

        public ulong GetD2()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["D2"];
        }

        public ulong GetD3()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["D3"];
        }

        public ulong GetD4()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["D4"];
        }

        public ulong GetD5()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["D5"];
        }

        public ulong GetD6()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["D6"];
        }

        public ulong GetD7()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["D7"];
        }

        public ulong GetD8()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["D8"];
        }

        public ulong GetD9()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["D9"];
        }

        public ulong GetA()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["A"];
        }

        public ulong GetB()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["B"];
        }

        public ulong GetC()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["C"];
        }

        public ulong GetD()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["D"];
        }

        public ulong GetE()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["E"];
        }

        public ulong GetF()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["F"];
        }

        public ulong GetG()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["G"];
        }

        public ulong GetH()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["H"];
        }

        public ulong GetI()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["I"];
        }

        public ulong GetJ()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["J"];
        }

        public ulong GetK()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["K"];
        }

        public ulong GetL()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["L"];
        }

        public ulong GetM()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["M"];
        }

        public ulong GetN()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["N"];
        }

        public ulong GetO()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["O"];
        }

        public ulong GetP()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["P"];
        }

        public ulong GetQ()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Q"];
        }

        public ulong GetR()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["R"];
        }

        public ulong GetS()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["S"];
        }

        public ulong GetT()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["T"];
        }

        public ulong GetU()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["U"];
        }

        public ulong GetV()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["V"];
        }

        public ulong GetW()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["W"];
        }

        public ulong GetX()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["X"];
        }

        public ulong GetY()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Y"];
        }

        public ulong GetZ()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Z"];
        }

        public ulong GetLWin()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["LWin"];
        }

        public ulong GetRWin()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["RWin"];
        }

        public ulong GetApps()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Apps"];
        }

        public ulong GetSleep()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Sleep"];
        }

        public ulong GetNumPad0()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["NumPad0"];
        }

        public ulong GetNumPad1()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["NumPad1"];
        }

        public ulong GetNumPad2()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["NumPad2"];
        }

        public ulong GetNumPad3()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["NumPad3"];
        }

        public ulong GetNumPad4()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["NumPad4"];
        }

        public ulong GetNumPad5()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["NumPad5"];
        }

        public ulong GetNumPad6()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["NumPad6"];
        }

        public ulong GetNumPad7()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["NumPad7"];
        }

        public ulong GetNumPad8()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["NumPad8"];
        }

        public ulong GetNumPad9()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["NumPad9"];
        }

        public ulong GetMultiply()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Multiply"];
        }

        public ulong GetAdd()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Add"];
        }

        public ulong GetSeparator()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Separator"];
        }

        public ulong GetSubstract()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Substract"];
        }

        public ulong GetDecimal()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Decimal"];
        }

        public ulong GetDivide()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Divide"];
        }

        public ulong GetF1()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["F1"];
        }

        public ulong GetF2()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["F2"];
        }

        public ulong GetF3()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["F3"];
        }

        public ulong GetF4()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["F4"];
        }

        public ulong GetF5()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["F5"];
        }

        public ulong GetF6()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["F6"];
        }

        public ulong GetF7()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["F7"];
        }

        public ulong GetF8()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["F8"];
        }

        public ulong GetF9()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["F9"];
        }

        public ulong GetF10()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["F10"];
        }

        public ulong GetF11()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["F11"];
        }

        public ulong GetF12()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["F12"];
        }

        public ulong GetF13()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["F13"];
        }

        public ulong GetF14()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["F14"];
        }

        public ulong GetF15()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["F15"];
        }

        public ulong GetF16()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["F16"];
        }

        public ulong GetF17()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["F17"];
        }

        public ulong GetF18()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["F18"];
        }

        public ulong GetF19()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["F19"];
        }

        public ulong Get20()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["F20"];
        }

        public ulong GetF21()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["F21"];
        }

        public ulong GetF22()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["F22"];
        }

        public ulong GetF23()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["F23"];
        }

        public ulong GetF24()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["F24"];
        }

        public ulong GetNumLock()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["NumLock"];
        }

        public ulong GetScroll()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Scroll"];
        }

        public ulong GetLShiftKey()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["LShiftKey"];
        }

        public ulong GetRShiftKey()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["RShiftKey"];
        }

        public ulong GetLControlKey()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["LControlKey"];
        }

        public ulong GetRControlKey()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["RControlKey"];
        }

        public ulong GetLMenu()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["LMenu"];
        }

        public ulong GetRMenu()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["RMenu"];
        }

        public ulong GetBrowserBack()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["BrowserBack"];
        }

        public ulong GetBrowserForward()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["BrowserForward"];
        }

        public ulong GetBrowserRefresh()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["BrowserRefresh"];
        }

        public ulong GetBrowserStop()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["BrowserStop"];
        }

        public ulong GetBrowserSearch()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["BrowserSearch"];
        }

        public ulong GetBrowserFavorites()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["BrowserFavorites"];
        }

        public ulong GetBrowserHome()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["BrowserHome"];
        }

        public ulong GetVolumeMute()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["VolumeMute"];
        }

        public ulong GetVolumeDown()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["VolumeDown"];
        }

        public ulong GetVolumeUp()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["VolumeUp"];
        }

        public ulong GetMediaNextTrack()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["MediaNextTrack"];
        }

        public ulong GetMediaPrevTrack()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["MediaPrevTrack"];
        }

        public ulong GetMediaStop()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["MediaStop"];
        }

        public ulong GetMediaPlayPause()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["MediaPlayPause"];
        }

        public ulong GetLaunchMail()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["LaunchMail"];
        }

        public ulong GetLaunchMediaSelect()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["LaunchMediaSelect"];
        }

        public ulong GetLaunchApp1()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["LaunchApp1"];
        }

        public ulong GetLaunchApp2()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["LaunchApp2"];
        }

        public ulong GetProcessKey()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["ProcessKey"];
        }

        public ulong GetPacket()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Packet"];
        }

        public ulong GetAttn()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Attn"];
        }

        public ulong GetCrsel()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Crsel"];
        }

        public ulong GetExsel()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Exsel"];
        }

        public ulong GetEreof()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Ereof"];
        }

        public ulong GetPlay()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Play"];
        }

        public ulong GetZoom()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Zoom"];
        }

        public ulong GetNoName()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["NoName"];
        }

        public ulong GetPa1()
        {
            return InputStatTrackerService.keyBoardStats.Mapping["Pa1"];
        }
    }
}