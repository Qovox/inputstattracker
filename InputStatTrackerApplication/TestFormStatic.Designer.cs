namespace InputStatTrackerApplication
{
    partial class TestFormStatic
    {
        /** Required designer variable. */
        private System.ComponentModel.IContainer components = null;

        /**
         * Clean up any resources being used.
         *
         * @author Kovox
         * @date 07/03/2018
         *
         * @param disposing true if managed resources should be disposed; otherwise, false.
         */
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /**
         * Required method for Designer support - do not modify the contents of this method with the
         * code editor.
         *
         * @author Kovox
         * @date 07/03/2018
         */
        private void InitializeComponent()
        {
            this.labelMousePosition = new System.Windows.Forms.Label();
            this.textBoxLog = new System.Windows.Forms.TextBox();
            this.labelWheel = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.checkBoxKeyUp = new System.Windows.Forms.CheckBox();
            this.checkBoxKeyPress = new System.Windows.Forms.CheckBox();
            this.checkBoxKeyDown = new System.Windows.Forms.CheckBox();
            this.checkBoxMouseWheel = new System.Windows.Forms.CheckBox();
            this.checkBoxMouseDoubleClick = new System.Windows.Forms.CheckBox();
            this.checkBoxOnMouseUp = new System.Windows.Forms.CheckBox();
            this.checkBoxOnMouseDown = new System.Windows.Forms.CheckBox();
            this.checkBoxOnMouseClick = new System.Windows.Forms.CheckBox();
            this.checkBoxOnMouseMove = new System.Windows.Forms.CheckBox();
            this.LButtonLabel = new System.Windows.Forms.Label();
            this.RButtonLabel = new System.Windows.Forms.Label();
            this.MButtonLabel = new System.Windows.Forms.Label();
            this.XButton1Label = new System.Windows.Forms.Label();
            this.XButton2Label = new System.Windows.Forms.Label();
            this.XButton2DblClkLabel = new System.Windows.Forms.Label();
            this.XButton1DblClkLabel = new System.Windows.Forms.Label();
            this.MButtonDblClkLabel = new System.Windows.Forms.Label();
            this.RButtonDblClkLabel = new System.Windows.Forms.Label();
            this.LButtonDblClkLabel = new System.Windows.Forms.Label();
            this.WheelBackScrollLabel = new System.Windows.Forms.Label();
            this.WheelForwardScrollLabel = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelMousePosition
            // 
            this.labelMousePosition.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelMousePosition.AutoSize = true;
            this.labelMousePosition.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMousePosition.Location = new System.Drawing.Point(247, 113);
            this.labelMousePosition.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelMousePosition.Name = "labelMousePosition";
            this.labelMousePosition.Size = new System.Drawing.Size(206, 17);
            this.labelMousePosition.TabIndex = 2;
            this.labelMousePosition.Text = "x={0:####}; y={1:####}";
            // 
            // textBoxLog
            // 
            this.textBoxLog.Location = new System.Drawing.Point(0, 199);
            this.textBoxLog.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxLog.Multiline = true;
            this.textBoxLog.Name = "textBoxLog";
            this.textBoxLog.ReadOnly = true;
            this.textBoxLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxLog.Size = new System.Drawing.Size(256, 334);
            this.textBoxLog.TabIndex = 5;
            this.textBoxLog.WordWrap = false;
            // 
            // labelWheel
            // 
            this.labelWheel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelWheel.AutoSize = true;
            this.labelWheel.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelWheel.Location = new System.Drawing.Point(247, 138);
            this.labelWheel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelWheel.Name = "labelWheel";
            this.labelWheel.Size = new System.Drawing.Size(134, 17);
            this.labelWheel.TabIndex = 6;
            this.labelWheel.Text = "Wheel={0:####}";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkBoxKeyUp);
            this.groupBox2.Controls.Add(this.labelWheel);
            this.groupBox2.Controls.Add(this.checkBoxKeyPress);
            this.groupBox2.Controls.Add(this.labelMousePosition);
            this.groupBox2.Controls.Add(this.checkBoxKeyDown);
            this.groupBox2.Controls.Add(this.checkBoxMouseWheel);
            this.groupBox2.Controls.Add(this.checkBoxMouseDoubleClick);
            this.groupBox2.Controls.Add(this.checkBoxOnMouseUp);
            this.groupBox2.Controls.Add(this.checkBoxOnMouseDown);
            this.groupBox2.Controls.Add(this.checkBoxOnMouseClick);
            this.groupBox2.Controls.Add(this.checkBoxOnMouseMove);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(471, 199);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            // 
            // checkBoxKeyUp
            // 
            this.checkBoxKeyUp.AutoSize = true;
            this.checkBoxKeyUp.Location = new System.Drawing.Point(253, 82);
            this.checkBoxKeyUp.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxKeyUp.Name = "checkBoxKeyUp";
            this.checkBoxKeyUp.Size = new System.Drawing.Size(72, 21);
            this.checkBoxKeyUp.TabIndex = 8;
            this.checkBoxKeyUp.Text = "KeyUp";
            this.checkBoxKeyUp.UseVisualStyleBackColor = true;
            this.checkBoxKeyUp.CheckedChanged += new System.EventHandler(this.checkBoxKeyUp_CheckedChanged);
            // 
            // checkBoxKeyPress
            // 
            this.checkBoxKeyPress.AutoSize = true;
            this.checkBoxKeyPress.Location = new System.Drawing.Point(253, 53);
            this.checkBoxKeyPress.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxKeyPress.Name = "checkBoxKeyPress";
            this.checkBoxKeyPress.Size = new System.Drawing.Size(90, 21);
            this.checkBoxKeyPress.TabIndex = 7;
            this.checkBoxKeyPress.Text = "KeyPress";
            this.checkBoxKeyPress.UseVisualStyleBackColor = true;
            this.checkBoxKeyPress.CheckedChanged += new System.EventHandler(this.checkBoxKeyPress_CheckedChanged);
            // 
            // checkBoxKeyDown
            // 
            this.checkBoxKeyDown.AutoSize = true;
            this.checkBoxKeyDown.Location = new System.Drawing.Point(253, 25);
            this.checkBoxKeyDown.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxKeyDown.Name = "checkBoxKeyDown";
            this.checkBoxKeyDown.Size = new System.Drawing.Size(89, 21);
            this.checkBoxKeyDown.TabIndex = 6;
            this.checkBoxKeyDown.Text = "KeyDown";
            this.checkBoxKeyDown.UseVisualStyleBackColor = true;
            this.checkBoxKeyDown.CheckedChanged += new System.EventHandler(this.checkBoxKeyDown_CheckedChanged);
            // 
            // checkBoxMouseWheel
            // 
            this.checkBoxMouseWheel.AutoSize = true;
            this.checkBoxMouseWheel.Location = new System.Drawing.Point(17, 167);
            this.checkBoxMouseWheel.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxMouseWheel.Name = "checkBoxMouseWheel";
            this.checkBoxMouseWheel.Size = new System.Drawing.Size(112, 21);
            this.checkBoxMouseWheel.TabIndex = 5;
            this.checkBoxMouseWheel.Text = "MouseWheel";
            this.checkBoxMouseWheel.UseVisualStyleBackColor = true;
            this.checkBoxMouseWheel.CheckedChanged += new System.EventHandler(this.checkBoxMouseWheel_CheckedChanged);
            // 
            // checkBoxMouseDoubleClick
            // 
            this.checkBoxMouseDoubleClick.AutoSize = true;
            this.checkBoxMouseDoubleClick.Location = new System.Drawing.Point(17, 138);
            this.checkBoxMouseDoubleClick.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxMouseDoubleClick.Name = "checkBoxMouseDoubleClick";
            this.checkBoxMouseDoubleClick.Size = new System.Drawing.Size(146, 21);
            this.checkBoxMouseDoubleClick.TabIndex = 4;
            this.checkBoxMouseDoubleClick.Text = "MouseDoubleClick";
            this.checkBoxMouseDoubleClick.UseVisualStyleBackColor = true;
            this.checkBoxMouseDoubleClick.CheckedChanged += new System.EventHandler(this.checkBoxMouseDoubleClick_CheckedChanged);
            // 
            // checkBoxOnMouseUp
            // 
            this.checkBoxOnMouseUp.AutoSize = true;
            this.checkBoxOnMouseUp.Location = new System.Drawing.Point(17, 112);
            this.checkBoxOnMouseUp.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxOnMouseUp.Name = "checkBoxOnMouseUp";
            this.checkBoxOnMouseUp.Size = new System.Drawing.Size(90, 21);
            this.checkBoxOnMouseUp.TabIndex = 3;
            this.checkBoxOnMouseUp.Text = "MouseUp";
            this.checkBoxOnMouseUp.UseVisualStyleBackColor = true;
            this.checkBoxOnMouseUp.CheckedChanged += new System.EventHandler(this.checkBoxOnMouseUp_CheckedChanged);
            // 
            // checkBoxOnMouseDown
            // 
            this.checkBoxOnMouseDown.AutoSize = true;
            this.checkBoxOnMouseDown.Location = new System.Drawing.Point(17, 82);
            this.checkBoxOnMouseDown.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxOnMouseDown.Name = "checkBoxOnMouseDown";
            this.checkBoxOnMouseDown.Size = new System.Drawing.Size(107, 21);
            this.checkBoxOnMouseDown.TabIndex = 2;
            this.checkBoxOnMouseDown.Text = "MouseDown";
            this.checkBoxOnMouseDown.UseVisualStyleBackColor = true;
            this.checkBoxOnMouseDown.CheckedChanged += new System.EventHandler(this.checkBoxOnMouseDown_CheckedChanged);
            // 
            // checkBoxOnMouseClick
            // 
            this.checkBoxOnMouseClick.AutoSize = true;
            this.checkBoxOnMouseClick.Location = new System.Drawing.Point(17, 53);
            this.checkBoxOnMouseClick.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxOnMouseClick.Name = "checkBoxOnMouseClick";
            this.checkBoxOnMouseClick.Size = new System.Drawing.Size(101, 21);
            this.checkBoxOnMouseClick.TabIndex = 1;
            this.checkBoxOnMouseClick.Text = "MouseClick";
            this.checkBoxOnMouseClick.UseVisualStyleBackColor = true;
            this.checkBoxOnMouseClick.CheckedChanged += new System.EventHandler(this.checkBoxOnMouseClick_CheckedChanged);
            // 
            // checkBoxOnMouseMove
            // 
            this.checkBoxOnMouseMove.AutoSize = true;
            this.checkBoxOnMouseMove.Location = new System.Drawing.Point(17, 25);
            this.checkBoxOnMouseMove.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxOnMouseMove.Name = "checkBoxOnMouseMove";
            this.checkBoxOnMouseMove.Size = new System.Drawing.Size(106, 21);
            this.checkBoxOnMouseMove.TabIndex = 0;
            this.checkBoxOnMouseMove.Text = "MouseMove";
            this.checkBoxOnMouseMove.UseVisualStyleBackColor = true;
            this.checkBoxOnMouseMove.CheckedChanged += new System.EventHandler(this.checkBoxOnMouseMove_CheckedChanged);
            // 
            // LButtonLabel
            // 
            this.LButtonLabel.AutoSize = true;
            this.LButtonLabel.Location = new System.Drawing.Point(264, 207);
            this.LButtonLabel.Name = "LButtonLabel";
            this.LButtonLabel.Size = new System.Drawing.Size(73, 17);
            this.LButtonLabel.TabIndex = 8;
            this.LButtonLabel.Text = "LButton: 0";
            // 
            // RButtonLabel
            // 
            this.RButtonLabel.AutoSize = true;
            this.RButtonLabel.Location = new System.Drawing.Point(264, 233);
            this.RButtonLabel.Name = "RButtonLabel";
            this.RButtonLabel.Size = new System.Drawing.Size(75, 17);
            this.RButtonLabel.TabIndex = 9;
            this.RButtonLabel.Text = "RButton: 0";
            // 
            // MButtonLabel
            // 
            this.MButtonLabel.AutoSize = true;
            this.MButtonLabel.Location = new System.Drawing.Point(263, 259);
            this.MButtonLabel.Name = "MButtonLabel";
            this.MButtonLabel.Size = new System.Drawing.Size(76, 17);
            this.MButtonLabel.TabIndex = 10;
            this.MButtonLabel.Text = "MButton: 0";
            // 
            // XButton1Label
            // 
            this.XButton1Label.AutoSize = true;
            this.XButton1Label.Location = new System.Drawing.Point(263, 285);
            this.XButton1Label.Name = "XButton1Label";
            this.XButton1Label.Size = new System.Drawing.Size(82, 17);
            this.XButton1Label.TabIndex = 11;
            this.XButton1Label.Text = "XButton1: 0";
            // 
            // XButton2Label
            // 
            this.XButton2Label.AutoSize = true;
            this.XButton2Label.Location = new System.Drawing.Point(263, 311);
            this.XButton2Label.Name = "XButton2Label";
            this.XButton2Label.Size = new System.Drawing.Size(82, 17);
            this.XButton2Label.TabIndex = 12;
            this.XButton2Label.Text = "XButton2: 0";
            // 
            // XButton2DblClkLabel
            // 
            this.XButton2DblClkLabel.AutoSize = true;
            this.XButton2DblClkLabel.Location = new System.Drawing.Point(262, 441);
            this.XButton2DblClkLabel.Name = "XButton2DblClkLabel";
            this.XButton2DblClkLabel.Size = new System.Drawing.Size(122, 17);
            this.XButton2DblClkLabel.TabIndex = 17;
            this.XButton2DblClkLabel.Text = "XButton2DblClk: 0";
            // 
            // XButton1DblClkLabel
            // 
            this.XButton1DblClkLabel.AutoSize = true;
            this.XButton1DblClkLabel.Location = new System.Drawing.Point(262, 415);
            this.XButton1DblClkLabel.Name = "XButton1DblClkLabel";
            this.XButton1DblClkLabel.Size = new System.Drawing.Size(122, 17);
            this.XButton1DblClkLabel.TabIndex = 16;
            this.XButton1DblClkLabel.Text = "XButton1DblClk: 0";
            // 
            // MButtonDblClkLabel
            // 
            this.MButtonDblClkLabel.AutoSize = true;
            this.MButtonDblClkLabel.Location = new System.Drawing.Point(262, 389);
            this.MButtonDblClkLabel.Name = "MButtonDblClkLabel";
            this.MButtonDblClkLabel.Size = new System.Drawing.Size(116, 17);
            this.MButtonDblClkLabel.TabIndex = 15;
            this.MButtonDblClkLabel.Text = "MButtonDblClk: 0";
            // 
            // RButtonDblClkLabel
            // 
            this.RButtonDblClkLabel.AutoSize = true;
            this.RButtonDblClkLabel.Location = new System.Drawing.Point(263, 363);
            this.RButtonDblClkLabel.Name = "RButtonDblClkLabel";
            this.RButtonDblClkLabel.Size = new System.Drawing.Size(115, 17);
            this.RButtonDblClkLabel.TabIndex = 14;
            this.RButtonDblClkLabel.Text = "RButtonDblClk: 0";
            // 
            // LButtonDblClkLabel
            // 
            this.LButtonDblClkLabel.AutoSize = true;
            this.LButtonDblClkLabel.Location = new System.Drawing.Point(263, 337);
            this.LButtonDblClkLabel.Name = "LButtonDblClkLabel";
            this.LButtonDblClkLabel.Size = new System.Drawing.Size(113, 17);
            this.LButtonDblClkLabel.TabIndex = 13;
            this.LButtonDblClkLabel.Text = "LButtonDblClk: 0";
            // 
            // WheelBackScrollLabel
            // 
            this.WheelBackScrollLabel.AutoSize = true;
            this.WheelBackScrollLabel.Location = new System.Drawing.Point(262, 467);
            this.WheelBackScrollLabel.Name = "WheelBackScrollLabel";
            this.WheelBackScrollLabel.Size = new System.Drawing.Size(130, 17);
            this.WheelBackScrollLabel.TabIndex = 18;
            this.WheelBackScrollLabel.Text = "WheelBackScroll: 0";
            // 
            // WheelForwardScrollLabel
            // 
            this.WheelForwardScrollLabel.AutoSize = true;
            this.WheelForwardScrollLabel.Location = new System.Drawing.Point(262, 493);
            this.WheelForwardScrollLabel.Name = "WheelForwardScrollLabel";
            this.WheelForwardScrollLabel.Size = new System.Drawing.Size(150, 17);
            this.WheelForwardScrollLabel.TabIndex = 19;
            this.WheelForwardScrollLabel.Text = "WheelForwardScroll: 0";
            // 
            // TestFormStatic
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(471, 533);
            this.Controls.Add(this.WheelForwardScrollLabel);
            this.Controls.Add(this.WheelBackScrollLabel);
            this.Controls.Add(this.XButton2DblClkLabel);
            this.Controls.Add(this.XButton1DblClkLabel);
            this.Controls.Add(this.MButtonDblClkLabel);
            this.Controls.Add(this.RButtonDblClkLabel);
            this.Controls.Add(this.LButtonDblClkLabel);
            this.Controls.Add(this.XButton2Label);
            this.Controls.Add(this.XButton1Label);
            this.Controls.Add(this.MButtonLabel);
            this.Controls.Add(this.RButtonLabel);
            this.Controls.Add(this.LButtonLabel);
            this.Controls.Add(this.textBoxLog);
            this.Controls.Add(this.groupBox2);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "TestFormStatic";
            this.Text = "Test for the class HookManager";
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelMousePosition;
        private System.Windows.Forms.TextBox textBoxLog;
        private System.Windows.Forms.Label labelWheel;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox checkBoxKeyUp;
        private System.Windows.Forms.CheckBox checkBoxKeyPress;
        private System.Windows.Forms.CheckBox checkBoxKeyDown;
        private System.Windows.Forms.CheckBox checkBoxMouseWheel;
        private System.Windows.Forms.CheckBox checkBoxMouseDoubleClick;
        private System.Windows.Forms.CheckBox checkBoxOnMouseUp;
        private System.Windows.Forms.CheckBox checkBoxOnMouseDown;
        private System.Windows.Forms.CheckBox checkBoxOnMouseClick;
        private System.Windows.Forms.CheckBox checkBoxOnMouseMove;
        private System.Windows.Forms.Label LButtonLabel;
        private System.Windows.Forms.Label RButtonLabel;
        private System.Windows.Forms.Label MButtonLabel;
        private System.Windows.Forms.Label XButton1Label;
        private System.Windows.Forms.Label XButton2Label;
        private System.Windows.Forms.Label XButton2DblClkLabel;
        private System.Windows.Forms.Label XButton1DblClkLabel;
        private System.Windows.Forms.Label MButtonDblClkLabel;
        private System.Windows.Forms.Label RButtonDblClkLabel;
        private System.Windows.Forms.Label LButtonDblClkLabel;
        private System.Windows.Forms.Label WheelBackScrollLabel;
        private System.Windows.Forms.Label WheelForwardScrollLabel;
    }
}