﻿namespace InputStatTrackerApplication
{
    partial class InputStatTrackerGUI
    {
        /**
         * Required designer variable.
         */
        private System.ComponentModel.IContainer components = null;

        /**
         * Clean up any resources being used.
         *
         * @author Kovox
         * @date 08/03/2018
         *
         * @param disposing true if managed resources should be disposed; otherwise, false.
         */
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /**
         * Required method for Designer support - do not modify the contents of this method with the
         * code editor.
         *
         * @author Kovox
         * @date 08/03/2018
         */
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InputStatTrackerGUI));
            this.MainVerticalSeparatorStatsPanel = new System.Windows.Forms.Label();
            this.MainTimer = new System.Windows.Forms.Timer(this.components);
            this.MainToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.KeyboardStatsButton = new InputStatTrackerApplication.DoubleBufferedPanel();
            this.MouseStatsButton = new InputStatTrackerApplication.DoubleBufferedPanel();
            this.StatsButtonHorizontalSeparator = new System.Windows.Forms.Label();
            this.ControlPanel = new InputStatTrackerApplication.DoubleBufferedPanel();
            this.MouseXPosition = new System.Windows.Forms.Label();
            this.SavingData = new System.Windows.Forms.Label();
            this.Logo = new System.Windows.Forms.PictureBox();
            this.MainHorizontalSeparator = new System.Windows.Forms.Label();
            this.MainVerticalSeparatorControlPanel = new System.Windows.Forms.Label();
            this.MinimizeApplicationButton = new System.Windows.Forms.PictureBox();
            this.CloseApplicationButton = new System.Windows.Forms.PictureBox();
            this.MouseStatsPanel = new InputStatTrackerApplication.DoubleBufferedPanel();
            this.MouseWheelScrollBackCounter = new System.Windows.Forms.Label();
            this.MouseWheelScrollForwardCounter = new System.Windows.Forms.Label();
            this.MouseWheelScrollForward = new System.Windows.Forms.Label();
            this.MouseWheelScrollBack = new System.Windows.Forms.Label();
            this.LeftClicksCounter = new System.Windows.Forms.Label();
            this.RightClicksCounter = new System.Windows.Forms.Label();
            this.MiddleClicksCounter = new System.Windows.Forms.Label();
            this.XButton1ClicksCounter = new System.Windows.Forms.Label();
            this.XButton2ClicksCounter = new System.Windows.Forms.Label();
            this.LeftDoubleClicksCounter = new System.Windows.Forms.Label();
            this.RightDoubleClicksCounter = new System.Windows.Forms.Label();
            this.MiddleDoubleClicksCounter = new System.Windows.Forms.Label();
            this.XButton1DoubleClicksCounter = new System.Windows.Forms.Label();
            this.XButton2DoubleClicksCounter = new System.Windows.Forms.Label();
            this.MainMouseDataVerticalSeparator = new System.Windows.Forms.Label();
            this.XButton2DoubleClicks = new System.Windows.Forms.Label();
            this.XButton1DoubleClicksLabel = new System.Windows.Forms.Label();
            this.MiddleDoubleClicksLabel = new System.Windows.Forms.Label();
            this.RightDoubleClicksLabel = new System.Windows.Forms.Label();
            this.LeftDoubleClicksLabel = new System.Windows.Forms.Label();
            this.XButton2ClicksLabel = new System.Windows.Forms.Label();
            this.XButton1ClicksLabel = new System.Windows.Forms.Label();
            this.MiddleClicksLabel = new System.Windows.Forms.Label();
            this.RightClicksLabel = new System.Windows.Forms.Label();
            this.LeftClicksLabel = new System.Windows.Forms.Label();
            this.KeyboardStatsPanel = new InputStatTrackerApplication.DoubleBufferedPanel();
            this.OverviewStatsPanel = new InputStatTrackerApplication.DoubleBufferedPanel();
            this.MouseStatsButton.SuspendLayout();
            this.ControlPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinimizeApplicationButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CloseApplicationButton)).BeginInit();
            this.MouseStatsPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainVerticalSeparatorStatsPanel
            // 
            this.MainVerticalSeparatorStatsPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MainVerticalSeparatorStatsPanel.Location = new System.Drawing.Point(128, 107);
            this.MainVerticalSeparatorStatsPanel.Name = "MainVerticalSeparatorStatsPanel";
            this.MainVerticalSeparatorStatsPanel.Size = new System.Drawing.Size(1, 612);
            this.MainVerticalSeparatorStatsPanel.TabIndex = 0;
            // 
            // MainTimer
            // 
            this.MainTimer.Interval = 10;
            // 
            // KeyboardStatsButton
            // 
            this.KeyboardStatsButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(54)))));
            this.KeyboardStatsButton.BackgroundImage = global::InputStatTrackerApplication.Properties.Resources.KeyboardStatsButton;
            this.KeyboardStatsButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.KeyboardStatsButton.Location = new System.Drawing.Point(0, 414);
            this.KeyboardStatsButton.Name = "KeyboardStatsButton";
            this.KeyboardStatsButton.Size = new System.Drawing.Size(128, 306);
            this.KeyboardStatsButton.TabIndex = 1;
            this.KeyboardStatsButton.TabStop = true;
            this.KeyboardStatsButton.Click += new System.EventHandler(this.KeyboardStatsButton_Click);
            this.KeyboardStatsButton.MouseHover += new System.EventHandler(this.KeyboardStatsButton_MouseHover);
            // 
            // MouseStatsButton
            // 
            this.MouseStatsButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(54)))));
            this.MouseStatsButton.BackgroundImage = global::InputStatTrackerApplication.Properties.Resources.MouseStatsButton;
            this.MouseStatsButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.MouseStatsButton.Controls.Add(this.StatsButtonHorizontalSeparator);
            this.MouseStatsButton.Location = new System.Drawing.Point(0, 108);
            this.MouseStatsButton.Name = "MouseStatsButton";
            this.MouseStatsButton.Size = new System.Drawing.Size(128, 306);
            this.MouseStatsButton.TabIndex = 0;
            this.MouseStatsButton.TabStop = true;
            this.MouseStatsButton.Click += new System.EventHandler(this.MouseStatsButton_Click);
            this.MouseStatsButton.MouseHover += new System.EventHandler(this.MouseStatsButton_MouseHover);
            // 
            // StatsButtonHorizontalSeparator
            // 
            this.StatsButtonHorizontalSeparator.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.StatsButtonHorizontalSeparator.Location = new System.Drawing.Point(0, 305);
            this.StatsButtonHorizontalSeparator.Name = "StatsButtonHorizontalSeparator";
            this.StatsButtonHorizontalSeparator.Size = new System.Drawing.Size(128, 1);
            this.StatsButtonHorizontalSeparator.TabIndex = 0;
            // 
            // ControlPanel
            // 
            this.ControlPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.ControlPanel.Controls.Add(this.MouseXPosition);
            this.ControlPanel.Controls.Add(this.SavingData);
            this.ControlPanel.Controls.Add(this.Logo);
            this.ControlPanel.Controls.Add(this.MainHorizontalSeparator);
            this.ControlPanel.Controls.Add(this.MainVerticalSeparatorControlPanel);
            this.ControlPanel.Controls.Add(this.MinimizeApplicationButton);
            this.ControlPanel.Controls.Add(this.CloseApplicationButton);
            this.ControlPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.ControlPanel.Location = new System.Drawing.Point(0, 0);
            this.ControlPanel.Name = "ControlPanel";
            this.ControlPanel.Size = new System.Drawing.Size(1280, 108);
            this.ControlPanel.TabIndex = 0;
            // 
            // MouseXPosition
            // 
            this.MouseXPosition.BackColor = System.Drawing.Color.Transparent;
            this.MouseXPosition.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MouseXPosition.ForeColor = System.Drawing.Color.White;
            this.MouseXPosition.Location = new System.Drawing.Point(1136, 46);
            this.MouseXPosition.Name = "MouseXPosition";
            this.MouseXPosition.Size = new System.Drawing.Size(132, 50);
            this.MouseXPosition.TabIndex = 4;
            this.MouseXPosition.Text = "X = {####} Y = {####}";
            // 
            // SavingData
            // 
            this.SavingData.AutoSize = true;
            this.SavingData.BackColor = System.Drawing.Color.Transparent;
            this.SavingData.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SavingData.ForeColor = System.Drawing.Color.White;
            this.SavingData.Location = new System.Drawing.Point(1079, 8);
            this.SavingData.Name = "SavingData";
            this.SavingData.Size = new System.Drawing.Size(108, 20);
            this.SavingData.TabIndex = 3;
            this.SavingData.Text = "Saving data...";
            this.SavingData.Visible = false;
            // 
            // Logo
            // 
            this.Logo.BackColor = System.Drawing.Color.Transparent;
            this.Logo.BackgroundImage = global::InputStatTrackerApplication.Properties.Resources.IST_Logo_Small;
            this.Logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Logo.Location = new System.Drawing.Point(1, 0);
            this.Logo.Name = "Logo";
            this.Logo.Size = new System.Drawing.Size(126, 106);
            this.Logo.TabIndex = 2;
            this.Logo.TabStop = false;
            this.Logo.Click += new System.EventHandler(this.Logo_Click);
            this.Logo.MouseHover += new System.EventHandler(this.Logo_MouseHover);
            // 
            // MainHorizontalSeparator
            // 
            this.MainHorizontalSeparator.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MainHorizontalSeparator.Location = new System.Drawing.Point(0, 106);
            this.MainHorizontalSeparator.Name = "MainHorizontalSeparator";
            this.MainHorizontalSeparator.Size = new System.Drawing.Size(1280, 1);
            this.MainHorizontalSeparator.TabIndex = 0;
            // 
            // MainVerticalSeparatorControlPanel
            // 
            this.MainVerticalSeparatorControlPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MainVerticalSeparatorControlPanel.Location = new System.Drawing.Point(128, 0);
            this.MainVerticalSeparatorControlPanel.Name = "MainVerticalSeparatorControlPanel";
            this.MainVerticalSeparatorControlPanel.Size = new System.Drawing.Size(1, 108);
            this.MainVerticalSeparatorControlPanel.TabIndex = 0;
            // 
            // MinimizeApplicationButton
            // 
            this.MinimizeApplicationButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.MinimizeApplicationButton.BackColor = System.Drawing.Color.Transparent;
            this.MinimizeApplicationButton.BackgroundImage = global::InputStatTrackerApplication.Properties.Resources.MinimizeApplicationButton;
            this.MinimizeApplicationButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MinimizeApplicationButton.Location = new System.Drawing.Point(1216, 0);
            this.MinimizeApplicationButton.Margin = new System.Windows.Forms.Padding(0);
            this.MinimizeApplicationButton.Name = "MinimizeApplicationButton";
            this.MinimizeApplicationButton.Size = new System.Drawing.Size(32, 32);
            this.MinimizeApplicationButton.TabIndex = 1;
            this.MinimizeApplicationButton.TabStop = false;
            this.MinimizeApplicationButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MinimizeApplicationButton_MouseDown);
            this.MinimizeApplicationButton.MouseEnter += new System.EventHandler(this.MinimizeApplicationButton_MouseEnter);
            this.MinimizeApplicationButton.MouseLeave += new System.EventHandler(this.MinimizeApplicationButton_MouseLeave);
            this.MinimizeApplicationButton.MouseHover += new System.EventHandler(this.MinimizeApplicationButton_MouseHover);
            this.MinimizeApplicationButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.MinimizeApplicationButton_MouseUp);
            // 
            // CloseApplicationButton
            // 
            this.CloseApplicationButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.CloseApplicationButton.BackColor = System.Drawing.Color.Transparent;
            this.CloseApplicationButton.BackgroundImage = global::InputStatTrackerApplication.Properties.Resources.CloseApplicationButton;
            this.CloseApplicationButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CloseApplicationButton.Location = new System.Drawing.Point(1248, 0);
            this.CloseApplicationButton.Margin = new System.Windows.Forms.Padding(0);
            this.CloseApplicationButton.Name = "CloseApplicationButton";
            this.CloseApplicationButton.Size = new System.Drawing.Size(32, 32);
            this.CloseApplicationButton.TabIndex = 0;
            this.CloseApplicationButton.TabStop = false;
            this.CloseApplicationButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.CloseApplicationButton_MouseDown);
            this.CloseApplicationButton.MouseEnter += new System.EventHandler(this.CloseApplicationButton_MouseEnter);
            this.CloseApplicationButton.MouseLeave += new System.EventHandler(this.CloseApplicationButton_MouseLeave);
            this.CloseApplicationButton.MouseHover += new System.EventHandler(this.CloseApplicationButton_MouseHover);
            this.CloseApplicationButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.CloseApplicationButton_MouseUp);
            // 
            // MouseStatsPanel
            // 
            this.MouseStatsPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(38)))));
            this.MouseStatsPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.MouseStatsPanel.Controls.Add(this.MouseWheelScrollBackCounter);
            this.MouseStatsPanel.Controls.Add(this.MouseWheelScrollForwardCounter);
            this.MouseStatsPanel.Controls.Add(this.MouseWheelScrollForward);
            this.MouseStatsPanel.Controls.Add(this.MouseWheelScrollBack);
            this.MouseStatsPanel.Controls.Add(this.LeftClicksCounter);
            this.MouseStatsPanel.Controls.Add(this.RightClicksCounter);
            this.MouseStatsPanel.Controls.Add(this.MiddleClicksCounter);
            this.MouseStatsPanel.Controls.Add(this.XButton1ClicksCounter);
            this.MouseStatsPanel.Controls.Add(this.XButton2ClicksCounter);
            this.MouseStatsPanel.Controls.Add(this.LeftDoubleClicksCounter);
            this.MouseStatsPanel.Controls.Add(this.RightDoubleClicksCounter);
            this.MouseStatsPanel.Controls.Add(this.MiddleDoubleClicksCounter);
            this.MouseStatsPanel.Controls.Add(this.XButton1DoubleClicksCounter);
            this.MouseStatsPanel.Controls.Add(this.XButton2DoubleClicksCounter);
            this.MouseStatsPanel.Controls.Add(this.MainMouseDataVerticalSeparator);
            this.MouseStatsPanel.Controls.Add(this.XButton2DoubleClicks);
            this.MouseStatsPanel.Controls.Add(this.XButton1DoubleClicksLabel);
            this.MouseStatsPanel.Controls.Add(this.MiddleDoubleClicksLabel);
            this.MouseStatsPanel.Controls.Add(this.RightDoubleClicksLabel);
            this.MouseStatsPanel.Controls.Add(this.LeftDoubleClicksLabel);
            this.MouseStatsPanel.Controls.Add(this.XButton2ClicksLabel);
            this.MouseStatsPanel.Controls.Add(this.XButton1ClicksLabel);
            this.MouseStatsPanel.Controls.Add(this.MiddleClicksLabel);
            this.MouseStatsPanel.Controls.Add(this.RightClicksLabel);
            this.MouseStatsPanel.Controls.Add(this.LeftClicksLabel);
            this.MouseStatsPanel.Location = new System.Drawing.Point(129, 108);
            this.MouseStatsPanel.Name = "MouseStatsPanel";
            this.MouseStatsPanel.Size = new System.Drawing.Size(1151, 612);
            this.MouseStatsPanel.TabIndex = 0;
            // 
            // MouseWheelScrollBackCounter
            // 
            this.MouseWheelScrollBackCounter.BackColor = System.Drawing.Color.Transparent;
            this.MouseWheelScrollBackCounter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MouseWheelScrollBackCounter.ForeColor = System.Drawing.Color.White;
            this.MouseWheelScrollBackCounter.Location = new System.Drawing.Point(404, 480);
            this.MouseWheelScrollBackCounter.Name = "MouseWheelScrollBackCounter";
            this.MouseWheelScrollBackCounter.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.MouseWheelScrollBackCounter.Size = new System.Drawing.Size(284, 23);
            this.MouseWheelScrollBackCounter.TabIndex = 14;
            this.MouseWheelScrollBackCounter.Text = "0";
            // 
            // MouseWheelScrollForwardCounter
            // 
            this.MouseWheelScrollForwardCounter.BackColor = System.Drawing.Color.Transparent;
            this.MouseWheelScrollForwardCounter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MouseWheelScrollForwardCounter.ForeColor = System.Drawing.Color.White;
            this.MouseWheelScrollForwardCounter.Location = new System.Drawing.Point(404, 520);
            this.MouseWheelScrollForwardCounter.Name = "MouseWheelScrollForwardCounter";
            this.MouseWheelScrollForwardCounter.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.MouseWheelScrollForwardCounter.Size = new System.Drawing.Size(284, 23);
            this.MouseWheelScrollForwardCounter.TabIndex = 13;
            this.MouseWheelScrollForwardCounter.Text = "0";
            // 
            // MouseWheelScrollForward
            // 
            this.MouseWheelScrollForward.BackColor = System.Drawing.Color.Transparent;
            this.MouseWheelScrollForward.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MouseWheelScrollForward.ForeColor = System.Drawing.Color.White;
            this.MouseWheelScrollForward.Location = new System.Drawing.Point(40, 520);
            this.MouseWheelScrollForward.Name = "MouseWheelScrollForward";
            this.MouseWheelScrollForward.Size = new System.Drawing.Size(311, 23);
            this.MouseWheelScrollForward.TabIndex = 11;
            this.MouseWheelScrollForward.Text = "Mouse Wheel Scroll Forward";
            // 
            // MouseWheelScrollBack
            // 
            this.MouseWheelScrollBack.BackColor = System.Drawing.Color.Transparent;
            this.MouseWheelScrollBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MouseWheelScrollBack.ForeColor = System.Drawing.Color.White;
            this.MouseWheelScrollBack.Location = new System.Drawing.Point(40, 480);
            this.MouseWheelScrollBack.Name = "MouseWheelScrollBack";
            this.MouseWheelScrollBack.Size = new System.Drawing.Size(284, 23);
            this.MouseWheelScrollBack.TabIndex = 12;
            this.MouseWheelScrollBack.Text = "Mouse Wheel Scroll Back";
            // 
            // LeftClicksCounter
            // 
            this.LeftClicksCounter.BackColor = System.Drawing.Color.Transparent;
            this.LeftClicksCounter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LeftClicksCounter.ForeColor = System.Drawing.Color.White;
            this.LeftClicksCounter.Location = new System.Drawing.Point(404, 36);
            this.LeftClicksCounter.Name = "LeftClicksCounter";
            this.LeftClicksCounter.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.LeftClicksCounter.Size = new System.Drawing.Size(284, 23);
            this.LeftClicksCounter.TabIndex = 10;
            this.LeftClicksCounter.Text = "0";
            // 
            // RightClicksCounter
            // 
            this.RightClicksCounter.BackColor = System.Drawing.Color.Transparent;
            this.RightClicksCounter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RightClicksCounter.ForeColor = System.Drawing.Color.White;
            this.RightClicksCounter.Location = new System.Drawing.Point(404, 80);
            this.RightClicksCounter.Name = "RightClicksCounter";
            this.RightClicksCounter.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightClicksCounter.Size = new System.Drawing.Size(284, 23);
            this.RightClicksCounter.TabIndex = 9;
            this.RightClicksCounter.Text = "0";
            // 
            // MiddleClicksCounter
            // 
            this.MiddleClicksCounter.BackColor = System.Drawing.Color.Transparent;
            this.MiddleClicksCounter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MiddleClicksCounter.ForeColor = System.Drawing.Color.White;
            this.MiddleClicksCounter.Location = new System.Drawing.Point(404, 120);
            this.MiddleClicksCounter.Name = "MiddleClicksCounter";
            this.MiddleClicksCounter.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.MiddleClicksCounter.Size = new System.Drawing.Size(284, 23);
            this.MiddleClicksCounter.TabIndex = 8;
            this.MiddleClicksCounter.Text = "0";
            // 
            // XButton1ClicksCounter
            // 
            this.XButton1ClicksCounter.BackColor = System.Drawing.Color.Transparent;
            this.XButton1ClicksCounter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XButton1ClicksCounter.ForeColor = System.Drawing.Color.White;
            this.XButton1ClicksCounter.Location = new System.Drawing.Point(404, 160);
            this.XButton1ClicksCounter.Name = "XButton1ClicksCounter";
            this.XButton1ClicksCounter.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.XButton1ClicksCounter.Size = new System.Drawing.Size(284, 23);
            this.XButton1ClicksCounter.TabIndex = 7;
            this.XButton1ClicksCounter.Text = "0";
            // 
            // XButton2ClicksCounter
            // 
            this.XButton2ClicksCounter.BackColor = System.Drawing.Color.Transparent;
            this.XButton2ClicksCounter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XButton2ClicksCounter.ForeColor = System.Drawing.Color.White;
            this.XButton2ClicksCounter.Location = new System.Drawing.Point(404, 200);
            this.XButton2ClicksCounter.Name = "XButton2ClicksCounter";
            this.XButton2ClicksCounter.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.XButton2ClicksCounter.Size = new System.Drawing.Size(284, 23);
            this.XButton2ClicksCounter.TabIndex = 6;
            this.XButton2ClicksCounter.Text = "0";
            // 
            // LeftDoubleClicksCounter
            // 
            this.LeftDoubleClicksCounter.BackColor = System.Drawing.Color.Transparent;
            this.LeftDoubleClicksCounter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LeftDoubleClicksCounter.ForeColor = System.Drawing.Color.White;
            this.LeftDoubleClicksCounter.Location = new System.Drawing.Point(404, 260);
            this.LeftDoubleClicksCounter.Name = "LeftDoubleClicksCounter";
            this.LeftDoubleClicksCounter.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.LeftDoubleClicksCounter.Size = new System.Drawing.Size(284, 23);
            this.LeftDoubleClicksCounter.TabIndex = 5;
            this.LeftDoubleClicksCounter.Text = "0";
            // 
            // RightDoubleClicksCounter
            // 
            this.RightDoubleClicksCounter.BackColor = System.Drawing.Color.Transparent;
            this.RightDoubleClicksCounter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RightDoubleClicksCounter.ForeColor = System.Drawing.Color.White;
            this.RightDoubleClicksCounter.Location = new System.Drawing.Point(404, 301);
            this.RightDoubleClicksCounter.Name = "RightDoubleClicksCounter";
            this.RightDoubleClicksCounter.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightDoubleClicksCounter.Size = new System.Drawing.Size(284, 23);
            this.RightDoubleClicksCounter.TabIndex = 4;
            this.RightDoubleClicksCounter.Text = "0";
            // 
            // MiddleDoubleClicksCounter
            // 
            this.MiddleDoubleClicksCounter.BackColor = System.Drawing.Color.Transparent;
            this.MiddleDoubleClicksCounter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MiddleDoubleClicksCounter.ForeColor = System.Drawing.Color.White;
            this.MiddleDoubleClicksCounter.Location = new System.Drawing.Point(404, 340);
            this.MiddleDoubleClicksCounter.Name = "MiddleDoubleClicksCounter";
            this.MiddleDoubleClicksCounter.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.MiddleDoubleClicksCounter.Size = new System.Drawing.Size(284, 23);
            this.MiddleDoubleClicksCounter.TabIndex = 3;
            this.MiddleDoubleClicksCounter.Text = "0";
            // 
            // XButton1DoubleClicksCounter
            // 
            this.XButton1DoubleClicksCounter.BackColor = System.Drawing.Color.Transparent;
            this.XButton1DoubleClicksCounter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XButton1DoubleClicksCounter.ForeColor = System.Drawing.Color.White;
            this.XButton1DoubleClicksCounter.Location = new System.Drawing.Point(404, 380);
            this.XButton1DoubleClicksCounter.Name = "XButton1DoubleClicksCounter";
            this.XButton1DoubleClicksCounter.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.XButton1DoubleClicksCounter.Size = new System.Drawing.Size(284, 23);
            this.XButton1DoubleClicksCounter.TabIndex = 2;
            this.XButton1DoubleClicksCounter.Text = "0";
            // 
            // XButton2DoubleClicksCounter
            // 
            this.XButton2DoubleClicksCounter.BackColor = System.Drawing.Color.Transparent;
            this.XButton2DoubleClicksCounter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XButton2DoubleClicksCounter.ForeColor = System.Drawing.Color.White;
            this.XButton2DoubleClicksCounter.Location = new System.Drawing.Point(404, 420);
            this.XButton2DoubleClicksCounter.Name = "XButton2DoubleClicksCounter";
            this.XButton2DoubleClicksCounter.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.XButton2DoubleClicksCounter.Size = new System.Drawing.Size(284, 23);
            this.XButton2DoubleClicksCounter.TabIndex = 1;
            this.XButton2DoubleClicksCounter.Text = "0";
            // 
            // MainMouseDataVerticalSeparator
            // 
            this.MainMouseDataVerticalSeparator.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MainMouseDataVerticalSeparator.Location = new System.Drawing.Point(377, 40);
            this.MainMouseDataVerticalSeparator.Name = "MainMouseDataVerticalSeparator";
            this.MainMouseDataVerticalSeparator.Size = new System.Drawing.Size(1, 503);
            this.MainMouseDataVerticalSeparator.TabIndex = 0;
            // 
            // XButton2DoubleClicks
            // 
            this.XButton2DoubleClicks.BackColor = System.Drawing.Color.Transparent;
            this.XButton2DoubleClicks.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XButton2DoubleClicks.ForeColor = System.Drawing.Color.White;
            this.XButton2DoubleClicks.Location = new System.Drawing.Point(40, 420);
            this.XButton2DoubleClicks.Name = "XButton2DoubleClicks";
            this.XButton2DoubleClicks.Size = new System.Drawing.Size(284, 23);
            this.XButton2DoubleClicks.TabIndex = 0;
            this.XButton2DoubleClicks.Text = "X Button 2 Double Clicks";
            // 
            // XButton1DoubleClicksLabel
            // 
            this.XButton1DoubleClicksLabel.BackColor = System.Drawing.Color.Transparent;
            this.XButton1DoubleClicksLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XButton1DoubleClicksLabel.ForeColor = System.Drawing.Color.White;
            this.XButton1DoubleClicksLabel.Location = new System.Drawing.Point(40, 380);
            this.XButton1DoubleClicksLabel.Name = "XButton1DoubleClicksLabel";
            this.XButton1DoubleClicksLabel.Size = new System.Drawing.Size(284, 23);
            this.XButton1DoubleClicksLabel.TabIndex = 0;
            this.XButton1DoubleClicksLabel.Text = "X Button 1 Double Clicks";
            // 
            // MiddleDoubleClicksLabel
            // 
            this.MiddleDoubleClicksLabel.BackColor = System.Drawing.Color.Transparent;
            this.MiddleDoubleClicksLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MiddleDoubleClicksLabel.ForeColor = System.Drawing.Color.White;
            this.MiddleDoubleClicksLabel.Location = new System.Drawing.Point(40, 340);
            this.MiddleDoubleClicksLabel.Name = "MiddleDoubleClicksLabel";
            this.MiddleDoubleClicksLabel.Size = new System.Drawing.Size(284, 23);
            this.MiddleDoubleClicksLabel.TabIndex = 0;
            this.MiddleDoubleClicksLabel.Text = "Middle Double Clicks";
            // 
            // RightDoubleClicksLabel
            // 
            this.RightDoubleClicksLabel.BackColor = System.Drawing.Color.Transparent;
            this.RightDoubleClicksLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RightDoubleClicksLabel.ForeColor = System.Drawing.Color.White;
            this.RightDoubleClicksLabel.Location = new System.Drawing.Point(40, 300);
            this.RightDoubleClicksLabel.Name = "RightDoubleClicksLabel";
            this.RightDoubleClicksLabel.Size = new System.Drawing.Size(284, 23);
            this.RightDoubleClicksLabel.TabIndex = 0;
            this.RightDoubleClicksLabel.Text = "Right Double Clicks";
            // 
            // LeftDoubleClicksLabel
            // 
            this.LeftDoubleClicksLabel.BackColor = System.Drawing.Color.Transparent;
            this.LeftDoubleClicksLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LeftDoubleClicksLabel.ForeColor = System.Drawing.Color.White;
            this.LeftDoubleClicksLabel.Location = new System.Drawing.Point(40, 260);
            this.LeftDoubleClicksLabel.Name = "LeftDoubleClicksLabel";
            this.LeftDoubleClicksLabel.Size = new System.Drawing.Size(284, 23);
            this.LeftDoubleClicksLabel.TabIndex = 0;
            this.LeftDoubleClicksLabel.Text = "Left Double Clicks";
            // 
            // XButton2ClicksLabel
            // 
            this.XButton2ClicksLabel.BackColor = System.Drawing.Color.Transparent;
            this.XButton2ClicksLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XButton2ClicksLabel.ForeColor = System.Drawing.Color.White;
            this.XButton2ClicksLabel.Location = new System.Drawing.Point(40, 200);
            this.XButton2ClicksLabel.Name = "XButton2ClicksLabel";
            this.XButton2ClicksLabel.Size = new System.Drawing.Size(284, 23);
            this.XButton2ClicksLabel.TabIndex = 0;
            this.XButton2ClicksLabel.Text = "X Button 2 Clicks";
            // 
            // XButton1ClicksLabel
            // 
            this.XButton1ClicksLabel.BackColor = System.Drawing.Color.Transparent;
            this.XButton1ClicksLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.XButton1ClicksLabel.ForeColor = System.Drawing.Color.White;
            this.XButton1ClicksLabel.Location = new System.Drawing.Point(40, 160);
            this.XButton1ClicksLabel.Name = "XButton1ClicksLabel";
            this.XButton1ClicksLabel.Size = new System.Drawing.Size(284, 23);
            this.XButton1ClicksLabel.TabIndex = 0;
            this.XButton1ClicksLabel.Text = "X Button 1 Clicks";
            // 
            // MiddleClicksLabel
            // 
            this.MiddleClicksLabel.BackColor = System.Drawing.Color.Transparent;
            this.MiddleClicksLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MiddleClicksLabel.ForeColor = System.Drawing.Color.White;
            this.MiddleClicksLabel.Location = new System.Drawing.Point(40, 120);
            this.MiddleClicksLabel.Name = "MiddleClicksLabel";
            this.MiddleClicksLabel.Size = new System.Drawing.Size(284, 23);
            this.MiddleClicksLabel.TabIndex = 0;
            this.MiddleClicksLabel.Text = "Middle Clicks";
            // 
            // RightClicksLabel
            // 
            this.RightClicksLabel.BackColor = System.Drawing.Color.Transparent;
            this.RightClicksLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RightClicksLabel.ForeColor = System.Drawing.Color.White;
            this.RightClicksLabel.Location = new System.Drawing.Point(40, 80);
            this.RightClicksLabel.Name = "RightClicksLabel";
            this.RightClicksLabel.Size = new System.Drawing.Size(284, 23);
            this.RightClicksLabel.TabIndex = 0;
            this.RightClicksLabel.Text = "Right Clicks";
            // 
            // LeftClicksLabel
            // 
            this.LeftClicksLabel.BackColor = System.Drawing.Color.Transparent;
            this.LeftClicksLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LeftClicksLabel.ForeColor = System.Drawing.Color.White;
            this.LeftClicksLabel.Location = new System.Drawing.Point(40, 40);
            this.LeftClicksLabel.Name = "LeftClicksLabel";
            this.LeftClicksLabel.Size = new System.Drawing.Size(284, 23);
            this.LeftClicksLabel.TabIndex = 0;
            this.LeftClicksLabel.Text = "Left Clicks";
            // 
            // KeyboardStatsPanel
            // 
            this.KeyboardStatsPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(38)))));
            this.KeyboardStatsPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.KeyboardStatsPanel.Location = new System.Drawing.Point(129, 108);
            this.KeyboardStatsPanel.Name = "KeyboardStatsPanel";
            this.KeyboardStatsPanel.Size = new System.Drawing.Size(1151, 612);
            this.KeyboardStatsPanel.TabIndex = 0;
            // 
            // OverviewStatsPanel
            // 
            this.OverviewStatsPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(38)))));
            this.OverviewStatsPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.OverviewStatsPanel.Location = new System.Drawing.Point(129, 108);
            this.OverviewStatsPanel.Name = "OverviewStatsPanel";
            this.OverviewStatsPanel.Size = new System.Drawing.Size(1151, 612);
            this.OverviewStatsPanel.TabIndex = 0;
            // 
            // InputStatTrackerGUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1280, 720);
            this.Controls.Add(this.MainVerticalSeparatorStatsPanel);
            this.Controls.Add(this.KeyboardStatsButton);
            this.Controls.Add(this.MouseStatsButton);
            this.Controls.Add(this.ControlPanel);
            this.Controls.Add(this.MouseStatsPanel);
            this.Controls.Add(this.KeyboardStatsPanel);
            this.Controls.Add(this.OverviewStatsPanel);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "InputStatTrackerGUI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Input Stat Tracker";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.OnClose);
            this.Load += new System.EventHandler(this.OnStart);
            this.MouseStatsButton.ResumeLayout(false);
            this.ControlPanel.ResumeLayout(false);
            this.ControlPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinimizeApplicationButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CloseApplicationButton)).EndInit();
            this.MouseStatsPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private DoubleBufferedPanel ControlPanel;
        private DoubleBufferedPanel OverviewStatsPanel;
        private System.Windows.Forms.PictureBox CloseApplicationButton;
        private System.Windows.Forms.PictureBox MinimizeApplicationButton;
        private DoubleBufferedPanel MouseStatsButton;
        private DoubleBufferedPanel KeyboardStatsButton;
        private System.Windows.Forms.Label MainVerticalSeparatorControlPanel;
        private System.Windows.Forms.Label MainHorizontalSeparator;
        private System.Windows.Forms.Timer MainTimer;
        private System.Windows.Forms.ToolTip MainToolTip;
        private System.Windows.Forms.Label StatsButtonHorizontalSeparator;
        private DoubleBufferedPanel KeyboardStatsPanel;
        private System.Windows.Forms.Label MainVerticalSeparatorStatsPanel;
        private DoubleBufferedPanel MouseStatsPanel;
        private System.Windows.Forms.Label MainMouseDataVerticalSeparator;
        private System.Windows.Forms.Label XButton2DoubleClicks;
        private System.Windows.Forms.Label XButton1DoubleClicksLabel;
        private System.Windows.Forms.Label MiddleDoubleClicksLabel;
        private System.Windows.Forms.Label RightDoubleClicksLabel;
        private System.Windows.Forms.Label LeftDoubleClicksLabel;
        private System.Windows.Forms.Label XButton2ClicksLabel;
        private System.Windows.Forms.Label XButton1ClicksLabel;
        private System.Windows.Forms.Label MiddleClicksLabel;
        private System.Windows.Forms.Label RightClicksLabel;
        private System.Windows.Forms.Label LeftClicksLabel;
        private System.Windows.Forms.PictureBox Logo;
        private System.Windows.Forms.Label XButton2DoubleClicksCounter;
        private System.Windows.Forms.Label LeftClicksCounter;
        private System.Windows.Forms.Label RightClicksCounter;
        private System.Windows.Forms.Label MiddleClicksCounter;
        private System.Windows.Forms.Label XButton1ClicksCounter;
        private System.Windows.Forms.Label XButton2ClicksCounter;
        private System.Windows.Forms.Label LeftDoubleClicksCounter;
        private System.Windows.Forms.Label RightDoubleClicksCounter;
        private System.Windows.Forms.Label MiddleDoubleClicksCounter;
        private System.Windows.Forms.Label XButton1DoubleClicksCounter;
        private System.Windows.Forms.Label SavingData;
        private System.Windows.Forms.Label MouseXPosition;
        private System.Windows.Forms.Label MouseWheelScrollBackCounter;
        private System.Windows.Forms.Label MouseWheelScrollForwardCounter;
        private System.Windows.Forms.Label MouseWheelScrollForward;
        private System.Windows.Forms.Label MouseWheelScrollBack;
    }
}