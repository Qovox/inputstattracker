﻿using System.Windows.Forms;

namespace InputStatTrackerApplication
{
    public class DoubleBufferedPanel : Panel
    {
        public DoubleBufferedPanel() : base()
        {
            DoubleBuffered = true;
        }
    }
}
