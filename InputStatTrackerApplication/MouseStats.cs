﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace InputStatTrackerApplication
{
    public class MouseStats
    {
        public static int X { get; set; }
        public static int Y { get; set; }

        private static ulong LBUTTON;
        private static ulong RBUTTON;
        private static ulong MBUTTON;
        private static ulong XBUTTON1;
        private static ulong XBUTTON2;
        private static ulong LBUTTONDBLCLK;
        private static ulong RBUTTONDBLCLK;
        private static ulong MBUTTONDBLCLK;
        private static ulong XBUTTON1DBLCLK;
        private static ulong XBUTTON2DBLCLK;
        private static ulong WHEELBACKSCROLL;
        private static ulong WHEELFORWARDSCROLL;

        private static Dictionary<string, ulong> mapping = new Dictionary<string, ulong>()
        {
            { "Left", LBUTTON },
            { "Right", RBUTTON },
            { "Middle", MBUTTON },
            { "XButton1", XBUTTON1 },
            { "XButton2", XBUTTON2 },
            { "LeftDblClk", LBUTTONDBLCLK },
            { "RightDblClk", RBUTTONDBLCLK },
            { "MiddleDblClk", MBUTTONDBLCLK },
            { "XButton1DblClk", XBUTTON1DBLCLK },
            { "XButton2DblClk", XBUTTON2DBLCLK },
            { "WheelBackScroll", WHEELBACKSCROLL },
            { "WheelForwardScroll", WHEELFORWARDSCROLL }
        };

        /**
         * This default constructor is used to instanciate a brand new MouseStats object
         * either because the service is launched for the first time or because mouse statistics
         * file has been deleted or corrupted.
         */
        public MouseStats()
        {

        }

        /**
         * Instanciate a MouseStats object from existing data source.
         */
        public MouseStats(JObject data)
        {
            foreach (var obj in data)
            {
                mapping[obj.Key] = (ulong)obj.Value;
            }
        }

        public static void IncreaseValue(string button)
        {
            mapping[button]++;
        }

        public static void IncreaseWheelMovementValue(short value)
        {
            if (value < 0)
            {
                mapping["WheelBackScroll"]++;
            }
            else
            {
                mapping["WheelForwardScroll"]++;
            }
        }

        public Dictionary<string, ulong> Mapping
        {
            get
            {
                return mapping;
            }
        }

        /**
         * Serialize the object into JSON object format. Used when saving data.
         */
        public override string ToString()
        {
            return JsonConvert.SerializeObject(mapping);
        }
    }
}