﻿using System.Resources;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("InputStatTrackerApplication")]
[assembly: AssemblyDescription("This is the user application (GUI) of the InputStatTracker software.")]
[assembly: AssemblyCompany("Kovox")]
[assembly: AssemblyProduct("InputStatTracker")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: ComVisible(false)]
[assembly: NeutralResourcesLanguage("en")]

