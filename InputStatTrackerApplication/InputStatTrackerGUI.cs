﻿using InputStatTrackerLibrary;
using Newtonsoft.Json.Linq;
using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace InputStatTrackerApplication
{
    public partial class InputStatTrackerGUI : Form
    {
        private const string MOUSE_STATS = "mouse_stats";
        private const string KEYBOARD_STATS = "keyboard_stats";

        public static MouseStats mouseStats;
        public static KeyBoardStats keyBoardStats;

        public InputStatTrackerGUI() : base()
        {
            InitializeComponent();

            // Anti-flickering solutionish
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);

            // Binding the hooks.
            HookManager.MouseMove += HookManager_MouseMove;
            HookManager.MouseDown += HookManager_MouseDown;
            HookManager.MouseDoubleClick += HookManager_MouseDoubleClick;
            HookManager.MouseWheel += HookManager_MouseWheel;
            HookManager.KeyPress += HookManager_KeyPress;
            HookManager.KeyUp += HookManager_KeyUp;
            HookManager.KeyDown += HookManager_KeyDown;

            // Timer setup for periodical data saving.
            MainTimer = new Timer();
            MainTimer.Tick += new EventHandler(OnTimer);
            MainTimer.Interval = 60000 * 3;
            MainTimer.Start();

            OverviewStatsPanel.BringToFront();
        }

        #region Data Management

        /**
         * Initialize the data when the application is starting.
         */
        private void OnStart(object sender, EventArgs e)
        {
            // Fetch mouse data
            if (File.Exists(MOUSE_STATS) && new FileInfo(MOUSE_STATS).Length != 0)
            {
                mouseStats = new MouseStats(JObject.Parse(ReadFile(MOUSE_STATS)));
            }
            else
            {
                mouseStats = new MouseStats();
            }

            // Fetch keyboard data
            if (File.Exists(KEYBOARD_STATS) && new FileInfo(KEYBOARD_STATS).Length != 0)
            {
                keyBoardStats = new KeyBoardStats(JObject.Parse(ReadFile(KEYBOARD_STATS)));
            }
            else
            {
                keyBoardStats = new KeyBoardStats();
            }

            LeftClicksCounter.Text = mouseStats.Mapping["Left"].ToString();
            RightClicksCounter.Text = mouseStats.Mapping["Right"].ToString();
            MiddleClicksCounter.Text = mouseStats.Mapping["Middle"].ToString();
            XButton1ClicksCounter.Text = mouseStats.Mapping["XButton1"].ToString();
            XButton2ClicksCounter.Text = mouseStats.Mapping["XButton2"].ToString();
            LeftDoubleClicksCounter.Text = mouseStats.Mapping["LeftDblClk"].ToString();
            RightDoubleClicksCounter.Text = mouseStats.Mapping["RightDblClk"].ToString();
            MiddleDoubleClicksCounter.Text = mouseStats.Mapping["MiddleDblClk"].ToString();
            XButton1DoubleClicksCounter.Text = mouseStats.Mapping["XButton1DblClk"].ToString();
            XButton2DoubleClicksCounter.Text = mouseStats.Mapping["XButton2DblClk"].ToString();
            MouseWheelScrollBackCounter.Text = mouseStats.Mapping["WheelBackScroll"].ToString();
            MouseWheelScrollForwardCounter.Text = mouseStats.Mapping["WheelForwardScroll"].ToString();
        }

        /**
         * Saves the data periodically.
         */
        public void OnTimer(object sender, EventArgs args)
        {
            SavingData.Visible = true;
            WriteFile(MOUSE_STATS, mouseStats.ToString());
            WriteFile(KEYBOARD_STATS, keyBoardStats.ToString());
            SavingData.Text = "Data Saved!";
            SavingData.Visible = false;
            SavingData.Text = "Saving Data...";
        }

        /**
         * Saves the data when the application is closing.
         */
        private void OnClose(object sender, FormClosedEventArgs e)
        {
            HookManager.MouseMove -= HookManager_MouseMove;
            HookManager.MouseDown -= HookManager_MouseDown;
            HookManager.MouseDoubleClick -= HookManager_MouseDoubleClick;
            HookManager.MouseWheel -= HookManager_MouseWheel;
            HookManager.KeyPress -= HookManager_KeyPress;
            HookManager.KeyUp -= HookManager_KeyUp;
            HookManager.KeyDown -= HookManager_KeyDown;
            WriteFile(MOUSE_STATS, mouseStats.ToString());
            WriteFile(KEYBOARD_STATS, keyBoardStats.ToString());
        }

        private static void CreateFile(string path)
        {
            File.Create(path).Dispose();
        }

        private static string ReadFile(string path)
        {
            string data = File.ReadAllText(path);
            // Avoid deleting the file when the application is running because if the system crashes,
            // then the data are lost forever. Moreover, the data will be saved periodically when
            // the service is running so that in case of system crash, the least data are lost.
            // File.Delete(path);
            return data;
        }

        private static void WriteFile(string path, string data)
        {
            File.WriteAllText(path, data);
        }

        #endregion

        #region Hooks

        #region Mouse events

        private void HookManager_MouseMove(object sender, MouseEventArgs e)
        {
            MouseStats.X = e.X;
            MouseStats.Y = e.Y;
            MouseXPosition.Text = string.Format("X = {0:0000} Y = {1:0000}", MouseStats.X, MouseStats.Y);
        }

        private void HookManager_MouseDown(object sender, MouseEventArgs e)
        {
            MouseStats.IncreaseValue(e.Button.ToString());
            switch (e.Button)
            {
                case MouseButtons.Left:
                    LeftClicksCounter.Text = mouseStats.Mapping["Left"].ToString();
                    break;
                case MouseButtons.Right:
                    RightClicksCounter.Text = mouseStats.Mapping["Right"].ToString();
                    break;
                case MouseButtons.Middle:
                    MiddleClicksCounter.Text = mouseStats.Mapping["Middle"].ToString();
                    break;
                case MouseButtons.XButton1:
                    XButton1ClicksCounter.Text = mouseStats.Mapping["XButton1"].ToString();
                    break;
                case MouseButtons.XButton2:
                    XButton2ClicksCounter.Text = mouseStats.Mapping["XButton2"].ToString();
                    break;
            }
        }

        private void HookManager_MouseWheel(object sender, MouseEventArgs e)
        {
            /*
            MouseStats.IncreaseWheelMovementValue((short)((e.Delta >> 16) & 0xffff));
            if (e.Delta == 120)
            {
                MouseWheelScrollBackCounter.Text = mouseStats.Mapping["WheelBackScroll"].ToString();
            }
            else
            {
                MouseWheelScrollForwardCounter.Text = mouseStats.Mapping["WheelForwardScroll"].ToString();
            }
            */
        }

        private void HookManager_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            MouseStats.IncreaseValue(e.Button.ToString() + "DblClk");
            switch (e.Button)
            {
                case MouseButtons.Left:
                    LeftDoubleClicksCounter.Text = mouseStats.Mapping["LeftDblClk"].ToString();
                    break;
                case MouseButtons.Right:
                    RightDoubleClicksCounter.Text = mouseStats.Mapping["RightDblClk"].ToString();
                    break;
                case MouseButtons.Middle:
                    MiddleDoubleClicksCounter.Text = mouseStats.Mapping["MiddleDblClk"].ToString();
                    break;
                case MouseButtons.XButton1:
                    XButton1DoubleClicksCounter.Text = mouseStats.Mapping["XButton1DblClk"].ToString();
                    break;
                case MouseButtons.XButton2:
                    XButton2DoubleClicksCounter.Text = mouseStats.Mapping["XButton2DblClk"].ToString();
                    break;
            }
        }

        #endregion

        #region Keyboard events

        private void HookManager_KeyDown(object sender, KeyEventArgs e)
        {
            KeyBoardStats.IncreaseValue(e.KeyCode.ToString());
        }

        private void HookManager_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void HookManager_KeyUp(object sender, KeyEventArgs e)
        {

        }

        #endregion

        #endregion

        #region CloseApplicationButton

        private void CloseApplicationButton_MouseEnter(object sender, EventArgs e)
        {
            CloseApplicationButton.BackColor = Color.FromArgb(200, 63, 63, 65);
        }

        private void CloseApplicationButton_MouseLeave(object sender, EventArgs e)
        {
            CloseApplicationButton.BackColor = Color.FromArgb(0, 0, 0, 0);
        }

        private void CloseApplicationButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                CloseApplicationButton.BackColor = Color.FromArgb(200, 228, 19, 0);
            }
        }

        private void CloseApplicationButton_MouseUp(object sender, MouseEventArgs e)
        {
            if (((PictureBox)sender).ClientRectangle.Contains(((PictureBox)sender).PointToClient(Cursor.Position)))
            {
                Application.Exit();
            }
            else
            {
                CloseApplicationButton.BackColor = Color.FromArgb(0, 0, 0, 0);
            }
        }

        private void CloseApplicationButton_MouseHover(object sender, EventArgs e)
        {
            MainToolTip.SetToolTip(CloseApplicationButton, "Close Application");
        }

        #endregion

        #region MinimizeApplicationButton

        private void MinimizeApplicationButton_MouseEnter(object sender, EventArgs e)
        {
            MinimizeApplicationButton.BackColor = Color.FromArgb(255, 63, 66, 65);
        }

        private void MinimizeApplicationButton_MouseLeave(object sender, EventArgs e)
        {
            MinimizeApplicationButton.BackColor = Color.FromArgb(0, 0, 0, 0);
        }

        private void MinimizeApplicationButton_MouseDown(object sender, MouseEventArgs e)
        {
            MinimizeApplicationButton.BackColor = Color.FromArgb(127, 0, 122, 204);
        }

        private void MinimizeApplicationButton_MouseUp(object sender, MouseEventArgs e)
        {
            if (((PictureBox)sender).ClientRectangle.Contains(((PictureBox)sender).PointToClient(Cursor.Position)))
            {
                WindowState = FormWindowState.Minimized;
            }
            else
            {
                MinimizeApplicationButton.BackColor = Color.FromArgb(0, 0, 0, 0);
            }
        }

        private void MinimizeApplicationButton_MouseHover(object sender, EventArgs e)
        {
            MainToolTip.SetToolTip(MinimizeApplicationButton, "Minimize Application");
        }


        #endregion

        #region MouseStatsButton

        private void MouseStatsButton_Click(object sender, EventArgs e)
        {
            KeyboardStatsButton.BackgroundImage = Properties.Resources.KeyboardStatsButton;
            MouseStatsPanel.BringToFront();
            MouseStatsButton.BackgroundImage = Properties.Resources.MouseStatsButton_Clicked;
        }

        private void MouseStatsButton_MouseHover(object sender, EventArgs e)
        {
            MainToolTip.SetToolTip(MouseStatsButton, "Mouse Stats");
        }

        #endregion

        #region KeyboardStatsButton

        private void KeyboardStatsButton_Click(object sender, EventArgs e)
        {
            MouseStatsButton.BackgroundImage = Properties.Resources.MouseStatsButton;
            KeyboardStatsPanel.BringToFront();
            KeyboardStatsButton.BackgroundImage = Properties.Resources.KeyboardStatsButton_Clicked;
        }

        private void KeyboardStatsButton_MouseHover(object sender, EventArgs e)
        {
            MainToolTip.SetToolTip(KeyboardStatsButton, "Keyboard Stats");
        }

        #endregion

        #region Logo

        private void Logo_Click(object sender, EventArgs e)
        {
            MouseStatsButton.BackgroundImage = Properties.Resources.MouseStatsButton;
            KeyboardStatsButton.BackgroundImage = Properties.Resources.KeyboardStatsButton;
            OverviewStatsPanel.BringToFront();
        }

        private void Logo_MouseHover(object sender, EventArgs e)
        {
            MainToolTip.SetToolTip(Logo, "Go Home");
        }

        #endregion

        #region DrawingFunctions

        #endregion
    }
}