using InputStatTrackerLibrary;
using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace InputStatTrackerApplication
{
    public partial class TestFormStatic : Form
    {
        private static ulong LBUTTON;
        private static ulong RBUTTON;
        private static ulong MBUTTON;
        private static ulong XBUTTON1;
        private static ulong XBUTTON2;
        private static ulong LBUTTONDBLCLK;
        private static ulong RBUTTONDBLCLK;
        private static ulong MBUTTONDBLCLK;
        private static ulong XBUTTON1DBLCLK;
        private static ulong XBUTTON2DBLCLK;
        private static ulong WHEELBACKSCROLL;
        private static ulong WHEELFORWARDSCROLL;

        public TestFormStatic()
        {
            InitializeComponent();
        }

        //##################################################################

        #region Check boxes to set or remove particular event handlers.

        private void checkBoxOnMouseMove_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxOnMouseMove.Checked)
            {
                HookManager.MouseMove += HookManager_MouseMove;
            }
            else
            {
                HookManager.MouseMove -= HookManager_MouseMove;
            }
        }

        private void checkBoxOnMouseClick_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxOnMouseClick.Checked)
            {
                HookManager.MouseClick += HookManager_MouseClick;
            }
            else
            {
                HookManager.MouseClick -= HookManager_MouseClick;
            }
        }

        private void checkBoxOnMouseUp_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxOnMouseUp.Checked)
            {
                HookManager.MouseUp += HookManager_MouseUp;
            }
            else
            {
                HookManager.MouseUp -= HookManager_MouseUp;
            }
        }

        private void checkBoxOnMouseDown_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxOnMouseDown.Checked)
            {
                HookManager.MouseDown += HookManager_MouseDown;
            }
            else
            {
                HookManager.MouseDown -= HookManager_MouseDown;
            }
        }

        private void checkBoxMouseDoubleClick_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxMouseDoubleClick.Checked)
            {
                HookManager.MouseDoubleClick += HookManager_MouseDoubleClick;
            }
            else
            {
                HookManager.MouseDoubleClick -= HookManager_MouseDoubleClick;
            }
        }

        private void checkBoxMouseWheel_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxMouseWheel.Checked)
            {
                HookManager.MouseWheel += HookManager_MouseWheel;
            }
            else
            {
                HookManager.MouseWheel -= HookManager_MouseWheel;
            }
        }

        private void checkBoxKeyDown_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxKeyDown.Checked)
            {
                HookManager.KeyDown += HookManager_KeyDown;
            }
            else
            {
                HookManager.KeyDown -= HookManager_KeyDown;
            }
        }


        private void checkBoxKeyUp_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxKeyUp.Checked)
            {
                HookManager.KeyUp += HookManager_KeyUp;
            }
            else
            {
                HookManager.KeyUp -= HookManager_KeyUp;
            }
        }

        private void checkBoxKeyPress_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxKeyPress.Checked)
            {
                HookManager.KeyPress += HookManager_KeyPress;
            }
            else
            {
                HookManager.KeyPress -= HookManager_KeyPress;
            }
        }

        #endregion

        //##################################################################

        #region Event handlers of particular events. They will be activated when an appropriate checkbox is checked.

        private void HookManager_KeyDown(object sender, KeyEventArgs e)
        {
            textBoxLog.AppendText(string.Format("KeyDown - {0}\n", e.KeyCode));
            textBoxLog.ScrollToCaret();
        }

        private void HookManager_KeyUp(object sender, KeyEventArgs e)
        {
            textBoxLog.AppendText(string.Format("KeyUp - {0}\n", e.KeyCode));
            textBoxLog.ScrollToCaret();
        }


        private void HookManager_KeyPress(object sender, KeyPressEventArgs e)
        {
            textBoxLog.AppendText(string.Format("KeyPress - {0}\n", e.KeyChar));
            textBoxLog.ScrollToCaret();
        }


        private void HookManager_MouseMove(object sender, MouseEventArgs e)
        {
            labelMousePosition.Text = string.Format("x={0:0000}; y={1:0000}", e.X, e.Y);
        }

        private void HookManager_MouseClick(object sender, MouseEventArgs e)
        {
            textBoxLog.AppendText(string.Format("MouseClick - {0}\n", e.Button));
            IntPtr hwnd = GetForegroundWindow();
            uint pid;
            GetWindowThreadProcessId(hwnd, out pid);
            Process p = Process.GetProcessById((int)pid);
            textBoxLog.AppendText(string.Format("Application Focus: {0}\n", p.ToString()));
            textBoxLog.ScrollToCaret();
        }

        [DllImport("user32.dll")]
        public static extern IntPtr GetWindowThreadProcessId(IntPtr hWnd, out uint ProcessId);

        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();

        private void HookManager_MouseUp(object sender, MouseEventArgs e)
        {
            textBoxLog.AppendText(string.Format("MouseUp - {0}\n", e.Button));
            textBoxLog.ScrollToCaret();
        }


        private void HookManager_MouseDown(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Left:
                    LBUTTON++;
                    LButtonLabel.Text = "LButton: " + LBUTTON;
                    break;
                case MouseButtons.Right:
                    RBUTTON++;
                    RButtonLabel.Text = "RButton: " + RBUTTON;
                    break;
                case MouseButtons.Middle:
                    MBUTTON++;
                    MButtonLabel.Text = "MButton: " + MBUTTON;
                    break;
                case MouseButtons.XButton1:
                    XBUTTON1++;
                    XButton1Label.Text = "XButton1: " + XBUTTON1;
                    break;
                case MouseButtons.XButton2:
                    XBUTTON2++;
                    XButton2Label.Text = "XButton2: " + XBUTTON2;
                    break;
            }

            textBoxLog.AppendText(string.Format("MouseDown - {0}\n", e.Button));
            textBoxLog.ScrollToCaret();
        }


        private void HookManager_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Left:
                    LBUTTONDBLCLK++;
                    LButtonDblClkLabel.Text = "LButtonDblClk: " + LBUTTONDBLCLK;
                    break;
                case MouseButtons.Right:
                    RBUTTONDBLCLK++;
                    RButtonDblClkLabel.Text = "RButtonDblClk: " + RBUTTONDBLCLK;
                    break;
                case MouseButtons.Middle:
                    MBUTTONDBLCLK++;
                    MButtonDblClkLabel.Text = "MButtonDblClk: " + MBUTTONDBLCLK;
                    break;
                case MouseButtons.XButton1:
                    XBUTTON1DBLCLK++;
                    XButton1DblClkLabel.Text = "XButton1DblClk: " + XBUTTON1DBLCLK;
                    break;
                case MouseButtons.XButton2:
                    XBUTTON2DBLCLK++;
                    XButton2DblClkLabel.Text = "XButton2DblClk: " + XBUTTON2DBLCLK;
                    break;
            }
            textBoxLog.AppendText(string.Format("MouseDoubleClick - {0}\n", e.Button));
            textBoxLog.ScrollToCaret();
        }


        private void HookManager_MouseWheel(object sender, MouseEventArgs e)
        {
            switch (e.Delta)
            {
                case 120:
                    WHEELFORWARDSCROLL++;
                    WheelForwardScrollLabel.Text = "WheelForwardScroll: " + WHEELFORWARDSCROLL;
                    break;
                case -120:
                    WHEELBACKSCROLL++;
                    WheelBackScrollLabel.Text = "WheelBackScroll: " + WHEELBACKSCROLL;
                    break;
            }
            labelWheel.Text = string.Format("Wheel={0:000}", e.Delta);
        }

        #endregion
    }
}