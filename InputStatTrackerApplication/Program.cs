using System;
using System.Windows.Forms;

namespace InputStatTrackerApplication
{
    static class Program
    {
        /**
         * The main entry point for the application.
         *
         * @author Kovox
         * @date 08/03/2018
         */
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Application.Run(new TestFormStatic());
        }
    }
}