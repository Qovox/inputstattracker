# InputStatTracker

A simple software retrieving mouse and keyboard user inputs in order to ouput some statistics.

## InputStatTrackerApplication

C'est l'application graphique utilisateur. Celle où l'utilisateur pourra retrouver toutes les statistiques concernant les input clavier et souris. C'est aussi cette application qui va désormais capter les input utilisateur puisque l'InputStatTrackerService ne peut s'en occuper correctement.

## InputStatTrackerLibrary

C'est le coeur de l'application. C'est grâce à ce code que j'arrive à chopper les input peu importe où je me situe dans windows (à part hors session, là où l'application n'est plus active).

## InputStatTrackerService

J'ai abandonné l'idée d'utiliser un service windows car malheureusement, il devient compliqué de gérer les différents aspects et contraintes imposés par ceux-ci. J'aurais réussi la communication inter processus grâce à la communication par webservice en localhost sur un port non réservé (50000). J'aurais aussi réussi l'écriture de fichier et l'enregistrement des données au bon endroit avec le service windows. Mais ce qui pose problème, c'est l'interception des input qui ne se fait tout simplement pas. Donc pour le service ça tombe à l'eau. Mais je garde quand même le code, bien sûr.